import { HttpTokenInterceptor } from './shared/interceptor/http-token.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PageModule } from './pages/page.module';
import { ThemeModule } from './theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResetOldPasswordComponent } from './reset-old-password/reset-old-password.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    PageNotFoundComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    ResetOldPasswordComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    PageModule,
    ThemeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgHttpLoaderModule.forRoot(),
    NgbModule
  ],
  providers: [
  {
    provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true
  }],
  bootstrap: [AppComponent],
})
export class AppModule {}
