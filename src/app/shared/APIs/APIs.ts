// User auth login and Common API endpoint
export const AUTHLOGIN = `/auth/login`;
export const RESETPASSWORD = '/reset/reset-password';
export const CHANGEPASSWORD = '/auth/change-password';
export const VALIDATEPASSWORD = '/auth/validate-password';
//User Account related endpoint
export const GETACCOUNT = `/account`;
export const POSTACCOUNT = `/account/add_new_account`;
export const DELETEACCOUNT = `/account/delete-account/`;
export const PUTACCOUNT = `/account/update-account/`;

//User Role related endpoint
export const GETROLES = `/role/roles`;
export const GETALLUSERS = `/user`;
export const ADDNEWUSER = `/user/add-user`;
export const DELETEUSER = `/user/delete-user/`;
export const UPDATEUSER = `/user/update-user/`;
export const USERSHIERARCHY = `/user/users-hierarchy`;

//User Common related endpoint
export const UPDATELASTLOGINDATE = `/common/update-lastlogin-date`;
export const GETCOUNTRIES = `/common/countries`;
export const GETSTATES = `/common/states`;
export const GETCITIES = `/common/cities`;

//Client related endpoint
export const GETCLIENT = '/client/get-client';
export const GETCLIENTS = '/client';
export const ALLCLIENTS = '/client/get-All-clients';
export const PUTCLIENTDETAIL = '/client/update-client/';
export const POSTCLIENTS = `/client/add-client`;
export const DELETECLIENT = `/client/delete-client/`;
export const EXPIRYNOTIFICATIONBYCLIENTID = `/client/get-client-expiry-notification/`
export const HIERARCHYBYCLIENT = `/client/get-client-organization-hierarchy/`

//Consultant related endpoint
export const GETCONSULTANT = `/consultant/get-consultant`;
export const GETCONSULTANTS = `/consultant`;
export const POSTCONSULTANTS = `/consultant/add-consultant`;
export const DELETECONSULTANTS = `/consultant/delete-consultant/`;
export const UPDATECONSULTANTS = `/consultant/update-consultant/`

//employee related endpoint
export const GETEMPLOYEE = `/employee/get-employee`;
export const GETEMPLOYEES = `/employee`;
export const POSTEMPLOYEE = `/employee/add-new-employee`;
export const UPDATEEMPLOYEE = `/employee/update-employee/`;
export const DELETEEMPLOYEE = `/employee/delete-employee/`;
export const GETEMPDEPARTMENT = `/employee/employee-department`;
export const POSTEMPDEPARTMENT = `/employee/add-department`;
export const GETEMPDESIGNATION = `/employee/employee-designation`;
export const POSTEMPDESIGNATION = `/employee/add-designation`;

//Client-Employee related endpoint
export const GETCLIENTEMPLOYEE = `/clientsEmployee`;
export const POSTCLIENTEMPLOYEE = `/clientsEmployee/add-new-clientEmployee`;
export const UPDATECLIENTEMPLOYEE = `/clientsEmployee/update-clientEmployee`;
export const DELETECLIENTEMPLOYEE = `/clientsEmployee/delete-clientEmployee`;
export const GETCLIENTEMPDEPARTMENT = `/clientsEmployee/employee-department`;
export const POSTCLIENTEMPDEPARTMENT = `/clientsEmployee/add-department`;
export const GETCLIENTEMPDESIGNATION = `/clientsEmployee/employee-designation`;
export const POSTCLIENTEMPDESIGNATION = `/clientsEmployee/add-designation`;
export const GETALLCLIENTEMPLOYEES = `/clientsEmployee/get-all-clients-employee`;
export const GETCLIENTEMPLOYEES = `/clientsEmployee/getClientsEmployees`;
export const GETCLIENTEMPLOYEESBYCLIENTID = `/clientsEmployee`;

//section-master endPoint
export const GETALLSECTIONS =`/section`;
export const GET_ALL_SECTIONS = `/section/getAll-section`;
export const POSTSECTION =`/section/add-section`;
export const UPDATESECTION =`/section/update-section/`;
export const DELETESECTION =`/section/delete-section/`;

//sub-section endpoint
export const GETALLSUBSECTIONS = `/subsection`;
export const POSTSUBSECTIONS = `/subsection/add-new-subsection/`;
export const UPDATESUBSECTIONS = `/subsection/update-subsection/`;
export const DELETESUBSECTIONS = `/subsection/delete-subsection/`;
export const GETALLSUBSECTION = `/subsection/getAll-sub-section`;
export const POSTSUBSECTIONSTATUS = `/subsection/color`;


//question-master endPoint
export const GETALLQUESTIONS = `/question`;
export const POSTQUESTION = `/question/add-question`;
export const UPDATEQUESTION = `/question/update-question/`;
export const DELETEQUESTION = `/question/delete-question/`;
export const GETALLANSWERS = `/answers`;
export const GETLEFTNAV = '/question/left-nav';
export const GETSUBSECTIONSBYSECTION =`/question/sectionId/`;
export const GETQUESTIONSBYSUBSECTION = `/question/subSectionId`;
export const GETQUESTIONSBYSECTION = `/question/secId`;
export const POSTVGAUDITREPORTOBJ = `/question/sectionsProcess`;
export const POSTVGAUDITREPORTOBJFORCOMPETENCY = `/question/sectionsProcessForCompetency`;
export const POSTAVERAGEOFQUESTION = `/question/average`;
export const GETREPORTANSWERS = `/question/getReportAnswers`;

//employee performance
export const GETQUESTIONOFEMPLOYEEPERFORMANCE = `/employeePerformance/getQuestionsOfEmployeePerformance`;
export const GETEMPLOYEEPERFORMANCEUSERDETAILS = `/employeePerformance/addEmployeePerformanceUserDetails`;
export const GETEMPLOYEEPERFORMANCEUSERS = `/employeePerformance/getEmployeePerformanceUsers`;
export const GETALLEMPLOYEEPERFORMANCEUSERS = `/employeePerformance/getAllEmployeePerformanceUsers`;
export const GETEMPLOYEESOFEMPLOYEEPERFORMANCEBYID = `/employeePerformance/`;
export const GETEMPLOYEEOFMANAGER = `/employeePerformance/getEmployeesListingOfManager`;
export const DELETEEMPLOYEEPERFORMANCEQUESTION = `/employeePerformance/delete-employee-performance`;
export const GETALLEMPLOYEEPERFORMANCEUSERSBYID = `/employeePerformance/getAllEmployeePerformanceUser`;


// create pdf
export const CREATEPDF = `/pdf/genrate-pdf`;

//VGA-Auditor endPoint
export const GETALLVGAAUDITORS = `/auditor/getAll-auditors`;

//Report -Answer-Value endPoint
export const POSTREPORTANSWERVALUE = `/answervalues/add-answers`;
export const GETREPORTANSWERDETAILSBYQUESTION = `/answervalues`;
export const POSTEMPLOYEEPERFORMANCE = `/employeePerformance/add-answers`;

//VG-Audit endPoint
export const GETALLVGAUDIT = '/audit';
export const POSTVGAUDITREPORT = '/audit/add-new-vgaudit';
export const UPDATEVGAUDIT = '/audit/update-vgaudit';
export const GETVGAUDITBYID = '/audit';
export const GETSTATUSDETAIL = '/audit/getStatusReportsOfClient';
export const GETVGAREPORTBYCONSULTANT = '/audit/getVgaAuditReportsOfClient';
export const GETVGAUDITREPORTSOFCLIENTSBYUSERID = '/audit/getVgaAuditReportsOfClientsByUserId';


//Country, State and City endPoints
export const GETCOUNTRYBYID = '/countries';
export const GETALLCOUNTRIES = '/countries/getAll-countries';
export const POSTALLCOUNTRIES = '/countries/add-countries';
export const UPDATECOUNTRIES = '/countries/update-countries';
export const DELETECOUNTRIES = '/countries/delete-countries';

export const GETSTATESBYID = '/states';
// export const GETALLSTATES = '/states/states';
export const GETALLSTATES = '/states/getAll-state';
export const POSTALLSTATES = '/states/add-state';
export const DELETESTATES = '/states/delete-state';
export const UPDATESTATES = '/states/update-state';

export const GETCITIESBYID = '/cities';
export const POSTALLCITIES = '/cities/add-cities';
export const DELETECITIES = '/cities/delete-cities';
export const UPDATECITIES = '/cities/update-cities';

// Plan Of Actions (Section-5)
export const ADDANSWERSFORSECTION5 = '/planofActionAndRecommendation/add-answers';
export const GETDATAOFPLANOFACTION = '/planofActionAndRecommendation';

