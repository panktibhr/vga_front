import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';
import { environment } from 'src/environments/environment';


@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  constructor(private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (sessionStorage.getItem('token')) {
     // let token = CryptoJS.DES.decrypt(sessionStorage.getItem('token'), environment.SECRET_KEY).toString();
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      })
      // console.log("You are in HTTP_INTERCEPTORS Services..!");
    } else {
      request = request.clone({
        setHeaders: {
          Authorization: ``
        }
      })
    }
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpErrorResponse) {
          console.log('event ===>', event);
          if (event.status === 401) {
            sessionStorage.clear();
            this.router.navigate(['/login'])
          }
        }
        return event;
      }));
  }
}
