import { TestBed } from '@angular/core/testing';

import { VgaAuditorService } from './vga-auditor.service';

describe('VgaAuditorService', () => {
  let service: VgaAuditorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VgaAuditorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
