import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GETALLVGAAUDITORS } from '../../APIs/APIs';
import { VGAAuditor } from '../../models/vga-auditor';

@Injectable({
  providedIn: 'root'
})
export class VgaAuditorService {

  constructor(private httpClient: HttpClient) { }

  getAllVGAAuditors(): Observable<VGAAuditor>{
    return this.httpClient.get<VGAAuditor>(`${environment.BASE_URL}${GETALLVGAAUDITORS}`)
  }
}
