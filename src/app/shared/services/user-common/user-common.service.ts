import { UPDATELASTLOGINDATE, RESETPASSWORD, CHANGEPASSWORD, GETCOUNTRIES, GETSTATES, GETCITIES, VALIDATEPASSWORD } from '../../APIs/APIs';
import { environment } from './../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserCommonService {
  constructor(private httpClient: HttpClient) { }

  userUpdateLastLoginDate(userId: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.BASE_URL}${UPDATELASTLOGINDATE}`, { id: userId }).pipe(catchError(this.handleError));
  }

  userResetPassword(email:string): Observable<any> {
    return this.httpClient.post<any>(`${environment.BASE_URL}${RESETPASSWORD}`, {email}).pipe(catchError(this.handleError));
  }
  userResetOldPassword(email:string): Observable<any> {
    return this.httpClient.post<any>(`${environment.BASE_URL}${RESETPASSWORD}`, {email}).pipe(catchError(this.handleError));
  }

  userChangePassword(userId: string, password: string) {
    const userObj = {
      id: userId,
      password: password
    }
    return this.httpClient.put(`${environment.BASE_URL}${CHANGEPASSWORD}`, userObj).pipe(catchError(this.handleError));
  }

  getCountries(){
    return this.httpClient.get(`${environment.BASE_URL}${GETCOUNTRIES}`);
  }
  getStates(countryId){
    return this.httpClient.get(`${environment.BASE_URL}${GETSTATES}/${countryId}`);
  }
  getCities(statesId){
    return this.httpClient.get(`${environment.BASE_URL}${GETCITIES}/${statesId}`);
  }

  validatePassword(id,oldPassword){
    const passwordObj = {
      password: oldPassword
    }
    return this.httpClient.post(`${environment.BASE_URL}${VALIDATEPASSWORD}/${id}`, passwordObj);
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }

}
