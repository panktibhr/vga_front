import { DELETEEMPLOYEE, GETEMPDEPARTMENT, GETEMPDESIGNATION, GETEMPLOYEE, GETEMPLOYEES, POSTEMPDEPARTMENT, POSTEMPDESIGNATION } from '../../APIs/APIs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { POSTEMPLOYEE, UPDATEEMPLOYEE } from '../../APIs/APIs';
import { Employees } from '../../models/employees';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(private httpClient: HttpClient) { }

  getEmployee(): Observable<Employees>{
    return this.httpClient.get<Employees>(`${environment.BASE_URL}${GETEMPLOYEE}`)
  }
  getAllEmployees(employeeObj): Observable<Employees>{
    return this.httpClient.post<Employees>(`${environment.BASE_URL}${GETEMPLOYEES}`, employeeObj)
  }
  postEmployeeObj(employeeObj:Employees): Observable<Employees>{
    return this.httpClient.post<Employees>(`${environment.BASE_URL}${POSTEMPLOYEE}`, employeeObj)
  }
  updateEmployeeObj(id,employeeObj): Observable<Employees>{
    return this.httpClient.put<Employees>(`${environment.BASE_URL}${UPDATEEMPLOYEE}${id}`, employeeObj)
  }
  deleteEmployeeObj(id): Observable<Employees>{
    return this.httpClient.delete<Employees>(`${environment.BASE_URL}${DELETEEMPLOYEE}${id}`)
  }
  getEmployeesById(id): Observable<Employees[]> {
    return this.httpClient.get<Employees[]>(`${environment.BASE_URL}${GETEMPLOYEES}/${id}`)
  }
  getEmployeeDepartment():Observable<Employees> {
    return this.httpClient.get<Employees>(`${environment.BASE_URL}${GETEMPDEPARTMENT}`)
  }
  postEmployeeDepartment(empDepartmentObj): Observable<Employees>{
    return this.httpClient.post<Employees>(`${environment.BASE_URL}${POSTEMPDEPARTMENT}`,empDepartmentObj)
  }
  getEmployeeDesignation():Observable<Employees> {
    return this.httpClient.get<Employees>(`${environment.BASE_URL}${GETEMPDESIGNATION}`)
  }
  postEmployeeDesignation(empDesignationObj): Observable<Employees>{
    return this.httpClient.post<Employees>(`${environment.BASE_URL}${POSTEMPDESIGNATION}`,empDesignationObj)
  }
}
