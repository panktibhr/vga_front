import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DELETECOUNTRIES, GETALLCOUNTRIES, GETCOUNTRYBYID, POSTALLCOUNTRIES, UPDATECOUNTRIES } from '../../APIs/APIs';
import { Countries } from '../../models/countries';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor(private http: HttpClient) { }

  postCountries(countryObj): Observable<Countries>{
    return this.http.post<Countries>(`${environment.BASE_URL}${POSTALLCOUNTRIES}`, countryObj);
  }

  getCountries(searchObj): Observable<Countries>{
    return this.http.post<Countries>(`${environment.BASE_URL}${GETCOUNTRYBYID}`, searchObj);
  }

  updateCountry(id, countryObj): Observable<Countries[]>{
    return this.http.put<Countries[]>(`${environment.BASE_URL}${UPDATECOUNTRIES}/${id}`, countryObj);
  }

  deleteCountry(id): Observable<Countries>{
    return this.http.delete<Countries>(`${environment.BASE_URL}${DELETECOUNTRIES}/${id}`);
  }

  getAllCountries(): Observable<Countries>{
    return this.http.get<Countries>(`${environment.BASE_URL}${GETALLCOUNTRIES}` );
  }
}
