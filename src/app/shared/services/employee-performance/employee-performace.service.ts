import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { DELETEEMPLOYEEPERFORMANCEQUESTION, GETALLEMPLOYEEPERFORMANCEUSERS, GETEMPLOYEEOFMANAGER, GETEMPLOYEEPERFORMANCEUSERDETAILS, GETEMPLOYEEPERFORMANCEUSERS, GETEMPLOYEESOFEMPLOYEEPERFORMANCEBYID, GETQUESTIONOFEMPLOYEEPERFORMANCE, GETALLEMPLOYEEPERFORMANCEUSERSBYID } from "../../APIs/APIs";
import { EmployeePerformance } from "../../models/employee-performance";

@Injectable({
  providedIn: 'root'
})
export class EmployeePerformanceService {

  constructor(private httpClient: HttpClient) { }

  getQuestionOfEmployeePerformance(sectionId){
    return this.httpClient.post(`${environment.BASE_URL}${GETQUESTIONOFEMPLOYEEPERFORMANCE}`, sectionId);
  }
  getAnswersOfEmployeePerformance(employeeId){
    return this.httpClient.post(`${environment.BASE_URL}${GETEMPLOYEEPERFORMANCEUSERDETAILS}`, employeeId);
  }
  getEmployeesListing(questionId, vgAuditNo, employeeListObj):Observable<EmployeePerformance>{
    return this.httpClient.post<EmployeePerformance>(`${environment.BASE_URL}${GETEMPLOYEEPERFORMANCEUSERS}/${questionId}/${vgAuditNo}`, employeeListObj);
  }
  // getAllEmployeesListing(questionId, employeeListObj):Observable<EmployeePerformance>{
  //   return this.httpClient.post<EmployeePerformance>(`${environment.BASE_URL}${GETALLEMPLOYEEPERFORMANCEUSERS}/${questionId}`, employeeListObj);
  // }

  getAllEmployeesListing(){
    return this.httpClient.get(`${environment.BASE_URL}${GETALLEMPLOYEEPERFORMANCEUSERS}`);
  }

  getAllEmployeePerformanceUsers(questionId, vgaAuditNo){
    return this.httpClient.get(`${environment.BASE_URL}${GETALLEMPLOYEEPERFORMANCEUSERSBYID}/${questionId}/${vgaAuditNo}`);
  }

  getEmployeesOfEmployeePerformanceById(id){
    return this.httpClient.get(`${environment.BASE_URL}${GETEMPLOYEESOFEMPLOYEEPERFORMANCEBYID}${id}`);
  }
  getManagerEmployeePerformance(vgAuditNo){
    return this.httpClient.post(`${environment.BASE_URL}${GETEMPLOYEEOFMANAGER}`, { 'vgAuditNo': vgAuditNo });
  }
  deleteEmployeePerformanceQuestion(id): Observable<EmployeePerformance>{
    return this.httpClient.delete<EmployeePerformance>(`${environment.BASE_URL}${DELETEEMPLOYEEPERFORMANCEQUESTION}/${id}`);
  }
}
