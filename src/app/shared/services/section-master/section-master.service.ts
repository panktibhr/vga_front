import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DELETESECTION, GETALLSECTIONS, GET_ALL_SECTIONS, POSTSECTION, UPDATESECTION } from '../../APIs/APIs';
import { Sections } from '../../models/sections';

@Injectable({
  providedIn: 'root'
})
export class SectionMasterService {

  constructor(private httpClient: HttpClient) { }

  getALLSections(sectionObj): Observable<Sections>{
    return this.httpClient.post<Sections>(`${environment.BASE_URL}${GETALLSECTIONS}`,sectionObj);
  }
  getSectionById(id):Observable<Sections[]>{
    return this.httpClient.get<Sections[]>(`${environment.BASE_URL}${GETALLSECTIONS}/${id}`);
  }
  postSectionObj(sectionObj: Sections): Observable<Sections>{
    return this.httpClient.post<Sections>(`${environment.BASE_URL}${POSTSECTION}`,sectionObj);
  }
  updateSectionObj(id,sectionObj): Observable<Sections>{
    return this.httpClient.put<Sections>(`${environment.BASE_URL}${UPDATESECTION}${id}`, sectionObj)
  }
  deleteSectionObj(id): Observable<Sections>{
    return this.httpClient.delete<Sections>(`${environment.BASE_URL}${DELETESECTION}${id}`)
  }
  getAllSec(): Observable<Sections[]>{
    return this.httpClient.get<Sections[]>(`${environment.BASE_URL}${GET_ALL_SECTIONS}`)
  }
  // postSection(id): Observable<Sections>{
  //   return this.httpClient.get<Sections>(`${environment.BASE_URL}${GETSECTIONBYID}${id}`);
  // }

}
