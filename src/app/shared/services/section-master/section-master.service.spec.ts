import { TestBed } from '@angular/core/testing';

import { SectionMasterService } from './section-master.service';

describe('SectionMasterService', () => {
  let service: SectionMasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SectionMasterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
