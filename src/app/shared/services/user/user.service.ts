import { User } from 'src/app/shared/models/userAuth';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ADDNEWUSER, DELETEUSER, GETALLUSERS, GETROLES, UPDATEUSER, USERSHIERARCHY } from '../../APIs/APIs';
// import { User } from '../models/userAuth';
import { UserRole } from '../../models/userRole';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // public currentUserSubject: BehaviorSubject<User>;
  // public currentUserToken: BehaviorSubject<string>;
  // public currentUser: Observable<User>;

  constructor(private httpClient: HttpClient, private router: Router) {
    // this.currentUserSubject = new BehaviorSubject<User>(
    //   JSON.parse(JSON.stringify(localStorage.getItem("id")))
    // );
    // this.currentUserToken = new BehaviorSubject<string>(
    //   localStorage.getItem("token")
    // );
    // this.currentUser = this.currentUserSubject.asObservable();
  }

  getRole(): Observable<UserRole> {
    return this.httpClient.get<UserRole>(`${environment.BASE_URL}${GETROLES}`);
  }
  getAllUsers(userObj): Observable<User> {
    return this.httpClient.post<User>(`${environment.BASE_URL}${GETALLUSERS}`, userObj);
  }
  addNewUser(userObj): Observable<User> {
    return this.httpClient.post<User>(`${environment.BASE_URL}${ADDNEWUSER}`, userObj);
  }
  updateUserDetailObj(id,userObj:User):Observable<any>{
    return this.httpClient.put<User>(`${environment.BASE_URL}${UPDATEUSER}${id}`,userObj);
  }
  deleteUser(id): Observable<User> {
    return this.httpClient.delete<User>(`${environment.BASE_URL}${DELETEUSER}${id}`);
  }
  getUsersHierarchy(){
    return this.httpClient.get(`${environment.BASE_URL}${USERSHIERARCHY}`)
  }

}
