import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { POSTALLCITIES, GETCITIESBYID, UPDATECITIES, DELETECITIES } from '../../APIs/APIs';
import { Cities } from '../../models/cities';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  constructor(private http: HttpClient) { }

  postCities(cityObj): Observable<Cities>{
    return this.http.post<Cities>(`${environment.BASE_URL}${POSTALLCITIES}`, cityObj)
  }

  getAllCities(cityObj): Observable<Cities>{
    return this.http.post<Cities>(`${environment.BASE_URL}${GETCITIESBYID}`, cityObj);
  }

  getCities(stateId){
    return this.http.get(`${environment.BASE_URL}${GETCITIESBYID}/${stateId}`);
  }

  updateCities(id, cityObj): Observable<Cities>{
    return this.http.put<Cities>(`${environment.BASE_URL}${UPDATECITIES}/${id}`, cityObj);
  }

  deleteCities(id): Observable<Cities>{
    return this.http.delete<Cities>(`${environment.BASE_URL}${DELETECITIES}/${id}`);
  }
}
