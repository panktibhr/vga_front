import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { clientEmployees } from '../../models/client-employee';
import { environment } from 'src/environments/environment';
import {
  GETCLIENTEMPLOYEE,
  POSTCLIENTEMPLOYEE,
  POSTCLIENTEMPDEPARTMENT,
  POSTCLIENTEMPDESIGNATION,
  GETCLIENTEMPDEPARTMENT,
  GETCLIENTEMPDESIGNATION,
  UPDATECLIENTEMPLOYEE,
  DELETECLIENTEMPLOYEE,
  GETALLCLIENTEMPLOYEES, GETCLIENTEMPLOYEES, GETCLIENTEMPLOYEESBYCLIENTID
} from '../../APIs/APIs';

@Injectable({
  providedIn: 'root',
})
export class ClientEmployeeService {
  constructor(private httpClient: HttpClient) {}

  getClientEmployees(clientEmpObj): Observable<clientEmployees> {
    return this.httpClient.post<clientEmployees>(`${environment.BASE_URL}${GETCLIENTEMPLOYEE}`,clientEmpObj);
  }
  postClientEmployeeObj(clientEmpObj: clientEmployees): Observable<clientEmployees> {
    return this.httpClient.post<clientEmployees>(`${environment.BASE_URL}${POSTCLIENTEMPLOYEE}`,clientEmpObj);
  }
  updateClientEmployeeObj(id, clientEmpObj): Observable<clientEmployees> {
    return this.httpClient.put<clientEmployees>(`${environment.BASE_URL}${UPDATECLIENTEMPLOYEE}/${id}`,clientEmpObj);
  }
  deleteClientEmployeeObj(id): Observable<clientEmployees> {
    return this.httpClient.delete<clientEmployees>(`${environment.BASE_URL}${DELETECLIENTEMPLOYEE}/${id}`);
  }
  getClientEmployeesById(id): Observable<clientEmployees[]> {
    return this.httpClient.get<clientEmployees[]>(`${environment.BASE_URL}${GETCLIENTEMPLOYEE}/${id}`);
  }
  getClientEmployeeDepartment(): Observable<clientEmployees> {
    return this.httpClient.get<clientEmployees>(`${environment.BASE_URL}${GETCLIENTEMPDEPARTMENT}`);
  }
  postClientEmployeeDepartment(clientEmpmpDepartmentObj): Observable<clientEmployees> {
    return this.httpClient.post<clientEmployees>(`${environment.BASE_URL}${POSTCLIENTEMPDEPARTMENT}`,clientEmpmpDepartmentObj);
  }
  getClientEmployeeDesignation(): Observable<clientEmployees> {
    return this.httpClient.get<clientEmployees>(`${environment.BASE_URL}${GETCLIENTEMPDESIGNATION}`);
  }
  postClientEmployeeDesignation(clientEmpDesignationObj): Observable<clientEmployees> {
    return this.httpClient.post<clientEmployees>(`${environment.BASE_URL}${POSTCLIENTEMPDESIGNATION}`,clientEmpDesignationObj);
  }
  getClientEmployee() : Observable<clientEmployees>{
    // return this.httpClient.post<clientEmployees>(`${environment.BASE_URL}${GETALLCLIENTEMPLOYEES}`, {'questionId' : questionId, 'vgaAuditNo': vgaAuditNo});
    return this.httpClient.get<clientEmployees>(`${environment.BASE_URL}${GETALLCLIENTEMPLOYEES}`);
  }
  getAllClientEmployee(){
    return this.httpClient.get(`${environment.BASE_URL}${GETCLIENTEMPLOYEES}`);
  }

  getClientEmployeesByclientId(clientId, clientEmployeesObj){
    return this.httpClient.post(`${environment.BASE_URL}${GETCLIENTEMPLOYEESBYCLIENTID}/${clientId}`, clientEmployeesObj);
  }
  getClientEmployeesByclientIdAndEmployeeLevel(clientId, empLevel, clientEmployeesObj){
    return this.httpClient.post(`${environment.BASE_URL}${GETCLIENTEMPLOYEESBYCLIENTID}/${clientId}/${empLevel}`, clientEmployeesObj);
  }
}
