import { TestBed } from '@angular/core/testing';

import { ClientEmployeeService } from './client-employee.service';

describe('ClientEmployeeService', () => {
  let service: ClientEmployeeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClientEmployeeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
