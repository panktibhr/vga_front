import { ALLCLIENTS, DELETECLIENT, EXPIRYNOTIFICATIONBYCLIENTID, GETCLIENT, GETCLIENTS, HIERARCHYBYCLIENT, POSTCLIENTS, PUTCLIENTDETAIL } from '../../APIs/APIs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GETALLUSERS } from '../../APIs/APIs';
import { Clients } from '../../models/clients';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private httpClient: HttpClient) { }

  getAllClients(clientsObj): Observable<Clients> {
    return this.httpClient.post<Clients>(`${environment.BASE_URL}${GETCLIENTS}`,clientsObj);
  }
  getClientsById(id): Observable<Clients[]> {
    return this.httpClient.get<Clients[]>(`${environment.BASE_URL}${GETCLIENTS}/${id}`);
  }
  updateClientById(id, clientObj): Observable<Clients[]> {
    return this.httpClient.put<Clients[]>(`${environment.BASE_URL}${PUTCLIENTDETAIL}${id}`, clientObj);
  }
  postClientDetailsObj(clientObj: Clients): Observable<Clients>{
    return this.httpClient.post<Clients>(`${environment.BASE_URL}${POSTCLIENTS}`, clientObj);
  }
  deleteClientDetailsObj(id): Observable<Clients>{
    return this.httpClient.delete<Clients>(`${environment.BASE_URL}${DELETECLIENT}${id}`);
  }
  getClientForUsers(userId): Observable<Clients>{
    return this.httpClient.post<Clients>(`${environment.BASE_URL}${GETCLIENT}`, {'user_id': userId});
  }
  getClientForAdmin(userId, clientId): Observable<Clients>{
    return this.httpClient.post<Clients>(`${environment.BASE_URL}${GETCLIENT}`, {'user_id': userId, 'client_id': clientId});
  }
  allClients(): Observable<Clients>{
    return this.httpClient.get<Clients>(`${environment.BASE_URL}${ALLCLIENTS}`);
  }
  getExpiryNotification(id):Observable<any> {
    return this.httpClient.get<any>(`${environment.BASE_URL}${EXPIRYNOTIFICATIONBYCLIENTID}${id}`);
  }
  getHierarchyByClient(id):Observable<any> {
    return this.httpClient.get<any>(`${environment.BASE_URL}${HIERARCHYBYCLIENT}${id}`);
  }
  downloadHierarchyByClient(id,isDownload:boolean):Observable<any> {
    return this.httpClient.get<any>(`${environment.BASE_URL}${HIERARCHYBYCLIENT}${id}/${isDownload}`,{'responseType':'arraybuffer' as 'json'});
  }
}
