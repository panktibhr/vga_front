import { POSTSUBSECTIONS, UPDATESUBSECTIONS, DELETESUBSECTIONS, GETALLSUBSECTION, POSTSUBSECTIONSTATUS } from '../../APIs/APIs';
import { HttpClient } from '@angular/common/http';
import { SubSections } from './../../models/sub-sections';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GETALLSUBSECTIONS } from '../../APIs/APIs';

@Injectable({
  providedIn: 'root'
})
export class SubSectionService {

  constructor(private httpClient: HttpClient) { }

  getAllSubSections(subSectionObj): Observable<SubSections>{
    return this.httpClient.post<SubSections>(`${environment.BASE_URL}${GETALLSUBSECTIONS}`, subSectionObj)
  }
  postSubSections(subSectionObj:SubSections): Observable<SubSections>{
    return this.httpClient.post<SubSections>(`${environment.BASE_URL}${POSTSUBSECTIONS}`, subSectionObj)
  }
  updateSubSections(id, subSectionObj): Observable<SubSections>{
    return this.httpClient.put<SubSections>(`${environment.BASE_URL}${UPDATESUBSECTIONS}${id}`, subSectionObj)
  }
  deleteSubSections(id): Observable<SubSections>{
    return this.httpClient.delete<SubSections>(`${environment.BASE_URL}${DELETESUBSECTIONS}${id}`)
  }
  getSubSectionById(id): Observable<SubSections>{
    return this.httpClient.get<SubSections>(`${environment.BASE_URL}${GETALLSUBSECTIONS}/${id}`)
  }
  getAllSubSection(): Observable<SubSections>{
    return this.httpClient.get<SubSections>(`${environment.BASE_URL}${GETALLSUBSECTION}`)
  }
  postSubSectionStatus(subSectionObj): Observable<SubSections>{
    return this.httpClient.post<SubSections>(`${environment.BASE_URL}${POSTSUBSECTIONSTATUS}`, subSectionObj)
  }
}
