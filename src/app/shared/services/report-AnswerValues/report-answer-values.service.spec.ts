import { TestBed } from '@angular/core/testing';

import { ReportAnswerValuesService } from './report-answer-values.service';

describe('ReportAnswerValuesService', () => {
  let service: ReportAnswerValuesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportAnswerValuesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
