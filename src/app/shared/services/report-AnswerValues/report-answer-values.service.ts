import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ADDANSWERSFORSECTION5, GETDATAOFPLANOFACTION, GETREPORTANSWERDETAILSBYQUESTION, POSTEMPLOYEEPERFORMANCE, POSTREPORTANSWERVALUE } from '../../APIs/APIs';
@Injectable({
  providedIn: 'root'
})
export class ReportAnswerValuesService {

  constructor(private httpClient: HttpClient) { }

  reportAnswerValue(answerValue){
    return this.httpClient.post(`${environment.BASE_URL}${POSTREPORTANSWERVALUE}`, answerValue);
  }

  getReportAnswersByQuestion(id, vgAuditNo){
    return this.httpClient.get(`${environment.BASE_URL}${GETREPORTANSWERDETAILSBYQUESTION}/${id}/${vgAuditNo}`);
  }

  employeePerformanceValue(answerValue){
    return this.httpClient.post(`${environment.BASE_URL}${POSTEMPLOYEEPERFORMANCE}`, answerValue)
  }

  // Plan of Actions and Recommendations
  addReportAnswerValue(answerValue){
    return this.httpClient.post(`${environment.BASE_URL}${ADDANSWERSFORSECTION5}`, answerValue);
  }

  getAnswerValues(questionId, vgaReportNumber){
    return this.httpClient.get(`${environment.BASE_URL}${GETDATAOFPLANOFACTION}/${questionId}/${vgaReportNumber}`);
  }

}
