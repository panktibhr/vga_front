import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DELETESTATES, GETSTATESBYID, POSTALLSTATES, UPDATESTATES, GETALLSTATES } from '../../APIs/APIs';
import { States } from '../../models/states';

@Injectable({
  providedIn: 'root'
})
export class StatesService {

  constructor(private http: HttpClient) { }

  postState(stateObj): Observable<States>{
    return this.http.post<States>(`${environment.BASE_URL}${POSTALLSTATES}`, stateObj)
  }

  getAllStates(): Observable<States>{
    return this.http.get<States>(`${environment.BASE_URL}${GETALLSTATES}`);
  }

  getState(countryId){
    return this.http.get(`${environment.BASE_URL}${GETSTATESBYID}/${countryId}`);
  }

  searchStates(searchObj): Observable<States>{
    return this.http.post<States>(`${environment.BASE_URL}${GETSTATESBYID}`, searchObj);
  }

  updateStates(id, statesObj): Observable<States>{
    return this.http.put<States>(`${environment.BASE_URL}${UPDATESTATES}/${id}`, statesObj);
  }

  deleteStates(id): Observable<States>{
    return this.http.delete<States>(`${environment.BASE_URL}${DELETESTATES}/${id}`);
  }

}
