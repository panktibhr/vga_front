import { TestBed } from '@angular/core/testing';

import { VgAuditService } from './vg-audit.service';

describe('VgAuditService', () => {
  let service: VgAuditService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VgAuditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
