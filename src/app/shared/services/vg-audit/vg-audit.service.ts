import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GETALLVGAUDIT, POSTVGAUDITREPORT, GETVGAUDITBYID, UPDATEVGAUDIT, GETSTATUSDETAIL, GETVGAREPORTBYCONSULTANT, GETVGAUDITREPORTSOFCLIENTSBYUSERID } from '../../APIs/APIs';
import { vgAudit } from './../../models/vg-audit';

@Injectable({
  providedIn: 'root'
})
export class VgAuditService {

  constructor(private httpClient: HttpClient) { }

  getAllVgAudits(clientId, vgAuditObj):Observable<vgAudit>{
    return this.httpClient.post<vgAudit>(`${environment.BASE_URL}${GETALLVGAUDIT}/${clientId}`, vgAuditObj)
  }

  postVgAuditReport(vgAuditObj):Observable<vgAudit>{
    return this.httpClient.post<vgAudit>(`${environment.BASE_URL}${POSTVGAUDITREPORT}`, vgAuditObj)
  }

  getVgAuditById(id): Observable<vgAudit[]>{
    return this.httpClient.get<vgAudit[]>(`${environment.BASE_URL}${GETVGAUDITBYID}/${id}`);
  }

  getVgAuditByConsultantId(consultantId): Observable<vgAudit[]>{
    return this.httpClient.get<vgAudit[]>(`${environment.BASE_URL}${GETVGAREPORTBYCONSULTANT}/${consultantId}`);
  }

  updateVgAudits(id, vgAuditObj):Observable<vgAudit[]>{
    return this.httpClient.put<vgAudit[]>(`${environment.BASE_URL}${UPDATEVGAUDIT}/${id}`, vgAuditObj);
  }

  getStatusReportOfClient(clientId): Observable<vgAudit>{
    return this.httpClient.post<vgAudit>(`${environment.BASE_URL}${GETSTATUSDETAIL}`, {clientId: clientId});
  }

  getVgaAuditReportsOfClientsByUserId(vgAuditObj): Observable<vgAudit[]>{
    return this.httpClient.post<vgAudit[]>(`${environment.BASE_URL}${GETVGAUDITREPORTSOFCLIENTSBYUSERID}`, vgAuditObj);
  }
}
