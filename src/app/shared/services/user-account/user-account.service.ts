import { DELETEACCOUNT, GETACCOUNT, POSTACCOUNT, PUTACCOUNT } from '../../APIs/APIs';
import { Account } from './../../models/userAccount';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserAccountService {

  constructor(private httpClient: HttpClient) { }

  getAccountDetail(accountObj): Observable<Account> {
    return this.httpClient.post<Account>(`${environment.BASE_URL}${GETACCOUNT}`, accountObj).pipe(catchError(this.handleError));
  }
  postAccountDetailObj(accountObj: Account): Observable<Account> {
    return this.httpClient.post<Account>(`${environment.BASE_URL}${POSTACCOUNT}`, accountObj).pipe(catchError(this.handleError));
  }
  updateAccountDetailObj(id,accountObj:Account):Observable<any>{
    return this.httpClient.put<Account>(`${environment.BASE_URL}${PUTACCOUNT}${id}`,accountObj).pipe(catchError(this.handleError));
  }
  deleteAccountDetailObj(id): Observable<any>{
    return this.httpClient.delete<any>(`${environment.BASE_URL}${DELETEACCOUNT}${id}`).pipe(catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
