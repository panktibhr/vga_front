import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CREATEPDF, DELETEQUESTION, GETALLANSWERS, GETALLQUESTIONS, GETEMPLOYEEPERFORMANCEUSERDETAILS, GETEMPLOYEEPERFORMANCEUSERS, GETEMPLOYEESOFEMPLOYEEPERFORMANCEBYID, GETLEFTNAV, GETQUESTIONOFEMPLOYEEPERFORMANCE, GETQUESTIONSBYSECTION, GETQUESTIONSBYSUBSECTION, GETREPORTANSWERS, GETSUBSECTIONSBYSECTION, POSTAVERAGEOFQUESTION, POSTQUESTION, POSTVGAUDITREPORTOBJ, POSTVGAUDITREPORTOBJFORCOMPETENCY, UPDATEQUESTION } from '../../APIs/APIs';
import { AnswerValue, Questions } from '../../models/questions';
import { Sections } from '../../models/sections';

@Injectable({
  providedIn: 'root'
})
export class QuestionMasterService {

  constructor(private httpClient: HttpClient) { }

  getAllQuestions(quetionObj): Observable<Questions>{
    return this.httpClient.post<Questions>(`${environment.BASE_URL}${GETALLQUESTIONS}`,quetionObj);
  }
  getQuestionById(id): Observable<Questions[]>{
    return this.httpClient.get<Questions[]>(`${environment.BASE_URL}${GETALLQUESTIONS}/${id}`);
  }
  postQuestionObj(questionObj:Questions): Observable<Questions>{
    return this.httpClient.post<Questions>(`${environment.BASE_URL}${POSTQUESTION}`, questionObj);
  }
  updateQuestionObj(id,questionObj): Observable<Questions>{
    return this.httpClient.put<Questions>(`${environment.BASE_URL}${UPDATEQUESTION}${id}`, questionObj);
  }
  deleteQuestionObj(id): Observable<Questions>{
    return this.httpClient.delete<Questions>(`${environment.BASE_URL}${DELETEQUESTION}${id}`);
  }
  getAllAnswers():Observable<AnswerValue[]>{
    return this.httpClient.get<AnswerValue[]>(`${environment.BASE_URL}${GETALLANSWERS}`);
  }
  getLeftNavBar(){
    return this.httpClient.get(`${environment.BASE_URL}${GETLEFTNAV}`);
  }
  getSubSectionsBySection(secId): Observable<Sections>{
    return this.httpClient.get<Sections>(`${environment.BASE_URL}${GETSUBSECTIONSBYSECTION}${secId}`);
  }
  getQuestionsBySubSections(subSecId){
    return this.httpClient.get(`${environment.BASE_URL}${GETQUESTIONSBYSUBSECTION}/${subSecId}`);
  }
  getQuestionsBySections(secId){
    return this.httpClient.get(`${environment.BASE_URL}${GETQUESTIONSBYSECTION}/${secId}`);
  }
  postVgAuditReportObj(id, vgAuditObj){
    return this.httpClient.post(`${environment.BASE_URL}${POSTVGAUDITREPORTOBJ}/${id}`,vgAuditObj);
  }
  postVgAuditReportObjForCompetency(id, vgAuditObj){
    return this.httpClient.post(`${environment.BASE_URL}${POSTVGAUDITREPORTOBJFORCOMPETENCY}/${id}`,vgAuditObj);
  }
  postAverageOfQuestion(averageObj: any){
    return this.httpClient.post(`${environment.BASE_URL}${POSTAVERAGEOFQUESTION}`, averageObj);
  }
  generatePDF(pdfObj: any){
    return this.httpClient.post(`${environment.BASE_URL}${CREATEPDF}`, pdfObj,{'responseType':'arraybuffer' as 'json'});
  }

  getReportAnswers(ansObj: any){
    return this.httpClient.post(`${environment.BASE_URL}${GETREPORTANSWERS}`, ansObj);
  }

}
