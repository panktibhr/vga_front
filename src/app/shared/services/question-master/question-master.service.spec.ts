import { TestBed } from '@angular/core/testing';

import { QuestionMasterService } from './question-master.service';

describe('QuestionMasterService', () => {
  let service: QuestionMasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionMasterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
