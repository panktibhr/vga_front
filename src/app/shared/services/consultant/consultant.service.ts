import { POSTCONSULTANTS, DELETECONSULTANTS, UPDATECONSULTANTS, GETCONSULTANTS, GETCONSULTANT } from '../../APIs/APIs';
import { Consultant } from './../../models/consultant';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsultantService {

  constructor(private httpClient: HttpClient) { }

  getConsultant():Observable<Consultant>{
    return this.httpClient.get<Consultant>(`${environment.BASE_URL}${GETCONSULTANT}`);
  }
  getAllConsultants(consultantObj):Observable<Consultant>{
    return this.httpClient.post<Consultant>(`${environment.BASE_URL}${GETCONSULTANTS}`, consultantObj);
  }
  postConsultantObj(consultantObj: Consultant): Observable<Consultant>{
    return this.httpClient.post<Consultant>(`${environment.BASE_URL}${POSTCONSULTANTS}`, consultantObj);
  }
  deleteConsultantObj(id):Observable<Consultant>{
    return this.httpClient.delete<any>(`${environment.BASE_URL}${DELETECONSULTANTS}${id}`)
  }
  updateConsultantObj(id, consultantObj): Observable<Consultant[]>{
    return this.httpClient.put<Consultant[]>(`${environment.BASE_URL}${UPDATECONSULTANTS}${id}`, consultantObj);
  }
  getConsultantById(id):Observable<Consultant[]>{
    return this.httpClient.get<Consultant[]>(`${environment.BASE_URL}${GETCONSULTANTS}/${id}`);
  }
}
