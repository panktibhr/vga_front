export class clientEmployees{
  id?: String;
  clientEmpTitle?: String;
  clientEmpName?: String;
  clientEmpLevel?: String;
  designation?: String;
  deptDiv?: String;
  mobileNumber?: number;
  email?: String;
  address?: String;
  country?:String;
  state?:String;
  city_id?:String;
  zipCode?:String;
  username?: string;
  password?: String;
  parent_account_id?: string;
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: String;
  SearchText2: String;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 5;
    this.SortItem = "clientEmpName";
    this.SortOrder = "ASC";
    this.SearchText = "";
    this.SearchText2 = "";
  }
}
