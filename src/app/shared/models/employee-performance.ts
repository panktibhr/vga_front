export class EmployeePerformance{
  id?: String;
  name?: String;
  employeeId?: String;
  employeePerformanceQuestionId?: String;
  questionId?: any;
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: string;

  constructor() {
    this.PageNo = 0,
    this.PageSize = 10,
    this.SortItem = "name",
    this.SortOrder = "ASC",
    this.SearchText = ''
  }
}

