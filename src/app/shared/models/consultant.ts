export class Consultant{
  id?: String;
  company_name?: String;
  consultant_name?: String;
  address?: String;
  country?: String;
  city?: String;
  zip?: number;
  mobile?: number;
  email?: String;
  password?: String;
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: string;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 5;
    this.SortItem = 'consultant_name';
    this.SortOrder = 'ASC';
    this.SearchText = '';
  }
}
