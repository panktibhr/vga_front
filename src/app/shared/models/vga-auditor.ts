export class VGAAuditor{
  id?: String;
  auditorName?: String;
  phone?: String;
  mobileNumber?: String;
  email?: String;
  address?: String;
  country?:String;
  state?:String;
  city?:String;
  companyName?: String;
  zipCode?:String;
  parent_account_id?: string;
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: String;
  SearchText2: String;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 5;
    this.SortItem = "auditorName";
    this.SortOrder = "ASC";
    this.SearchText = "";
    this.SearchText2 = "";
  }
}
