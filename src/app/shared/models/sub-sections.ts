export class SubSections {
  id: String;
  sub_section_name: String;
  sub_section_description: String;
  parent_section_name: String;
  sequenceNumber: String;
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: string;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 5;
    this.SortItem = 'sub_section_name';
    this.SortOrder = 'ASC';
    this.SearchText = '';
  }
}
