export class Account {
  id?: string;
  name?: string;
  description?: string;
  email?: string;
  phone?: number;
  parent_account_id?: string;
  PageNo: number;
  PageSize: number;
  SortItem: String;
  SortOrder: String;
  SearchText: String;
  SearchEmail: String;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 5;
    this.SortItem = "username";
    this.SortOrder = "ASC";
    this.SearchText = '';
    this.SearchEmail = '';
  }


}
