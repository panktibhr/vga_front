export class Countries{
  id?: String;
  countryName?: String;
  SearchText?: string;
  PageNo: number;
  PageSize: number;
  // SortItem: String;
  // SortOrder: String;

  constructor(){
    this.PageNo = 0;
    this.PageSize = 30;
    this.SearchText = '';
    // this.SortItem = 'countryName';
    // this.SortOrder = 'ASC';
  }
}
