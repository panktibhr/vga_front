export class vgAudit {
  id?: String;
  // companyName?: String;
  clientId?: String;
  // consultantName?: String;
  vgaAuditNo?: any;
  fromDate?: any;
  toDate?: any;
  // overAllPerfomance?: String;

  constructor() {
    this.vgaAuditNo = '';
    this.fromDate = '';
    this.toDate = '';
  }
}

export class vgAuditDetails {
  id?: String;
  section_id?: String;
}
