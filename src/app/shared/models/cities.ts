export class Cities{
  id?: String;
  city_name?: String;
  states_id?: String;
  SearchText?: string;
  PageNo: number;
  PageSize: number;
  // SortItem: String;
  // SortOrder: String;

  constructor(){
    this.PageNo = 0;
    this.PageSize = 10;
    this.SearchText = '';
    // this.SortItem = 'countryName';
    // this.SortOrder = 'ASC';
  }
}
