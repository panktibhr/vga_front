export class Employees{
  id?: String;
  empTitle?: String;
  empName?: String;
  empLevel?: String;
  designation?: String;
  deptDiv?: String;
  reportingTo?: number;
  mobileNumber?: number;
  email?: String;
  address?: String;
  country?:String;
  state?:String;
  city_id?:String;
  zipCode?:String;
  username?: string;
  password?: String;
  parent_account_id?: string;
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: String;
  SearchText2: String;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 10;
    this.SortItem = "empName";
    this.SortOrder = "ASC";
    this.SearchText = "";
    this.SearchText2 = "";
  }
}
