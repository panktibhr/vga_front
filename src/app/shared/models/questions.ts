export class Questions{
  id?: String;
  sectionName?: String;
  subSectionName?: String;
  questionName?: String;
  answerControlType?: String;
  deafaultAnswerValue?:String;
  answerValueType?: String;
  subQuestionsList?: AnswerValue[];
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: string;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 5;
    this.SortItem = "questionName";
    this.SortOrder = "ASC";
    this.SearchText = '';
  }
}
export class AnswerValue{
  id?: String;
  title?: String;
  value?: String;
}
