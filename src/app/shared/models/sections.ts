export class Sections{
  id?: String;
  sectionName?: String;
  sectionDescription?: String;
  seqNo?: number;
  isDefault?: boolean;
  PageNo: number;
  PageSize: number;
  SortItem: string;
  SortOrder: string;
  SearchText: string;

  constructor() {
    this.PageNo = 0;
    this.PageSize = 5;
    this.SortItem = "sectionName";
    this.SortOrder = "ASC";
    this.SearchText = '';
  }
}
