import { AuthLoginService } from './../../shared/services/auth-login/auth-login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userName: string;
  userObj: any;
  cities: any = [];
  countries: any = [];
  states: any = [];
  parentAccountName: any;

  constructor(private router: Router, private authService: AuthLoginService) { }

  ngOnInit(): void {
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.parentAccountName = this.userObj.parentAccountName;
    this.userName = this.userObj.name;
  }

  logOut = () => {
    this.authService.logout();
    this.router.navigate(['auth/login']);
  }
}
