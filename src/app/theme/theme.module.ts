import { SlidebarComponent } from './slidebar/slidebar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { GravatarModule } from "ngx-gravatar";
import {MatTreeModule} from '@angular/material/tree';

@NgModule({
  declarations: [HeaderComponent, SlidebarComponent],
  imports: [CommonModule, RouterModule, GravatarModule, MatTreeModule],
  exports: [HeaderComponent, SlidebarComponent],
})
export class ThemeModule {}
