import { Sections } from './../../shared/models/sections';
import { SectionMasterService } from './../../shared/services/section-master/section-master.service';
import { EmployeesService } from './../../shared/services/employees/employees.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';
import { ConsultantService } from 'src/app/shared/services/consultant/consultant.service';
import { environment } from 'src/environments/environment';
import { SubSectionService } from 'src/app/shared/services/sub-section/sub-section.service';
import { SubSections } from 'src/app/shared/models/sub-sections';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { Questions } from 'src/app/shared/models/questions';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { DataService } from 'src/app/shared/services/data.service';
import { Subscription } from 'rxjs';

let TREE_DATA: any;

@Component({
  selector: 'app-slidebar',
  templateUrl: './slidebar.component.html',
  styleUrls: ['./slidebar.component.scss'],
})
export class SlidebarComponent implements OnInit {

  userObj: any;
  roleType: any;
  userEmail: any;
  clientId: any;
  consultantId: any;
  employeeId: any;
  sections$ = [];
  _sections: Sections;
  _subSections: SubSections;
  _questions: Questions;
  url: any;
  subSections$: any;
  questions$: any;
  sectionId: any;
  sections: any = [];
  question: any = [];
  getQuestionsBySubSection: any;
  subSectionId: any;
  subscription: Subscription;
  que: any;
  getQuestionsBysection: any;

  constructor(
    private clientService: ClientsService,
    private consultantService: ConsultantService,
    private employeeService: EmployeesService,
    private sectionService: SectionMasterService,
    private router: Router,
    private subSectionService: SubSectionService,
    private questionService: QuestionMasterService,
    private dataService: DataService
  ) {
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'),environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
    this.userEmail = this.userObj.email;
    this._sections = new Sections();
    this._subSections = new SubSections();
    this._questions = new Questions();
    this.url = this.router.url;
  }

  ngOnInit(): void {
    if (this.roleType === 'Client') {
      this.getClientDetails();
    } else if (this.roleType === 'Consultant') {
      this.getConsultantDetails();
      if (this.url === '/pages/VGAudit/VGAuditReport') {
        this.getSectionDetails(this._sections);
        this.getSubSectionDetails(this._subSections);
      }
    } else if (this.roleType === 'Employee') {
      this.getEmployeesObj();
    }
    this.getQuestionDetails(this._questions);
  }

  getClientDetails() {
    if(this.roleType === 'Super Admin'){
      let clientId = '';
      this.subscription = this.clientService.getClientForAdmin(this.userObj.id, clientId).subscribe((res: any) => {
        this.clientId = res.res.id;
        sessionStorage.setItem('clientId', this.clientId);
      });
    } else {
      this.subscription = this.clientService.getClientForUsers(this.userObj.id).subscribe((res: any) => {
        this.clientId = res.res.id;
        sessionStorage.setItem('clientId', this.clientId);
      })
    }
  }

  getConsultantDetails() {
    this.subscription = this.consultantService.getConsultant().subscribe((res: any) => {
      this.consultantId = res.res.id;
      localStorage.setItem('ConsultantId', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(this.consultantId), environment.SECRET_KEY.trim()).toString());
    });
  }

  getEmployeesObj() {
    this.subscription = this.employeeService.getEmployee().subscribe((res: any) => {
      this.employeeId = res.res.id;
    });
  }

  getSectionDetails(sectionObj) {
    this.subscription = this.sectionService.getALLSections(sectionObj).subscribe((res: any) => {
      this.sections$ = res.res.results;
    });
  }

  getSubSectionDetails(subSectionObj) {
    this.subscription = this.subSectionService.getAllSubSections(subSectionObj).subscribe((res: any) => {
      this.subSections$ = res.res.results;
    });
  }

  getQuestionDetails(questionObj) {
    this.subscription = this.questionService.getAllQuestions(questionObj).subscribe((res: any) => {
      if (res) {
        this.questions$ = res.res.results;
      }
    });
  }

  getSubSectionsOfSection(secId) {
    this.subscription = this.questionService.getSubSectionsBySection(secId).subscribe((res: any) => {
      if(res.res.length === 0){
        this.questionService.getQuestionsBySections(secId).subscribe((res: any) => {
          this.getQuestionsBysection = res.res;
          this.dataService.changeMessage(this.getQuestionsBysection);
          return res;
        })
      }
      this.sectionId = secId;
      this.router.navigate(['/pages/VGAudit/vgaudits', secId])
    })
  }

  getQuestionsOnclickOfSubSection(subSecId) {
    this.subscription = this.questionService.getQuestionsBySubSections(subSecId).subscribe((res: any) => {
      this.getQuestionsBySubSection = res.res;
      this.subSectionId = subSecId;
      this.router.navigate(['/pages/VGAudit/vgaudits', this.sectionId, subSecId])
      this.dataService.changeMessage(this.getQuestionsBySubSection);
    })
  }

  onClickOfQuestion(id) {
    this.subscription = this.questionService.getQuestionById(id).subscribe((res: any) => {
      this.question = res.res.question;
      if (res) {
        this.router.navigate(['/pages/VGAudit/vgaudits', this.sectionId, this.subSectionId, res.res.id, res.res.sequence_number])
      }
    })
  }

  onClickOfSectionQuestion(id, sequence_number){
    this.router.navigate(['/pages/VGAudit/vgaudits', this.sectionId, id, sequence_number]);
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

}
