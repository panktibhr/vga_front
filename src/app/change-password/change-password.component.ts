import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UserCommonService } from 'src/app/shared/services/user-common/user-common.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  oldPasswordHidden = true;
  passwordHidden = true;
  confirmPasswordHidden = true;
  submitted = false;
  returnUrl = '/signin';
  error = false;
  validationError = '';
  successMessage = '';
  resetId = '';
  showMsg: boolean = false;
  subscription: any;
  changePwd: Observable<any>;
  oldPassword: any;
  isOldPassword = false;
  userObj: any;
  userId: any;
  errorMessage: string;
  changePasswordSubscription : Subscription;
  validatePassSubscription : Subscription;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userCommonService: UserCommonService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.userId = this.userObj.id;
    this.subscription = this.activatedRoute.snapshot.queryParams['id'];
    this.changePasswordForm = this.formBuilder.group(
      {
        oldPassword: ['', [Validators.required]],
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]],
      },
      { validators: this.MatchPassword }
    );
  }

  MatchPassword(control: AbstractControl) {
    const password = control.get('password').value;
    const confirmPassword = control.get('confirmPassword').value;
    if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({ ConfirmPassword: true });
    } else {
      return null;
    }
  }

  get getFormControl() {
    return this.changePasswordForm.controls;
  }

  onSubmit() {
    this.error = false;
    this.submitted = true;
    this.successMessage = 'Passward change Successfully!';
    this.showMsg = true;

    if (this.changePasswordForm.invalid) {
      this.changePasswordForm.markAllAsTouched();
      this.submitted = false;
      return false;
    }

    this.changePasswordSubscription = this.userCommonService
      .userChangePassword(this.userId, this.getFormControl.password.value)
      .subscribe((data: any) => {
        setTimeout(() => {
          this.router.navigate(['pages/dashboard']);
        })
      });
  }

  toggleOldPassword = () => {
    this.oldPasswordHidden = !this.oldPasswordHidden;
  };

  togglePassword = () => {
    this.passwordHidden = !this.passwordHidden;
  };

  toggleConfirmPassword = () => {
    this.confirmPasswordHidden = !this.confirmPasswordHidden;
  };

  onOldPasswordCheack(event) {
    this.oldPassword = event.target.value;
    this.validatePassSubscription = this.userCommonService
      .validatePassword(this.userId, this.getFormControl.oldPassword.value)
      .subscribe((res) => {
        this.isOldPassword = false;
      }, (error: HttpErrorResponse) => {
        if (error) {
          this.isOldPassword = true;
          this.errorMessage = 'Old password didn not match';
        }
      });
  }
  ngOnDestroy(): void {
    this.changePasswordSubscription?.unsubscribe();
    this.validatePassSubscription?.unsubscribe();
  }
}
