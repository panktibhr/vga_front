import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { UserCommonService } from '../shared/services/user-common/user-common.service';

@Component({
  selector: 'app-reset-old-password',
  templateUrl: './reset-old-password.component.html',
  styleUrls: ['./reset-old-password.component.scss']
})
export class ResetOldPasswordComponent implements OnInit {

  resetOldPasswordForm: FormGroup;
  passwordHidden = true;
  confirmPasswordHidden = true;
  submitted = false;
  returnUrl = '/signin';
  error = false;
  validationError = '';
  successMessage = '';
  resetId = '';
  showMsg: boolean = false;
  subscription: any;
  changePwd: Observable<any>;
  oldPassword: any;
  isOldPassword = false;
  isLoading: boolean = false;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userCommonService: UserCommonService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.subscription = this.activatedRoute.snapshot.queryParams['id'];
    this.resetOldPasswordForm = this.formBuilder.group(
      {
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]],
      },
      { validators: this.MatchPassword }
    );
  }

  MatchPassword(control: AbstractControl) {
    const password = control.get('password').value;
    const confirmPassword = control.get('confirmPassword').value;
    if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({ ConfirmPassword: true });
    } else {
      return null;
    }
  }

  onSubmit() {
    this.error = false;
    this.submitted = true;
    this.showMsg = true;
    this.isLoading = true;
    this.successMessage = "Please wait!"

    if (this.resetOldPasswordForm.invalid) {
      this.resetOldPasswordForm.markAllAsTouched();
      this.submitted = false;
      return false;
    }

    this.userCommonService
      .userChangePassword(this.subscription, this.resetOldPasswordForm.controls.password.value)
      .subscribe((data: any) => {
        setTimeout(() => {
          this.isLoading = false;
          this.successMessage = 'Passward change Successfully!';
          this.router.navigate(['auth/login']);
        },1000)
      });
  }

  togglePassword = () => {
    this.passwordHidden = !this.passwordHidden;
  };

  toggleConfirmPassword = () => {
    this.confirmPasswordHidden = !this.confirmPasswordHidden;
  };

}
