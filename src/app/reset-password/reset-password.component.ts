import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserCommonService } from 'src/app/shared/services/user-common/user-common.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  submitted = false;
  error = false;
  showMsg: boolean = false;
  successMessage:any;
  isLoading = false;
  resetPassSubscription : Subscription;

  constructor(
    public formBuilder: FormBuilder,
    private userCommonService: UserCommonService,
  ) {}

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
    });
  }

  onSubmit() {
    this.error = false;
    this.showMsg = true;
    this.submitted = true;
    this.isLoading = true;
    this.successMessage = "Please wait!"

    if (this.forgotPasswordForm.invalid) {
      return;
    }
    this.resetPassSubscription = this.userCommonService.userResetPassword(this.forgotPasswordForm.controls.email.value).subscribe(
      (data) => {
        if (data.success === true) {
          setTimeout(()=>{
            this.isLoading = false;
            this.successMessage = "Email Sent Sccessfully!";
          this.forgotPasswordForm.reset();
          },100)
        } else {
          this.error = true;
        }
      },
      (error) => {
        this.error = error;
      }
    );
  }

  ngOnDestroy(){
    this.resetPassSubscription?.unsubscribe();
  }

}
