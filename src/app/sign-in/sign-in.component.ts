import { UserCommonService } from './../shared/services/user-common/user-common.service';
import { environment } from './../../environments/environment';
import { AuthLoginService } from './../shared/services/auth-login/auth-login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {
  passwordHidden = true;
  isLoading = false;
  isError = false;
  button = 'Sign In';
  errorMessage = '';
  subscription: Subscription;

  constructor(private fb: FormBuilder,
    private authService: AuthLoginService,
    private commonService: UserCommonService) {
      this.userSignInForm.get('email').setValue(null);
      this.userSignInForm.get('password').setValue(null);
  }
  userSignInForm = this.fb.group({
    email: [null, [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
    password: [null, Validators.required],
  });
  ngOnInit(): void {
  }

  signIn = () => {
    this.userSignInForm.markAllAsTouched();
    this.isError = false;
    this.errorMessage = '';

    if (this.userSignInForm.valid) {
      this.subscription = this.authService.userSignin(this.userSignInForm.value).subscribe((res:any) => {
        if (res) {
          if (res.user.roleName === 'Super Admin') {
            sessionStorage.setItem('token', res.token);
            sessionStorage.setItem('id', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.id), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('name', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.name), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('userObj', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(JSON.stringify(res.user)), environment.SECRET_KEY.trim()).toString());
            this.subscription = this.commonService.userUpdateLastLoginDate(res.user.id)
              .subscribe(() => {});
           window.location.href = '/pages/dashboard';
          }
          else if(res.user.roleName === 'Client'){
            sessionStorage.setItem('token', res.token);
            sessionStorage.setItem('id', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.id), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('name', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.name), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('userObj', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(JSON.stringify(res.user)), environment.SECRET_KEY.trim()).toString());
            this.subscription = this.commonService.userUpdateLastLoginDate(res.user.id)
              .subscribe(() => {});
            window.location.href = '/pages/dashboard';
          }
          else if(res.user.roleName === 'Consultant'){
            sessionStorage.setItem('token', res.token);
            sessionStorage.setItem('id', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.id), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('name', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.name), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('userObj', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(JSON.stringify(res.user)), environment.SECRET_KEY.trim()).toString());
            this.subscription = this.commonService.userUpdateLastLoginDate(res.user.id)
              .subscribe(() => {});
            window.location.href = '/pages/dashboard';
          }
          else if(res.user.roleName === 'Employee'){
            sessionStorage.setItem('token', res.token);
            sessionStorage.setItem('id', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.id), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('name', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(res.user.name), environment.SECRET_KEY.trim()).toString());
            sessionStorage.setItem('userObj', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(JSON.stringify(res.user)), environment.SECRET_KEY.trim()).toString());
            this.subscription = this.commonService.userUpdateLastLoginDate(res.user.id)
              .subscribe(() => {});
            window.location.href = '/pages/dashboard';
          }
        }
        else{
          if(res.success.success === false){
            this.isError = true;
            this.errorMessage = 'Invalid Login!';
          }
        }
      },
      (error: HttpErrorResponse) => {
          if (error) {
            this.isError = true;
            this.errorMessage = 'Invalid Login!';
          }
      }
      )
    } else {
      return false;
    }
  };

  togglePassword = () => {
    this.passwordHidden = !this.passwordHidden;
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }
}
