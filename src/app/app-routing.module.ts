import { AuthGuardGuard } from './shared/guard/auth-guard.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ResetOldPasswordComponent } from './reset-old-password/reset-old-password.component';

const routes: Routes = [
  { path: 'auth/login', component: SignInComponent },
  { path: 'auth/reset-password', component: ResetPasswordComponent },
  { path: 'auth/change-password', component: ChangePasswordComponent },
  { path: 'auth/reset-old-password', component: ResetOldPasswordComponent },
  {
    path: 'pages',
    canActivate: [AuthGuardGuard],
    loadChildren: () =>
    import('./pages/page.module').then((module) => module.PageModule),
  },
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
