import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Cities } from 'src/app/shared/models/cities';
import { States } from 'src/app/shared/models/states';
import { CitiesService } from 'src/app/shared/services/cities/cities.service';
import { StatesService } from 'src/app/shared/services/states/states.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class CitiesComponent implements OnInit, OnDestroy {

  addCityForm: FormGroup;
  countrySubscription: Subscription;
  stateSubscription: Subscription;
  addCitySusbcription: Subscription;
  citySubscrtiption: Subscription;
  deleteSubscription: Subscription;
  updateSubscription: Subscription;
  allStates: States;
  allCities: Cities;
  states: any = [];
  getCities: any = [];
  page: any = 1;
  previousPage: any;
  totalItems: any;
  isEditable: any = false;
  onEditDisabled: boolean = false;
  cityId: any;
  addeditModalRef: any;
  @ViewChild('content') content;

  constructor(private formBuilder: FormBuilder, private modalService: NgbModal, private cityService: CitiesService, private statesService: StatesService, private toastr: ToastrService) {
    this.allStates = new States();
    this.allCities = new Cities();
  }

  ngOnInit(): void {
    this.addCityForm = this.formBuilder.group({
      id: [''],
      stateId: ['', Validators.required],
      cityName: ['', [Validators.pattern('[a-zA-Z ]*')]]
    });
    this.getAllCities(this.allCities);
    this.getAllStates();
  }

  onSubmit() {
   if(!this.isEditable){
      this.addCityForm.markAllAsTouched();
      if(this.addCityForm.valid){
        this.addCitySusbcription = this.cityService.postCities(this.addCityForm.value).subscribe(() => {
          this.modalService.dismissAll();
            this.toastr.success('City added successfully!');
            this.addCityForm.reset();
            this.getAllCities(this.allCities);
        });
      }
    }else{
      this.onUpdateCity(this.cityId, this.addCityForm.value);
    }
  }

  getAllStates(){
    this.stateSubscription = this.statesService.getAllStates().subscribe((res:any) => {
      this.states = res.res;
    })
  }

  addNewCity(content) {
    this.addCityForm.reset();
    this.modalService.open(content, { centered: false });
  }

  getStateName(id: any){
    let state = this.states?.find((obj:any) => obj.id === id);
    return state?.states_name;
  }

  getAllCities(cityObj){
    this.citySubscrtiption = this.cityService.getAllCities(cityObj).subscribe((res:any) => {
      if(res){
        this.getCities = res.res.results;
        this.totalItems = res.res.total;
      }else{
        this.getCities = [];
        this.totalItems = 0;
      }
    })
  }

  loadPage(page:number){
    if(page !== this.previousPage){
      this.allStates.PageNo = page - 1;
      this.previousPage = page;
      this.getAllCities(this.allCities);
    }
  }

  onEditCity(cityObj){
    console.log(cityObj);
    this.cityId = cityObj.id;
      this.isEditable = true;
      this.onEditDisabled = true;
      this.addeditModalRef = this.modalService.open(this.content, {
        size: "md",
        backdrop: true,
        animation: true,
        keyboard: false,
        backdropClass: "modal-backdrop"
      });

      this.addCityForm.get('cityName').patchValue(cityObj.city_name);
      this.addCityForm.get('stateId').patchValue(cityObj.states_id);
    }

  onUpdateCity(id, stateObj){
    if(this.addCityForm.valid){
      this.updateSubscription = this.cityService.updateCities(id, stateObj).subscribe((res:any) => {
        if(res.success){
          this.toastr.success('Updated City successfully');
          this.modalService.dismissAll();
          this.getAllCities(this.allCities);
        }
      });
    }
  }

  deleteCity(id){
    if(window.confirm('Are you sure want to delete this State?')){
      this.onDeleteCity(id);
    }
  }

  onDeleteCity(id){
    this.deleteSubscription = this.cityService.deleteCities(id).subscribe(() => this.getAllCities(this.allCities));
  }

  onSearch(searchValue){
    this.allCities.SearchText = searchValue
    this.getAllCities(this.allCities);
  }

  onClear(clearText){
    this.allCities.SearchText = clearText
    this.getAllCities(this.allCities);
  }

  ngOnDestroy(){
    this.citySubscrtiption?.unsubscribe();
    this.stateSubscription?.unsubscribe();
    this.updateSubscription?.unsubscribe();
    this.deleteSubscription?.unsubscribe();
    this.countrySubscription?.unsubscribe();
    this.addCitySusbcription?.unsubscribe();
  }

}
