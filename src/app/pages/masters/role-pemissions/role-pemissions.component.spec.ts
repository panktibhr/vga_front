import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePemissionsComponent } from './role-pemissions.component';

describe('RolePemissionsComponent', () => {
  let component: RolePemissionsComponent;
  let fixture: ComponentFixture<RolePemissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolePemissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePemissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
