import { Component, OnInit } from '@angular/core';
import { Account } from 'src/app/shared/models/userAccount';
import { UserAccountService } from 'src/app/shared/services/user-account/user-account.service';
import { UserService } from 'src/app/shared/services/user/user.service';

@Component({
  selector: 'app-role-pemissions',
  templateUrl: './role-pemissions.component.html',
  styleUrls: ['./role-pemissions.component.scss']
})
export class RolePemissionsComponent implements OnInit {

  getAllRoles: any;
  accounts$: any = [];
  _accounts:Account;
  constructor(private userService: UserService, private accountService: UserAccountService) {
    this._accounts = new Account();
  }

  ngOnInit(): void {
    this.getAccounts(this._accounts);
    this.getRole();
  }

  getRole() {
    this.userService.getRole().subscribe((res: any) => {
      // let getRole = Object.entries(res);
      this.getAllRoles = res.role;
      res.forEach((data) => {
        this.getAllRoles = this.getAllRoles.name;
        console.log(this.getAllRoles)
      })
    })
  }
  getAccounts(accountsObj){
    this.accountService.getAccountDetail(accountsObj).subscribe((res: any) => {
      console.log(res)
      if (res) {
          this.accounts$ = res.res.results;
      }
    })
  }

}
