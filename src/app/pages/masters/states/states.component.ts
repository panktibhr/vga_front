import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Countries } from 'src/app/shared/models/countries';
import { States } from 'src/app/shared/models/states';
import { CountriesService } from 'src/app/shared/services/countries/countries.service';
import { StatesService } from 'src/app/shared/services/states/states.service';

@Component({
  selector: 'app-states',
  templateUrl: './states.component.html',
  styleUrls: ['./states.component.scss']
})
export class StatesComponent implements OnInit, OnDestroy {

  addStateForm: FormGroup;
  getStates: any = [];
  countries: any = [];
  allStates: States;
  page: any = 1;
  previousPage: any;
  totalItems: any;
  allCountries: Countries;
  isEditable: any = false;
  stateId: any;
  onEditDisabled: boolean = false;
  addeditModalRef: any;
  @ViewChild('content') content;
  stateObj: any;
  addStateSubscription: Subscription;
  countrySubscription: Subscription;
  stateSubscription: Subscription;
  updateSubscription: Subscription;
  deleteSubscription: Subscription;


  constructor(private formBuilder: FormBuilder, private modalService: NgbModal, private stateService: StatesService,
              private countryService: CountriesService, private toastr: ToastrService) {
    this.allStates = new States();
    this.allCountries = new Countries();

  }

  ngOnInit(): void {
    this.addStateForm = this.formBuilder.group({
      id: [''],
      stateName: ['', [Validators.pattern('[a-zA-Z ]*')]],
      countryId: ['', Validators.required]
    });
    this.getAllStates(this.allStates);
    this.getAllCountries();
  }

  ngOnDestroy(){
    this.stateSubscription?.unsubscribe();
    this.updateSubscription?.unsubscribe();
    this.countrySubscription?.unsubscribe();
    this.addStateSubscription?.unsubscribe();
    this.deleteSubscription?.unsubscribe();
  }

  getAllStates(stateObj){
    this.stateSubscription = this.stateService.searchStates(stateObj).subscribe((res:any) => {
      if(res){
        this.getStates = res.res.results;
        this.totalItems = res.res.total;
      }else{
        this.getStates = [];
        this.totalItems = 0;
      }
    });
  }

  onSubmit() {
      if(!this.isEditable){
        this.addStateForm.markAllAsTouched();
        if(this.addStateForm.valid){
            this.addStateSubscription = this.stateService.postState(this.addStateForm.value).subscribe((res: any) => {
              this.modalService.dismissAll();
              this.toastr.success(res.message);
              this.addStateForm.reset();
            });
        }

      }else{
        this.onUpdateState(this.stateId, this.addStateForm.value);
      }
  }

  getAllCountries(){
    this.countrySubscription = this.countryService.getAllCountries().subscribe((res:any) => {
      this.countries = res.res;
    })
  }

  getCountryName(id: any){
    let country = this.countries?.find((obj:any) => obj.id === id);
    return country?.countries_name;
  }

  addNewState(content) {
    this.addStateForm.reset();
    this.modalService.open(content, { centered: false });
  }

  loadPage(page:number){
    if(page !== this.previousPage){
      this.allStates.PageNo = page - 1;
      this.previousPage = page;
      this.getAllStates(this.allStates);
    }
  }

  onEditState(stateObj){
    this.stateId = stateObj.id;
      this.isEditable = true;
      this.onEditDisabled = true;
      this.addeditModalRef = this.modalService.open(this.content, {
        size: "md",
        // modalClass: "mymodal",
        // hideCloseButton: false,
        // centered: true,
        backdrop: true,
        animation: true,
        keyboard: false,
        // closeOnOutsideClick: true,
        backdropClass: "modal-backdrop"
      });

      this.addStateForm.get('stateName').patchValue(stateObj.states_name);
      this.addStateForm.get('countryId').patchValue(stateObj.countries_id);
    }

  onUpdateState(id, stateObj){
    if(this.addStateForm.valid){
      this.updateSubscription = this.stateService.updateStates(id, stateObj).subscribe((res:any) => {
        if(res.success){
          this.toastr.success('Updated state successfully');
          this.modalService.dismissAll();
          this.getAllStates(this.allStates);
        }
      });
    }
  }

  deleteState(id){
    if(window.confirm('Are you sure want to delete this state?')){
      this.onDeleteState(id);
    }
  }

  onDeleteState(id){
    this.deleteSubscription = this.stateService.deleteStates(id).subscribe(() => this.getAllStates(this.allStates));
  }

  onSearch(searchValue){
    this.allStates.SearchText = searchValue
    this.getAllStates(this.allStates);
  }

  onClear(clearText){
    this.allStates.SearchText = clearText
    this.getAllStates(this.allStates);
  }

}
