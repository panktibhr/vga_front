import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Countries } from 'src/app/shared/models/countries';
import { CountriesService } from 'src/app/shared/services/countries/countries.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss'],
})
export class CountryComponent implements OnInit, OnDestroy {

  addCountryForm: FormGroup;
  countries: any = [];
  addCountrySubscription: Subscription;
  getCountrySubscription: Subscription;
  editCountrySubscription: Subscription;
  deleteCountrySubscription: Subscription;
  editCountryValue: any;
  allCountries: Countries;
  page: any = 1;
  itemsPerPage: number = 30;
  previousPage: any;
  totalItems: any;
  country_value: any;

  constructor(private modalService: NgbModal, public formBuilder: FormBuilder, private countryService: CountriesService, private toastr: ToastrService) {
    this.allCountries = new Countries();
  }

  ngOnInit(): void {
    this.addCountryForm = this.formBuilder.group({
      countryName: ['', [Validators.pattern('[a-zA-Z ]*')]],
    });
    this.getAllCountries(this.allCountries);
  }

  addNewCountry(content) {
    this.addCountryForm.reset();
    this.modalService.open(content, { centered: false });
  }

  onAddCountry() {
    this.addCountryForm.markAllAsTouched();
    let countryFound = false;
    this.countries.map((country:any) => {
      if(country.countries_name.toLowerCase().includes(this.addCountryForm.get("countryName").value.toLowerCase())){
        countryFound = true
      }
    });

    if ( countryFound ) {
      this.toastr.error('Country already exist');
    }else{
      if(this.addCountryForm.valid){
        this.addCountrySubscription = this.countryService.postCountries(this.addCountryForm.value).subscribe((res: any) => {
          this.modalService.dismissAll();
          this.toastr.success(res.message);
          this.addCountryForm.reset();
          this.getAllCountries(this.allCountries);
        });
      }
   }
  }

  getAllCountries(countryObj){
    this.getCountrySubscription = this.countryService.getCountries(countryObj).subscribe((res:any) => {
      if(res){
        this.totalItems = res.res.total;
        this.countries = res.res.results;
      }else{
        this.totalItems = 0;
        this.countries = [];
      }
    })
  }

  onEditCountry(cntObj, countryName) {
    let inputTag: any = document.getElementById(`input${cntObj.id}`);
    inputTag.value = cntObj.countries_name;
    if(this.addCountryForm.valid){
      let countryObj = {countryName: countryName};
      const pTag: HTMLElement = document.getElementById(`p${cntObj.id}`);
      const editButton: any = document.getElementById(`edit${cntObj.id}`);
      pTag.hidden = !pTag.hidden;
      inputTag.hidden = !inputTag.hidden;
      if(!inputTag.hidden){
        inputTag.focus();
        editButton.className = 'fas fa-check text-success';
      } else{
        editButton.className = 'fas fa-edit text-success';
        this.editCountrySubscription = this.countryService.updateCountry(cntObj.id, countryObj).subscribe((res:any) => {
          res.success && this.getAllCountries(this.allCountries);
        });
      }
    }
  }

  deleteCountry(id){
    if(window.confirm('Are you sure want to delete this country?')){
      this.onDeleteCountry(id);
    }
  }

  onDeleteCountry(i) {
    this.deleteCountrySubscription = this.countryService.deleteCountry(i).subscribe(() => {this.getAllCountries(this.allCountries)})
  }

  onSearch(searchValue){
    this.allCountries.SearchText = searchValue;
   this.getAllCountries(this.allCountries);
  }

  onClear(clearText){
    this.allCountries.SearchText = clearText;
    this.getAllCountries(this.allCountries);
  }

  loadPage(page:number){
    if(page !== this.previousPage){
      this.allCountries.PageNo = page - 1;
      this.previousPage = page;
      this.getAllCountries(this.allCountries);
    }
  }

  ngOnDestroy(){
    this.addCountrySubscription?.unsubscribe();
    this.editCountrySubscription?.unsubscribe();
    this.getCountrySubscription?.unsubscribe();
    this.deleteCountrySubscription?.unsubscribe();
  }
}
