import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';
import { UserService } from 'src/app/shared/services/user/user.service';
import { environment } from 'src/environments/environment';

enum UserRoleType {
  SuperAdmin = 'Super Admin',
  Client = 'Client',
}
@Component({
  selector: 'app-organizational-overview',
  templateUrl: './organizational-overview.component.html',
  styleUrls: ['./organizational-overview.component.scss'],
})
export class OrganizationalOverviewComponent implements OnInit {

  users: any = [];
  userObj:any;
  roleType: any = UserRoleType;
  clientId: string;
  hierarchy: any;
  topLevel: any;
  constructor(private userService: UserService, private toastr: ToastrService, private clientServices: ClientsService) {
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
    this.clientId = sessionStorage.getItem('clientId');
  }

  ngOnInit(): void {
    if(this.roleType === UserRoleType.Client) {
      this.getClientHierarchy(this.clientId);
    }else {
      this.getUsersHierarchy();
    }
  }

  getUsersHierarchy(){
    this.userService.getUsersHierarchy().subscribe((res: any) => {
      this.users = res.res.parent;
      this.topLevel = res.res.parent.name;
    })
  }

  getClientHierarchy(id) {
    debugger
      this.clientServices.getHierarchyByClient(id).subscribe((res:any) => {
        debugger
        this.users = res.res.parent;
         this.topLevel = res.res.parent.name;

        });
  }

  downloadChart(id, isDownload) {
    this.clientServices.downloadHierarchyByClient(id, isDownload).subscribe((res:Blob) => {
      var file = new Blob([res], { type: 'application/pdf' })
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, (error) => {
      if(error){
        this.toastr.error('Please try after some time !!');
      }
    });
  }

}
