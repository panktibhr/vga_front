import { UserAccountService } from './../../../shared/services/user-account/user-account.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConsultantService } from './../../../shared/services/consultant/consultant.service';
import { Consultant } from './../../../shared/models/consultant';
import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Account } from './../../../shared/models/userAccount';
import { CountriesService } from 'src/app/shared/services/countries/countries.service';
import { StatesService } from 'src/app/shared/services/states/states.service';
import { CitiesService } from 'src/app/shared/services/cities/cities.service';
import { Countries } from 'src/app/shared/models/countries';
import { States } from 'src/app/shared/models/states';
import { Cities } from 'src/app/shared/models/cities';
import { environment } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-new-consultant',
  templateUrl: './new-consultant.component.html',
  styleUrls: ['./new-consultant.component.scss'],
})
export class NewConsultantComponent implements OnInit, OnDestroy {
  button: String;
  consultantPageTitle: String;
  consultantForm: FormGroup;
  states: any;
  cities: any;
  userObj: any;
  roleType: any;
  countries: any;
  consultantId: any;
  consultantInfoById: any;
  getAllConsultantDetails: any;
  consultants$: any = [];
  _states: States;
  _cities: Cities;
  _accounts: Account;
  _countries: Countries;
  _consultant: Consultant;
  accountsObj$: Observable<Account>;
  consultantObj: Observable<Consultant[]>;
  postConsultantObj: Observable<Consultant>;
  consultantDetailsListing: Observable<Consultant>;
  subscription: Subscription;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private consultantService: ConsultantService,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private accountservice: UserAccountService,
    private countryService: CountriesService,
    private stateService: StatesService,
    private cityService: CitiesService
  ) {
    this._accounts = new Account();
    this.consultantId = this.actRoute.snapshot.params.id;
    this._countries = new Countries();
    this._states = new States();
    this._cities = new Cities();
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
  }

  ngOnInit() {
    if (this.router.url === '/pages/new-consultant') {
      this.button = 'Submit';
      this.consultantPageTitle = 'New Consultant';
    } else if (this.router.url === window.location.pathname) {
      this.button = 'Update';
      this.consultantPageTitle = 'Consultant Profile';
      this.getConsultantById();
    } else {
      this.button = 'Update';
      this.consultantPageTitle = 'Update Consultant';
      this.getConsultantById();
    }
    this.getAccountDetailObj();
    this.getCountries();
    this.consultantForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      consultantName: ['', [Validators.required, Validators.minLength(3)]],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', [Validators.required, Validators.maxLength(6), Validators.minLength(6)]],
      mobile: [
        null,
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/),
        ],
      ],
      password: ['', Validators.required],
      username: ['', Validators.required],
      parent_account_id: ['', Validators.required],
    });
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    this.consultantForm.markAllAsTouched();
    this.consultantForm.get('password').patchValue(this.generatePassword(8));
    if (this.consultantForm.valid) {
      if (this.router.url === '/pages/new-consultant') {
        this.postConsultantObj = this.consultantService.postConsultantObj(this.consultantForm.value);
        this.postConsultantObj.subscribe((res: any) => {
          if (res) {
            if (res.success === true) {
              this.toastr.success('Consultant added successfully!');
              this.consultantForm.reset();
              this.router.navigate(['/pages/search-consultants']);
            }
          }
        }, (error) => {
          if(error.error.errors[0].msg === 'Email is already exist.'){
            this.toastr.error('Email is already exist!');
          } else if(error.error.errors[0].msg === 'username is already exist.'){
            this.toastr.error('Username is already exist!');
          } else if(error.error.errors[0].msg === 'must be at last 3 chars long.'){
            this.toastr.error('Username must be at 3 characters long!');
          } else{
            this.toastr.error('Invalid Data!')
          }
        });
      } else if (this.router.url === '/pages/ConsultantsViewProfile/:id') {
        setTimeout(() => {
          this.editConsultant();
        }, 1500);
      } else {
        setTimeout(() => {
          this.editConsultant();
        }, 1500);
      }
    } else {
      this.validateAllFormFields(this.consultantForm);
    }
  }

  generatePassword(length, charSet = '') {
    charSet = charSet ? charSet : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789^°!"§$%&/()=?`*+~\'#,;.:-_';
    let newPass = Array.apply(null, Array(length || 10)).map(function () {
      return charSet.charAt(Math.random() * charSet.length);
    }).join('');
    return newPass;
  }

  getConsultantById() {
    this.consultantObj = this.consultantService.getConsultantById(this.consultantId);
    this.consultantObj.subscribe((res: any) => {
      this.consultantInfoById = res.res;
      this.consultantForm.get('companyName').patchValue(this.consultantInfoById.company_name);
      this.consultantForm.get('consultantName').patchValue(this.consultantInfoById.name);
      this.consultantForm.get('address').patchValue(this.consultantInfoById.address);
      this.getStates(this.consultantInfoById.countries_id);
      this.consultantForm.get('country').patchValue(this.consultantInfoById.countries_id);
      this.getCities(this.consultantInfoById.states_id);
      this.consultantForm.get('state').patchValue(this.consultantInfoById.states_id);
      this.consultantForm.get('city').patchValue(this.consultantInfoById.city_id);
      this.consultantForm.get('zipCode').patchValue(this.consultantInfoById.zip);
      this.consultantForm.get('mobile').patchValue(this.consultantInfoById.mobile);
      this.consultantForm.get('email').patchValue(this.consultantInfoById.email);
      this.consultantForm.get('username').patchValue(this.consultantInfoById.username);
      this.consultantForm.get('parent_account_id').patchValue(this.consultantInfoById.parent_account_id);
    });
  }

  editConsultant() {
    this.putConsultantByIdToApi(this.consultantId, this.consultantForm.value);
    setTimeout(() => {
      this.consultantDetailsListing = this.consultantService.getAllConsultants(this._consultant);
      this.consultantDetailsListing.subscribe((res: any) => {
        if (res) {
          this.getAllConsultantDetails = res.res;
        }
      });
    }, 1500);
  }

  putConsultantByIdToApi(id, consultantObj) {
    this.subscription = this.consultantService.updateConsultantObj(id, consultantObj).subscribe((res: any) => {
      if (res) {
        this.toastr.success('Consultant Updated Successfully');
        (this.roleType !== 'Consultant') ? this.router.navigate(['/pages/search-consultants']) : null;
      } else {
        this.toastr.error('Something went wrong!');
      }
    },(error) => {
      if(error.error.errors[0].msg === 'Email is already exist.'){
        this.toastr.error('Email is already exist!');
      } else if(error.error.errors[0].msg === 'username is already exist.'){
        this.toastr.error('Username is already exist!');
      } else if(error.error.errors[0].msg === 'must be at last 3 chars long.'){
        this.toastr.error('Username must be at 3 characters long!');
      } else{
        this.toastr.error('Invalid Data!')
      }
    });
  }

  getAccountDetailObj() {
    this.accountsObj$ = this.accountservice.getAccountDetail(this._accounts);
    this.accountsObj$.subscribe((res: any) => {
      if (res) {
        if (res.success === true) {
          this.consultants$ = res.res.results;
        }
      }
    },(error) => { return error });
  }

  getParentAccountName(id) {
    let matchParentObj = this.consultants$.filter((element: any) => {
      if (element.id === id) {
        return id;
      }
    });
    if (matchParentObj.length > 0) {
      return matchParentObj[0].name;
    }
  }

  getCountries(){
    this.subscription = this.countryService.getAllCountries().subscribe((res: any) => {
      this.countries = res.res;
    })
  }

  getStates(countryId){
    this.subscription = this.stateService.getState(countryId).subscribe((res:any) => {
      this.states = res.res;
    })
  }

  getCities(stateId){
    this.subscription = this.cityService.getCities(stateId).subscribe((res:any) => {
      this.cities = res.res;
    })
  }
}
