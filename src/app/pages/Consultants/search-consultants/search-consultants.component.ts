import { ToastrService } from 'ngx-toastr';
import { ConsultantService } from './../../../shared/services/consultant/consultant.service';
import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Consultant } from 'src/app/shared/models/consultant';
import { Router } from '@angular/router';
import { ModalManager } from 'ngb-modal';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';

@Component({
  selector: 'app-search-consultants',
  templateUrl: './search-consultants.component.html',
  styleUrls: ['./search-consultants.component.scss']
})

export class SearchConsultantsComponent implements OnInit, OnDestroy {

  @ViewChild('contentDelete') contentDelete;

  consultantObj:Observable<Consultant>;
  consultant:any = [];
  countries: any =[];
  states: any;
  cities: any =[];
  _deleteConsultantId:any;
  searchConsultant:String;
  page: any = 0;
  itemsPerPage: number = 5;
  previousPage: any;
  totalItems: any;
  _consultants: Consultant;
  subscription: Subscription;
  deleteModalRef: any;
  _searchConsultantDeleteId: any;

  constructor(private consultantService: ConsultantService, private toastr: ToastrService, private router: Router, private modalService: ModalManager ) {
    this._consultants = new Consultant();
  }

  ngOnInit() {
    this.countries = JSON.parse(localStorage.getItem('Countries'));
    this.states = JSON.parse(localStorage.getItem('States'));
    this.cities = JSON.parse(localStorage.getItem('Cities'));
    this.getAllConsultants(this._consultants);
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  getAllConsultants(consultantObj){
    this.consultantObj = this.consultantService.getAllConsultants(consultantObj);
    this.consultantObj.subscribe((res:any) => {
      if(res){
        this.totalItems = res.res.total;
        this.consultant = res.res.results;
      }else{
        this.consultant = [];
        this.totalItems = 0;
      }
    },(error) => {
      console.log(error);
    })

  }

  deleteConsultant(id){
    this._searchConsultantDeleteId= id;
    this.deleteModalRef = this.modalService.open(this.contentDelete, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
    })
    this.deleteModalRef.onClose.subscribe(() => {
    })
  }

  OnCanceleDeleteModal() {
    this.modalService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
    this.deleteConsultantByAPI(this._searchConsultantDeleteId);
        this.toastr.success("User deleted successfully!")
        this.modalService.close(this.deleteModalRef);
      }


  deleteConsultantByAPI(deleteConsultantId){
    this.subscription = this.consultantService.deleteConsultantObj(deleteConsultantId).subscribe((res :any) => {
      if (res.success) {
        this.getAllConsultants(this._consultants);
      } else {
        this.toastr.error('Soemthing is wrong');
      }
    })
  }

  editConsultant(id){
    this.router.navigate(['pages/edit-consultant', id]);
  }

  getCityName(id){
    const getCity =  this.cities.find((element: any) => {
      if (element.id === id) {
        return element;
       }
       return getCity;
    })
    return getCity.city_name;
  }

  getStateName(id){
    const getStates = this.states.find((element: any) => {
      if(element.id === id){
        return element;
      }
      return getStates;
    })
    return getStates.states_name;
  }

   onSearch(searchValue){
    this._consultants.SearchText = searchValue;
    this.getAllConsultants(this._consultants);
   }

   onClear(clearText){
    this._consultants.SearchText = clearText;
    this.getAllConsultants(this._consultants);
   }

   loadPage(page:number){
    if(page !== this.previousPage){
      this._consultants.PageNo = page - 1;
      this.previousPage = page;
      this.getAllConsultants(this._consultants);
    }
  }

  sort(columnName:any){
    this._consultants.SortItem = columnName;
    if(this._consultants.SortOrder === "ASC"){
      this._consultants.SortOrder = "DESC";
    }else{
      this._consultants.SortOrder = "ASC";
    }
    this.getAllConsultants(this._consultants);
  }

}
