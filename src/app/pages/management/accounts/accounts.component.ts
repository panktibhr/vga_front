import { Account } from './../../../shared/models/userAccount';
import { UserAccountService } from './../../../shared/services/user-account/user-account.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalManager } from 'ngb-modal';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  accountId: any;
  totalItems: any;
  putAccountObj: any;
  previousPage: any;
  deleteModalRef: any;
  addeditModalRef: any;
  _deleteAccountId: any;
  page: any = 0;
  itemsPerPage: number = 5;
  _accounts:Account;
  accountForm: FormGroup;
  isEditable = false;
  accounts$: any = [];
  accountsObj$: Observable<Account>
  postAccountObj$: Observable<Account>;
  @ViewChild('content') content;
  @ViewChild('contentDelete') contentDelete;

  constructor(private modelService: ModalManager,
    private accountService: UserAccountService,
    private fb: FormBuilder, private toastr: ToastrService) {
    this.accountForm = this.fb.group({
      id: [],
      name: [null, [Validators.required, Validators.minLength(3)]],
      description: [null, Validators.required],
      email: [null, [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      phone: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      parent_account_id: [null, Validators.required]
    })
    this._accounts = new Account();
  }

  ngOnInit(): void {
    this.getAccountDetailObj(this._accounts);
  }

  openAccountAddEditModel = () => {
    this.accountForm.reset();
    this.isEditable = false;
    this.addeditModalRef = this.modelService.open(this.content, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
      keyboard: false,
      closeOnOutsideClick: true,
      backdropClass: "modal-backdrop"
    });
    this.addeditModalRef.onClose.subscribe(() => {
      this.accountService.getAccountDetail(this._accounts).subscribe((res: any) => {
        if (res) {
          if (res.success === true) {
            this.accounts$ = res.res.results;
          }
        }
      })
    })
  }

  onClickCloseModel() {
    this.modelService.close(this.addeditModalRef);
  }

  getAccountDetailObj(accountsObj) {
    this.accountsObj$ = this.accountService.getAccountDetail(accountsObj);
    this.accountsObj$.subscribe((res: any) => {
      if (res) {
          this.accounts$ = res.res.results;
          this.totalItems = res.res.total;
      }else{
        this.accounts$ = [];
        this.totalItems = 0;
      }
    });
  }
  getParantAccountName(id) {
    let matchParentObj = this.accounts$.filter((element: any) => {
      if (element.id === id) {
        return id;
      }
    })
    if (matchParentObj.length > 0) {
      return matchParentObj[0].name
    } else {
      return "-"
    }
  }

  onSaveAccountInfo() {
    this.accountForm.markAllAsTouched();
    if (this.accountForm.valid) {
      this.postAccountObj$ = this.accountService.postAccountDetailObj(this.accountForm.value);
      this.postAccountObj$.subscribe((res: any) => {
        if (res) {
          if (res.success === true) {
            this.toastr.success('Accound added successfully!')
            this.modelService.close(this.addeditModalRef);
            this.accountForm.reset();
          }
        }
      },(error: HttpErrorResponse) => {
        if (error) {
          this.toastr.error('Invalid Form!')
        }
      })
    } else {
      return false;
    }
  }
  onSaveUpdatedAccountInfo(){
    if (this.accountForm.valid) {
    this.putAccountObj = this.accountService.updateAccountDetailObj(this.accountId,this.accountForm.value);
    this.putAccountObj.subscribe((res: any) => {
      if(res.account){
        this.toastr.success('Account updated successfully!')
        this.modelService.close(this.addeditModalRef);
        this.getAccountDetailObj(this._accounts);
      }
    });
  }
  }

  onEditAccountDetail(id) {
    this.accountId = id;
    let accountInfo = this.accounts$.filter((res: any) => { if (res.id === id) { return res } })
    if (accountInfo) {
      this.isEditable = true;
      this.accountForm.reset();
      this.addeditModalRef = this.modelService.open(this.content, {
        size: "md",
        modalClass: 'mymodal',
        hideCloseButton: false,
        centered: false,
        backdrop: true,
        animation: true,
        keyboard: false,
        closeOnOutsideClick: true,
        backdropClass: "modal-backdrop"
      });
      this.accountForm.get('id').patchValue(accountInfo[0].id);
      this.accountForm.patchValue(accountInfo[0]);
      this.addeditModalRef.onClose.subscribe(() => {});
    }
  }
  onDeleteAccountDetail(id) {
    this._deleteAccountId = id;
    this.deleteModalRef = this.modelService.open(this.contentDelete, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
    });
    this.deleteModalRef.onClose.subscribe(() => {
    })
  }

  OnCanceleDeleteModal() {
    this.modelService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
    this.accountService.deleteAccountDetailObj(this._deleteAccountId)
      .subscribe((res: any) => {
        if(res){
          this.toastr.success('Account deleted successfully!');
          this.modelService.close(this.deleteModalRef);
          this.getAccountDetailObj(this._accounts);
        }
    })
  }

  onSearch(searchValue, searchEmail) {
    this._accounts.SearchText = searchValue;
    this._accounts.SearchEmail = searchEmail;
    this.getAccountDetailObj(this._accounts);
  }

  onClear(clearName, clearEmail){
    this._accounts.SearchText = clearName;
    this._accounts.SearchEmail = clearEmail;
    this.getAccountDetailObj(this._accounts);
  }

  sort(columnName: any) {
    this._accounts.SortItem = columnName;
    if (this._accounts.SortOrder == 'ASC') {
      this._accounts.SortOrder = 'DESC';
    } else {
      this._accounts.SortOrder = 'ASC';
    }
    this.getAccountDetailObj(this._accounts);
  }

  loadPage(page:number){
    if(page !== this.previousPage){
      this._accounts.PageNo = page - 1;
      this.previousPage = page;
      this.getAccountDetailObj(this._accounts);
    }
  }
}
