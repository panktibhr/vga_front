import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalManager } from 'ngb-modal';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/models/userAuth';
import { UserAccountService } from 'src/app/shared/services/user-account/user-account.service';
import { UserService } from '../../../shared/services/user/user.service';
import { Account } from '../../../shared/models/userAccount';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @ViewChild('contentDelete') contentDelete;
  @ViewChild('content') content;
  _users: User;
  _accounts:Account;
  userForm: FormGroup;
  userId: any;
  totalItems: any;
  putUserObj: any;
  getAllUsers: any;
  getAllRoles: any;
  previousPage: any;
  _userDeleteId: any;
  get_All_Users: any;
  deleteModalRef: any;
  addeditModalRef: any;
  page: any = 0;
  itemsPerPage: number = 5;
  button:any = 'Confirm';
  isEditable: any = false;
  name: any = [];
  users$: any = [];
  accounts$: any = [];
  dropdownSettings: any = {};
  selectedRole: string = '';
  selectedValue: string = '';
  postUserObj$: Observable<User>
  accountsObj$: Observable<Account>
  onEditDisabled: boolean = false;

  constructor(private modalService: ModalManager, private accountservice: UserAccountService, private userService: UserService,
    private formBuilder: FormBuilder, private toastr: ToastrService) {
    this._users = new User();
    this._accounts = new Account();
    this.userForm = this.formBuilder.group({
      id: [],
      name: ['', [Validators.required, Validators.minLength(3)]],
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      role: ['', Validators.required],
      parent_account_id: ['', Validators.required]
    })
  }

  generatePassword(length, charSet = "") {
    charSet = charSet ? charSet : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789^°!"§$%&/()=?`*+~\'#,;.:-_';
    let newPass = Array.apply(null, Array(length || 10)).map(function () {
      return charSet.charAt(Math.random() * charSet.length);
    }).join('');
    return newPass
  }

  ngOnInit() {
    this.getAccountDetailObj(this._accounts);
    this.getRole();
    this.getUsers(this._users);
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  onEditUserDetail(id) {
    this.userId = id;
    let userInfo = this.users$.filter((res: any) => { if (res.id === id) { return res } })
    if (userInfo) {
      this.isEditable = true;
      this.onEditDisabled = true;
      this.userForm.get('role').disable();
      this.userForm.get('email').disable();
      this.addeditModalRef = this.modalService.open(this.content, {
        size: "md",
        modalClass: "mymodal",
        hideCloseButton: false,
        centered: true,
        backdrop: true,
        animation: true,
        keyboard: false,
        closeOnOutsideClick: true,
        backdropClass: "modal-backdrop"
      });
      this.userForm.get('id').patchValue(userInfo[0].id);
      this.userForm.patchValue(userInfo[0]);
      this.userForm.patchValue({role:userInfo[0].role_id})
      this.addeditModalRef.onClose.subscribe(() => {});
    }
  }

  onSaveUpdatedUserInfo() {
    if (this.userForm.valid) {
    this.putUserObj = this.userService.updateUserDetailObj(this.userId, this.userForm.value);
    this.putUserObj.subscribe((res: any) => {
      if (res.success === true) {
        this.toastr.success('User updated successfully!')
        this.modalService.close(this.addeditModalRef);
        this.getUsers(this._users);
      }
      else if (res.success === false) {
        this.toastr.error('Username is already taken!')
        this.modalService.close(this.addeditModalRef);
        this.getUsers(this._users);
      }
    })
    }
  }

  onClickCloseModel() {
    this.modalService.close(this.addeditModalRef);
    this.userForm.get('role').enable();
    this.userForm.get('email').enable();
  }

  getRole() {
    this.userService.getRole().subscribe((res: any) => {
      this.getAllRoles = res.role;
    })
  }

  getRoleName = (role) => {
    const roles = this.getAllRoles.filter((res) => {
      if (res.id === role.role_id) {
        return res
      }
    });
    if (roles) {
      return roles[0].name;
    }
  }

  getUsers(usersObj) {
    this.get_All_Users = this.userService.getAllUsers(usersObj);
    this.get_All_Users.subscribe((res: any) => {
      if (res) {
        this.users$ = res.res.results;
        this.totalItems = res.res.total;
      }else{
        this.users$ = [];
        this.totalItems = 0;
      }
    }, (error) => {
      return error;
    })
  }

  onSubmit() {
    this.userForm.markAllAsTouched();
    this.userForm.get("password").patchValue(this.generatePassword(8));
    if (this.userForm.valid) {
      this.postUserObj$ = this.userService.addNewUser(this.userForm.value);
      this.postUserObj$.subscribe((res: any) => {
        if (res) {
          if (res.success === true) {
            this.modalService.close(this.addeditModalRef);
            this.toastr.success('User added successfully!');
            this.userForm.reset();
            this.getUsers(this._users);
          }
        }
      }, (error) => {
        if(error.error.errors[0].msg === 'must be at last 3 chars long.'){
          this.toastr.error('Please enter characters of name more then 2 characters!');
        } else if(error.error.errors[0].msg === 'Email is already exist.'){
          this.toastr.error('Email is already exist!')
        } else if(error.error.errors[0].msg === 'username is already exist.'){
          this.toastr.error('Username is already exist!');
        }
      })
    }
  }

  getAccountDetailObj(accountsObj) {
    this.accountsObj$ = this.accountservice.getAccountDetail(accountsObj);
    this.accountsObj$.subscribe((res: any) => {
      if (res) {
        if (res.success === true) {
          this.accounts$ = res.res.results;
        }
      }
    }, (error) => { return error; })
  }

  getParantAccountName(id) {
    let matchParentObj = this.accounts$.filter((element: any) => {
      if (element.id === id) {
        return id;
      }
    })
    if (matchParentObj.length > 0) {
      return matchParentObj[0].name;
    }
  }

  parent($event: any) {
    this.selectedValue = $event.target.value;
  }

  role($event: any) {
    this.selectedRole = $event.target.value;
  }

  addUser(content) {
    this.userForm.reset();
    this.isEditable = false;
    this.userForm.get('role').enable();
    this.userForm.get('email').enable();
    this.addeditModalRef = this.modalService.open(this.content, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
      keyboard: false,
      closeOnOutsideClick: true,
      backdropClass: "modal-backdrop"
    });
    this.addeditModalRef.onClose.subscribe(() => { })
    this.getRole();
  }

  onDeleteUserDetail(id) {
    this._userDeleteId= id;
    this.deleteModalRef = this.modalService.open(this.contentDelete, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
    })
    this.deleteModalRef.onClose.subscribe(() => { })
  }

  OnCanceleDeleteModal() {
    this.modalService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
    this.userService.deleteUser(  this._userDeleteId).subscribe((res: any) => {
      if (res.message) {
        this.toastr.success("User deleted successfully!")
        this.modalService.close(this.deleteModalRef);
        this.getUsers(this._users);
      }
    })
  }

  onSearch(searchValue, searchEmail){
    this._users.SearchText = searchValue;
    this._users.SearchText2 = searchEmail;
    this.getUsers(this._users);
  }

  onClear(clearName, clearEmail){
    this._users.SearchText = clearName;
    this._users.SearchText2 = clearEmail;
    this.getUsers(this._users);
  }

  sort(columnName:any){
    this._users.SortItem = columnName;
    if(this._users.SortOrder == "ASC"){
      this._users.SortOrder = "DESC";
    }else{
      this._users.SortOrder = "ASC";
    }
    this.getUsers(this._users);
  }

  loadPage(pageNumber:number){
    if(pageNumber !== this.previousPage){
      this._users.PageNo = pageNumber - 1;
      this.previousPage = pageNumber;
      this.getUsers(this._users);
    }
  }
}
