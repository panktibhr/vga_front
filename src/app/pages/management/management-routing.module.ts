import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountsComponent } from './accounts/accounts.component';
import { UsersComponent } from './users/users.component';

const managementRoutes: Routes = [
  { path: 'user', component: UsersComponent },
  { path: 'account', component:AccountsComponent}
]
@NgModule({
  imports: [RouterModule.forChild(managementRoutes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule { }
