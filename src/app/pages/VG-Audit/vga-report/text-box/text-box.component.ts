import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/shared/services/data.service';
import { ReportAnswerValuesService } from 'src/app/shared/services/report-AnswerValues/report-answer-values.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss']
})
export class TextBoxComponent implements OnInit {

  @Input() answerValue:any;
  @Input() answerValueType: any;
  @Input() createform: any;
  @Input() vgaReportForm: any;
  @Input() questions:any;
  @Input() answerType:any;
  @Input() subQuestionIdArr:any;
  @Input() subQuestionsArr:any;
  @Input() arr:any;
  @Input() question: any;

  textbox: any = [1, 2, 3, 4, 5];
  textboxForSalesActionPlan: any = [1,2,3];
  strategicSphereForm: FormGroup;
  userObj: any;
  vgAuditDetails: any;
  vgAudit: any = [];
  auditDetails: any;
  vgAuditDetailsFromLocalStorage: string;
  questionId: any;
  subSectionId: any;
  clientId: any;
  answerDetails: FormArray;
  strategicSphereArray: any = [];
  sectionId: any;
  definiteDirectionForm: FormGroup;
  purposefulPlanningForm: FormGroup;
  concreteForm: FormGroup;
  definiteDirectionArray: any = [];
  purposefulPlanningArray: any = [];
  concreteArray: any = [];
  growthFrameForm: FormGroup;
  growthFrameArray: any = [];
  correctiveActionPlanForm: FormGroup;
  workActionPlanForm: FormGroup;
  salesActionPlanForm: FormGroup;
  _clientId: any;

  constructor(private formBuilder: FormBuilder, private actRoute: ActivatedRoute, private reportAnswerValues: ReportAnswerValuesService, private toastr: ToastrService, private serviceData: DataService) {
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.vgAuditDetailsFromLocalStorage = CryptoJS.AES.decrypt(localStorage.getItem('vgAuditDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);

    this.actRoute.params.subscribe((res: any) => {
      this.subSectionId = res.secId;
      this.questionId = res.queId;
    });
    this.clientId = sessionStorage.getItem('clientId');
    this.sectionId = CryptoJS.AES.decrypt(localStorage.getItem('section-id'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8);
  }

  ngOnInit(): void {
    this.getVgAuditDetails();
    this.strategicSphereForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        strategicSphere1: this.formBuilder.group({
          core: '',
          vitalInterests: '',
          bufferZones: '',
          pivotalZones: '',
          forwardPositions: '',
          powerVaccums: '',
        }),
        strategicSphere2: this.formBuilder.group({
          textbox1: '',
          textbox2: '',
          textbox3: '',
          textbox4: '',
        }),
        strategicSphere3: this.formBuilder.group({
          initiative1: this.formBuilder.group({
            txtIntiative1: '',
            txtOperatingProperties1: '',
            txtOperatingProperties2: '',
          }),
          initiative2: this.formBuilder.group({
            txtIntiative2: '',
            txtOperatingProperties3: '',
            txtOperatingProperties4: '',
          }),
          initiative3: this.formBuilder.group({
            txtIntiative3: '',
            txtOperatingProperties5: '',
            txtOperatingProperties6: ''
          }),
        }),
      }),
    });
    this.strategicSphereArray.push(this.strategicSphereForm.get('planOfActionDetails')['controls']);

    this.definiteDirectionForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        definiteDirection1: this.formBuilder.group({
          policies: this.formBuilder.group({
            policies1: '',
            policies2: '',
            policies3: '',
          }),
          demands: this.formBuilder.group({
            demand1: '',
            demand2: '',
            demand3: '',
          }),
          supply: this.formBuilder.group({
            supply1: '',
            supply2: '',
            supply3: '',
          }),
        }),
        definiteDirection2: this.formBuilder.group({
          strategicDesicion1: '',
          strategicDesicion2: '',
          strategicDesicion3: '',
          strategicDesicion4: '',
        }),
        definiteDirection3: this.formBuilder.group({
          acronym1: '',
          acronym2: '',
          acronym3: '',
          acronym4: '',
          acronym5: '',
          commitmentTostackholders: '',
          pledgeToCustomer: '',
          ourMission: '',
          ourVision: '',
          vision: '',
          year: '',
          strategicProposition: '',
        }),
      })
    });

    this.definiteDirectionArray.push(this.definiteDirectionForm.get('planOfActionDetails')['controls']);

    this.concreteForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        concreteCommecement1: this.formBuilder.group({
          txtfocus1: '',
          txtfocus2: '',
          txtfocus3: '',
          txtfocus4: '',
          txtareafocus1: '',
          txtareafocus2: '',
          txtareafocus3: '',
          txtareafocus4: '',
        }),
        concreteCommecement2: this.formBuilder.group({
          txtscope: '',
          txtinitiate: '',
          txtexecute: '',
          txtclose: '',
        }),
        concreteCommecement3: this.formBuilder.group({
          strategicInitiative1: '',
          strategicInitiative2: '',
          strategicInitiative3: '',
          strategicInitiative4: '',
          strategicInitiative5: '',
          strategicInitiative6: '',
        }),
      })
    });

    this.concreteArray.push(this.concreteForm.get('planOfActionDetails')['controls']);

    this.purposefulPlanningForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        strategyManagementProcess: this.formBuilder.group({
          strategicIssue: '',
          mission: '',
          useSwot: '',
          rollOut: '',
          industryMarketData: '',
          values: '',
          organizationalGoal: '',
          calander: '',
          customerInsights: '',
          vision: '',
          kpi: '',
          leverageTool: '',
          employeeInput: '',
          competitiveAdvantages: '',
          departmentGoals: '',
          adaptQuarterly: '',
          swot: '',
          orgWideStrategies: '',
          teamMemberGoals: '',
          updateAnnually: '',
          longTermObjectives: '',
          budget: '',
          forecast: '',
        }),
        balanceScoreCard: this.formBuilder.group({
          governanceLeadership1: '',
          governanceLeadership2: '',
          programServices1: '',
          programServices2: '',
          sustainableInfrastructure1: '',
          sustainableInfrastructure2: '',
          communityDevelopment1: '',
          communityDevelopment2: '',
          marketingSales1: '',
          marketingSales2: '',
          qualityCommunication1: '',
          qualityCommunication2: '',
        }),
        strategicPlanningFramework: this.formBuilder.group({
          strategicPlanning1: '',
          strategicPlanning2: '',
          strategicPlanning3: '',
          strategicPlanning4: '',
          strategicPlanning5: '',
          strategicPlanning6: '',
        }),
      })
    });

    this.purposefulPlanningArray.push(this.purposefulPlanningForm.get('planOfActionDetails')['controls']);

    this.growthFrameForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        strategic: this.formBuilder.group({
          strategy: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''
          }),
          sustainability: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''

          }),
          solutions: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''
          })
        }),
        managerial: this.formBuilder.group({
          system: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''
          }),
          styleOfLeadership: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''

          }),
          skills: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''
          })
        }),
        oprational: this.formBuilder.group({
          structure: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''
          }),
          sharedValues: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''
          }),
          standardOperationProducers: this.formBuilder.group({
            status: '',
            score: '',
            synergy: ''
          })
        })
      })
    });

    this.growthFrameArray.push(this.growthFrameForm.get('planOfActionDetails')['controls']);

    this.salesActionPlanForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        goal: '',
        customerSegmentTargets: '',
        leadingProspects: '',
        keyTeamMembers: '',
        establishedPlanOfAttack: '',
        outreachAwareness: '',
        brandEstablishment: '',
        networking: '',
        supportingReaserch: '',
        evaluationPlan: '',
        additionalNotes: '',
        strategicActionDescriptions: this.formBuilder.group({
          description1: '',
          description2: '',
          description3: '',
          description4: '',
          description5: '',
        }),
        deptResponsible: this.formBuilder.group({
          dept1: '',
          dept2: '',
          dept3: '',
          dept4: '',
          dept5: '',
        }),
        dateTobegin: this.formBuilder.group({
          beginDate1: '',
          beginDate2: '',
          beginDate3: '',
          beginDate4: '',
          beginDate5: '',
        }),
        dateTodue: this.formBuilder.group({
          dueDate1: '',
          dueDate2: '',
          dueDate3: '',
          dueDate4: '',
          dueDate5: '',
        }),
        resourcesRequired: this.formBuilder.group({
          resource1: '',
          resource2: '',
          resource3: '',
          resource4: '',
          resource5: '',
        }),
        desiredOutcome: this.formBuilder.group({
          outcome1: '',
          outcome2: '',
          outcome3: '',
          outcome4: '',
          outcome5: '',
        }),
      }),
    });

    this.workActionPlanForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        goal: '',
        benchmarkForSuccess: '',
        evaluationPlan: '',
        additionalNotes: '',
        strategicActionDescriptions: this.formBuilder.group({
          description1: '',
          description2: '',
          description3: '',
          description4: '',
          description5: '',
        }),
        deptResponsible: this.formBuilder.group({
          dept1: '',
          dept2: '',
          dept3: '',
          dept4: '',
          dept5: '',
        }),
        dateTobegin: this.formBuilder.group({
          beginDate1: '',
          beginDate2: '',
          beginDate3: '',
          beginDate4: '',
          beginDate5: '',
        }),
        dateTodue: this.formBuilder.group({
          dueDate1: '',
          dueDate2: '',
          dueDate3: '',
          dueDate4: '',
          dueDate5: '',
        }),
        resourcesRequired: this.formBuilder.group({
          resource1: '',
          resource2: '',
          resource3: '',
          resource4: '',
          resource5: '',
        }),
        potentialHazards: this.formBuilder.group({
          hazards1: '',
          hazards2: '',
          hazards3: '',
          hazards4: '',
          hazards5: '',
        }),
        desiredOutcome: this.formBuilder.group({
          outcome1: '',
          outcome2: '',
          outcome3: '',
          outcome4: '',
          outcome5: '',
        }),
      }),
    });

    this.correctiveActionPlanForm = this.formBuilder.group({
      section_id: [this.sectionId],
      sub_section_id: [this.subSectionId],
      user_id: [this.userObj.id],
      question_id: [this.questionId],
      vga_audit_number: [this.vgAudit.vga_audit_number],
      client_id: [this._clientId],
      planOfActionDetails: this.formBuilder.group({
        issueDescription: '',
        desiredOutcome: '',
        actionPlanSponser: '',
        additionalNotes: '',
        strategicAction: this.formBuilder.group({
          action1: '',
          action2: '',
          action3: '',
          action4: '',
          action5: '',
        }),
        deptResponsible: this.formBuilder.group({
          dept1: '',
          dept2: '',
          dept3: '',
          dept4: '',
          dept5: '',
        }),
        resourcesRequired: this.formBuilder.group({
          resource1: '',
          resource2: '',
          resource3: '',
          resource4: '',
          resource5: '',
        }),
        stackHolders: this.formBuilder.group({
          stackHolder1: '',
          stackHolder2: '',
          stackHolder3: '',
          stackHolder4: '',
          stackHolder5: '',
        }),
        constraints: this.formBuilder.group({
          constraints1: '',
          constraints2: '',
          constraints3: '',
          constraints4: '',
          constraints5: '',
        }),
        metric: this.formBuilder.group({
          metric1: '',
          metric2: '',
          metric3: '',
          metric4: '',
          metric5: '',
        }),
        dateTodue: this.formBuilder.group({
          dueDate1: '',
          dueDate2: '',
          dueDate3: '',
          dueDate4: '',
          dueDate5: '',
        }),
        priority: this.formBuilder.group({
          priority1: '',
          priority2: '',
          priority3: '',
          priority4: '',
          priority5: '',
        }),
        executionTimeline: this.formBuilder.group({
          executionTimeline1: '',
          executionTimeline2: '',
          executionTimeline3: '',
          executionTimeline4: '',
          executionTimeline5: '',
        }),
      }),
    });

    this.getReportDetails();
  }

  onSubmitOfStrategicSphere(){
    this.reportAnswerValues.addReportAnswerValue(this.strategicSphereForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.strategicSphereForm.disable();
        }
      }
    });
  }

  onSubmitOfDefiniteDirection(){
    this.reportAnswerValues.addReportAnswerValue(this.definiteDirectionForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.definiteDirectionForm.disable();
        }
      }
    });
  }

  onSubmitOfConcreteCommencement(){
    this.reportAnswerValues.addReportAnswerValue(this.concreteForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.concreteForm.disable();
        }
      }
    });
  }
  onSubmitOfPurposefulPlanning(){
    this.reportAnswerValues.addReportAnswerValue(this.purposefulPlanningForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.purposefulPlanningForm.disable();
        }
      }
    });
  }

  onSubmitOfGrowthFrame(){
    this.reportAnswerValues.addReportAnswerValue(this.growthFrameForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.growthFrameForm.disable();
        }
      }
    });
  }

  onSubmitOfSalesActionPlan(){
    this.reportAnswerValues.addReportAnswerValue(this.salesActionPlanForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.salesActionPlanForm.disable();
        }
      }
    });
  }

  onSubmitOfWorkActionPlan(){
    this.reportAnswerValues.addReportAnswerValue(this.workActionPlanForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.workActionPlanForm.disable();
        }
      }
    });
  }

  onSubmitOfcorrectiveActionPlan(){
    this.reportAnswerValues.addReportAnswerValue(this.correctiveActionPlanForm.value).subscribe((res: any) => {
      if(res){
        if(res.success === true){
          this.toastr.success('Answer submitted successfully!');
          this.correctiveActionPlanForm.disable();
        }
      }
    });
  }

  getVgAuditDetails() {
    this.vgAuditDetails = JSON.parse(this.vgAuditDetailsFromLocalStorage);
    this.vgAudit = (this.vgAuditDetails.audits) || (this.vgAuditDetails.res.audits);
    this._clientId = this.vgAudit.client.id;
    this.auditDetails = (this.vgAuditDetails.auditsDetails) || (this.vgAuditDetails.res.auditsDetails);
  }

  getReportDetails(){
    this.reportAnswerValues.getAnswerValues(this.questionId, this.vgAudit.vga_audit_number).subscribe((res: any) => {
      if(res.res.length > 0){
        this.strategicSphereForm.disable();
        this.definiteDirectionForm.disable();
        this.concreteForm.disable();
        this.purposefulPlanningForm.disable();
        this.growthFrameForm.disable();
        this.salesActionPlanForm.disable();
        this.workActionPlanForm.disable();
        this.correctiveActionPlanForm.disable();
      }
      let answerData: any = [];
      let strategicSphare1: any = [];
      let strategicSphare2: any = [];
      let strategicSphare3: any = [];
      let definiteDirection1: any = [];
      let definiteDirection2: any = [];
      let definiteDirection3: any = [];
      let concreteCommecement1: any = [];
      let concreteCommecement2: any = [];
      let concreteCommecement3: any = [];
      let strategyManagementProcess: any = [];
      let balanceScoreCard: any = [];
      let strategicPlanningFramework: any = [];
      let stretagyA: any = [];
      let stretagyB: any = [];
      let stretagyC: any = [];
      let strategic: any = [];
      let managerial: any = [];
      let oprational: any = [];
      let salesActionPlan: any = [];
      let workActionPlan: any = [];
      let correctiveActionPlan: any = [];

      res.res.filter((data: any) => {
        answerData.push(data.answer_value);
        answerData.filter((ans: any) => {
          strategicSphare1.push(ans.strategicSphere1);
          strategicSphare2.push(ans.strategicSphere2);
          strategicSphare3.push(ans.strategicSphere3);
          definiteDirection1.push(ans.definiteDirection1);
          definiteDirection2.push(ans.definiteDirection2);
          definiteDirection3.push(ans.definiteDirection3);
          concreteCommecement1.push(ans.concreteCommecement1);
          concreteCommecement2.push(ans.concreteCommecement2);
          concreteCommecement3.push(ans.concreteCommecement3);
          strategicPlanningFramework.push(ans.strategicPlanningFramework);
          balanceScoreCard.push(ans.balanceScoreCard);
          strategyManagementProcess.push(ans.strategyManagementProcess);
          stretagyA.push(ans.stretagy_A);
          stretagyB.push(ans.stretagy_B);
          stretagyC.push(ans.stretagy_C);
          strategic.push(ans.strategic);
          managerial.push(ans.managerial);
          oprational.push(ans.oprational);
          salesActionPlan.push(ans);
          workActionPlan.push(ans);
          correctiveActionPlan.push(ans);
        });
      });
      //Strategic Sphere question
      strategicSphare1?.filter((sphare1: any) => {
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere1']).controls['core'].patchValue(sphare1?.core);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere1']).controls['vitalInterests'].patchValue(sphare1?.vitalInterests);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere1']).controls['bufferZones'].patchValue(sphare1?.bufferZones);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere1']).controls['pivotalZones'].patchValue(sphare1?.pivotalZones);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere1']).controls['forwardPositions'].patchValue(sphare1?.forwardPositions);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere1']).controls['powerVaccums'].patchValue(sphare1?.powerVaccums);
      });

      strategicSphare2?.filter((sphare2: any) => {
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere2']).controls['textbox1'].patchValue(sphare2?.textbox1);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere2']).controls['textbox2'].patchValue(sphare2?.textbox2);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere2']).controls['textbox3'].patchValue(sphare2?.textbox3);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere2']).controls['textbox4'].patchValue(sphare2?.textbox4);
      });

      strategicSphare3?.filter((sphare3: any) => {
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative1']['controls']['txtIntiative1'].patchValue(sphare3?.initiative1?.txtIntiative1);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative1']['controls']['txtOperatingProperties1'].patchValue(sphare3?.initiative1?.txtOperatingProperties1);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative1']['controls']['txtOperatingProperties2'].patchValue(sphare3?.initiative1?.txtOperatingProperties2);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative2']['controls']['txtIntiative2'].patchValue(sphare3?.initiative2?.txtIntiative2);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative2']['controls']['txtOperatingProperties3'].patchValue(sphare3?.initiative2?.txtOperatingProperties3);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative2']['controls']['txtOperatingProperties4'].patchValue(sphare3?.initiative2?.txtOperatingProperties4);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative3']['controls']['txtIntiative3'].patchValue(sphare3?.initiative3?.txtIntiative3);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative3']['controls']['txtOperatingProperties5'].patchValue(sphare3?.initiative3?.txtOperatingProperties5);
        (<FormGroup>this.strategicSphereForm.controls['planOfActionDetails']['controls']['strategicSphere3']).controls['initiative3']['controls']['txtOperatingProperties6'].patchValue(sphare3?.initiative3?.txtOperatingProperties6);
      });

      //Definite direction question
      definiteDirection1?.filter((direction1: any) => {
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['policies']['controls']['policies1'].patchValue(direction1?.policies?.policies1);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['policies']['controls']['policies2'].patchValue(direction1?.policies?.policies2);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['policies']['controls']['policies3'].patchValue(direction1?.policies?.policies3);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['demands']['controls']['demand1'].patchValue(direction1?.demands?.demand1);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['demands']['controls']['demand2'].patchValue(direction1?.demands?.demand2);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['demands']['controls']['demand3'].patchValue(direction1?.demands?.demand3);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['supply']['controls']['supply1'].patchValue(direction1?.supply?.supply1);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['supply']['controls']['supply2'].patchValue(direction1?.supply?.supply2);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection1']).controls['supply']['controls']['supply3'].patchValue(direction1?.supply?.supply3);
      });

      definiteDirection2?.filter((direction2: any) => {
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection2']).controls['strategicDesicion1'].patchValue(direction2?.strategicDesicion1);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection2']).controls['strategicDesicion2'].patchValue(direction2?.strategicDesicion2);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection2']).controls['strategicDesicion3'].patchValue(direction2?.strategicDesicion3);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection2']).controls['strategicDesicion4'].patchValue(direction2?.strategicDesicion4);
      });

      definiteDirection3?.filter((direction3: any) => {
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['acronym1'].patchValue(direction3?.acronym1);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['acronym2'].patchValue(direction3?.acronym2);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['acronym3'].patchValue(direction3?.acronym3);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['acronym4'].patchValue(direction3?.acronym4);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['acronym5'].patchValue(direction3?.acronym5);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['commitmentTostackholders'].patchValue(direction3?.commitmentTostackholders);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['pledgeToCustomer'].patchValue(direction3?.pledgeToCustomer);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['ourMission'].patchValue(direction3?.ourMission);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['ourVision'].patchValue(direction3?.ourVision);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['vision'].patchValue(direction3?.vision);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['year'].patchValue(direction3?.year);
        (<FormGroup>this.definiteDirectionForm.controls['planOfActionDetails']['controls']['definiteDirection3']).controls['strategicProposition'].patchValue(direction3?.strategicProposition);
      });

      // Concrete commecement question

      concreteCommecement1?.filter((commecement1: any) => {
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtfocus1'].patchValue(commecement1?.txtfocus1);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtfocus2'].patchValue(commecement1?.txtfocus2);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtfocus3'].patchValue(commecement1?.txtfocus3);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtfocus4'].patchValue(commecement1?.txtfocus4);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtareafocus1'].patchValue(commecement1?.txtareafocus1);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtareafocus2'].patchValue(commecement1?.txtareafocus2);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtareafocus3'].patchValue(commecement1?.txtareafocus3);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement1']).controls['txtareafocus4'].patchValue(commecement1?.txtareafocus4);
      });

      concreteCommecement2?.filter((commecement2: any) => {
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement2']).controls['txtscope'].patchValue(commecement2?.txtscope);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement2']).controls['txtinitiate'].patchValue(commecement2?.txtinitiate);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement2']).controls['txtexecute'].patchValue(commecement2?.txtexecute);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement2']).controls['txtclose'].patchValue(commecement2?.txtclose);
      });

      concreteCommecement3?.filter((commecement3: any) => {
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement3']).controls['strategicInitiative1'].patchValue(commecement3?.strategicInitiative1);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement3']).controls['strategicInitiative2'].patchValue(commecement3?.strategicInitiative2);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement3']).controls['strategicInitiative3'].patchValue(commecement3?.strategicInitiative3);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement3']).controls['strategicInitiative4'].patchValue(commecement3?.strategicInitiative4);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement3']).controls['strategicInitiative5'].patchValue(commecement3?.strategicInitiative5);
        (<FormGroup>this.concreteForm.controls['planOfActionDetails']['controls']['concreteCommecement3']).controls['strategicInitiative6'].patchValue(commecement3?.strategicInitiative6);
      });

      //Purposeful planning question

      strategyManagementProcess?.filter((strategicPlanning: any) => {
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['strategicIssue'].patchValue(strategicPlanning?.strategicIssue);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['mission'].patchValue(strategicPlanning?.mission);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['useSwot'].patchValue(strategicPlanning?.useSwot);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['rollOut'].patchValue(strategicPlanning?.rollOut);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['industryMarketData'].patchValue(strategicPlanning?.industryMarketData);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['values'].patchValue(strategicPlanning?.values);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['organizationalGoal'].patchValue(strategicPlanning?.organizationalGoal);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['calander'].patchValue(strategicPlanning?.calander);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['customerInsights'].patchValue(strategicPlanning?.customerInsights);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['vision'].patchValue(strategicPlanning?.vision);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['kpi'].patchValue(strategicPlanning?.kpi);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['leverageTool'].patchValue(strategicPlanning?.leverageTool);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['employeeInput'].patchValue(strategicPlanning?.employeeInput);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['competitiveAdvantages'].patchValue(strategicPlanning?.competitiveAdvantages);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['departmentGoals'].patchValue(strategicPlanning?.departmentGoals);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['adaptQuarterly'].patchValue(strategicPlanning?.adaptQuarterly);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['swot'].patchValue(strategicPlanning?.swot);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['orgWideStrategies'].patchValue(strategicPlanning?.orgWideStrategies);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['teamMemberGoals'].patchValue(strategicPlanning?.teamMemberGoals);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['updateAnnually'].patchValue(strategicPlanning?.updateAnnually);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['longTermObjectives'].patchValue(strategicPlanning?.longTermObjectives);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['budget'].patchValue(strategicPlanning?.budget);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategyManagementProcess']).controls['forecast'].patchValue(strategicPlanning?.forecast);
      });

      balanceScoreCard?.filter((scoreCard: any) => {
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['governanceLeadership1'].patchValue(scoreCard?.governanceLeadership1);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['governanceLeadership2'].patchValue(scoreCard?.governanceLeadership2);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['programServices1'].patchValue(scoreCard?.programServices1);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['programServices2'].patchValue(scoreCard?.programServices2);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['sustainableInfrastructure1'].patchValue(scoreCard?.sustainableInfrastructure1);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['sustainableInfrastructure2'].patchValue(scoreCard?.sustainableInfrastructure2);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['communityDevelopment1'].patchValue(scoreCard?.communityDevelopment1);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['communityDevelopment2'].patchValue(scoreCard?.communityDevelopment2);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['marketingSales1'].patchValue(scoreCard?.marketingSales1);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['marketingSales2'].patchValue(scoreCard?.marketingSales2);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['qualityCommunication1'].patchValue(scoreCard?.qualityCommunication1);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['balanceScoreCard']).controls['qualityCommunication2'].patchValue(scoreCard?.qualityCommunication2);
      });

      strategicPlanningFramework?.filter((planningFramework: any) => {
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategicPlanningFramework']).controls['strategicPlanning1'].patchValue(planningFramework?.strategicPlanning1);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategicPlanningFramework']).controls['strategicPlanning2'].patchValue(planningFramework?.strategicPlanning2);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategicPlanningFramework']).controls['strategicPlanning3'].patchValue(planningFramework?.strategicPlanning3);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategicPlanningFramework']).controls['strategicPlanning4'].patchValue(planningFramework?.strategicPlanning4);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategicPlanningFramework']).controls['strategicPlanning5'].patchValue(planningFramework?.strategicPlanning5);
        (<FormGroup>this.purposefulPlanningForm.controls['planOfActionDetails']['controls']['strategicPlanningFramework']).controls['strategicPlanning6'].patchValue(planningFramework?.strategicPlanning6);
      });

      // GrowthFrame

      strategic.filter((strategics: any) => {
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['strategy']['controls']['status'].patchValue(strategics?.strategy.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['strategy']['controls']['score'].patchValue(strategics?.strategy.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['strategy']['controls']['synergy'].patchValue(strategics?.strategy.synergy);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['sustainability']['controls']['status'].patchValue(strategics?.sustainability.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['sustainability']['controls']['score'].patchValue(strategics?.sustainability.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['sustainability']['controls']['synergy'].patchValue(strategics?.sustainability.synergy);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['solutions']['controls']['status'].patchValue(strategics?.solutions.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['strategic']).controls['solutions']['controls']['score'].patchValue(strategics?.solutions.score);
      });

      managerial.filter((mangerial: any) => {
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['system']['controls']['status'].patchValue(mangerial?.system.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['system']['controls']['score'].patchValue(mangerial?.system.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['system']['controls']['synergy'].patchValue(mangerial?.system.synergy);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['styleOfLeadership']['controls']['status'].patchValue(mangerial?.styleOfLeadership.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['styleOfLeadership']['controls']['score'].patchValue(mangerial?.styleOfLeadership.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['styleOfLeadership']['controls']['synergy'].patchValue(mangerial?.styleOfLeadership.synergy);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['skills']['controls']['status'].patchValue(mangerial?.skills.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['skills']['controls']['score'].patchValue(mangerial?.skills.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['managerial']).controls['skills']['controls']['synergy'].patchValue(mangerial?.skills.synergy);
      });

      oprational.filter((operational: any) => {
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['structure']['controls']['status'].patchValue(operational?.structure.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['structure']['controls']['score'].patchValue(operational?.structure.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['structure']['controls']['synergy'].patchValue(operational?.structure.synergy);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['sharedValues']['controls']['status'].patchValue(operational?.sharedValues.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['sharedValues']['controls']['score'].patchValue(operational?.sharedValues.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['sharedValues']['controls']['synergy'].patchValue(operational?.sharedValues.synergy);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['standardOperationProducers']['controls']['status'].patchValue(operational?.standardOperationProducers.status);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['standardOperationProducers']['controls']['score'].patchValue(operational?.standardOperationProducers.score);
        (<FormGroup>this.growthFrameForm.controls['planOfActionDetails']['controls']['oprational']).controls['standardOperationProducers']['controls']['synergy'].patchValue(operational?.standardOperationProducers.synergy);
      });

      // Sales Action Plan
      salesActionPlan.filter((actions: any) => {
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['goal'].patchValue(actions.goal));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['customerSegmentTargets'].patchValue(actions.customerSegmentTargets));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['leadingProspects'].patchValue(actions.leadingProspects));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['keyTeamMembers'].patchValue(actions.keyTeamMembers));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['establishedPlanOfAttack'].patchValue(actions.establishedPlanOfAttack));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['outreachAwareness'].patchValue(actions.outreachAwareness));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['brandEstablishment'].patchValue(actions.brandEstablishment));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['networking'].patchValue(actions.networking));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['supportingReaserch'].patchValue(actions.supportingReaserch));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['evaluationPlan'].patchValue(actions.evaluationPlan));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['additionalNotes'].patchValue(actions.additionalNotes));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description1'].patchValue(actions.strategicActionDescriptions?.description1));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description2'].patchValue(actions.strategicActionDescriptions?.description2));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description3'].patchValue(actions.strategicActionDescriptions?.description3));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description4'].patchValue(actions.strategicActionDescriptions?.description4));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description5'].patchValue(actions.strategicActionDescriptions?.description5));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept1'].patchValue(actions.deptResponsible?.dept1));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept2'].patchValue(actions.deptResponsible?.dept2));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept3'].patchValue(actions.deptResponsible?.dept3));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept4'].patchValue(actions.deptResponsible?.dept4));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept5'].patchValue(actions.deptResponsible?.dept5));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate1'].patchValue(actions.dateTobegin?.beginDate1));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate2'].patchValue(actions.dateTobegin?.beginDate2));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate3'].patchValue(actions.dateTobegin?.beginDate3));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate4'].patchValue(actions.dateTobegin?.beginDate4));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate5'].patchValue(actions.dateTobegin?.beginDate5));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate1'].patchValue(actions.dateTodue?.dueDate1));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate2'].patchValue(actions.dateTodue?.dueDate2));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate3'].patchValue(actions.dateTodue?.dueDate3));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate4'].patchValue(actions.dateTodue?.dueDate4));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate5'].patchValue(actions.dateTodue?.dueDate5));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource1'].patchValue(actions.resourcesRequired?.resource1));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource2'].patchValue(actions.resourcesRequired?.resource2));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource3'].patchValue(actions.resourcesRequired?.resource3));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource4'].patchValue(actions.resourcesRequired?.resource4));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource5'].patchValue(actions.resourcesRequired?.resource5));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome1'].patchValue(actions.desiredOutcome?.outcome1));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome2'].patchValue(actions.desiredOutcome?.outcome2));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome3'].patchValue(actions.desiredOutcome?.outcome3));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome4'].patchValue(actions.desiredOutcome?.outcome4));
        (<FormGroup>this.salesActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome5'].patchValue(actions.desiredOutcome?.outcome5));
      });

      // Work action plan
      workActionPlan.filter((actions: any) => {
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['goal'].patchValue(actions.goal));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['benchmarkForSuccess'].patchValue(actions.benchmarkForSuccess));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['evaluationPlan'].patchValue(actions.evaluationPlan));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['additionalNotes'].patchValue(actions.additionalNotes));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description1'].patchValue(actions.strategicActionDescriptions?.description1));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description2'].patchValue(actions.strategicActionDescriptions?.description2));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description3'].patchValue(actions.strategicActionDescriptions?.description3));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description4'].patchValue(actions.strategicActionDescriptions?.description4));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['strategicActionDescriptions'].controls['description5'].patchValue(actions.strategicActionDescriptions?.description5));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept1'].patchValue(actions.deptResponsible?.dept1));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept2'].patchValue(actions.deptResponsible?.dept2));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept3'].patchValue(actions.deptResponsible?.dept3));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept4'].patchValue(actions.deptResponsible?.dept4));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept5'].patchValue(actions.deptResponsible?.dept5));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate1'].patchValue(actions.dateTobegin?.beginDate1));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate2'].patchValue(actions.dateTobegin?.beginDate2));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate3'].patchValue(actions.dateTobegin?.beginDate3));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate4'].patchValue(actions.dateTobegin?.beginDate4));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTobegin'].controls['beginDate5'].patchValue(actions.dateTobegin?.beginDate5));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate1'].patchValue(actions.dateTodue?.dueDate1));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate2'].patchValue(actions.dateTodue?.dueDate2));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate3'].patchValue(actions.dateTodue?.dueDate3));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate4'].patchValue(actions.dateTodue?.dueDate4));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate5'].patchValue(actions.dateTodue?.dueDate5));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource1'].patchValue(actions.resourcesRequired?.resource1));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource2'].patchValue(actions.resourcesRequired?.resource2));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource3'].patchValue(actions.resourcesRequired?.resource3));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource4'].patchValue(actions.resourcesRequired?.resource4));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource5'].patchValue(actions.resourcesRequired?.resource5));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome1'].patchValue(actions.desiredOutcome?.outcome1));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome2'].patchValue(actions.desiredOutcome?.outcome2));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome3'].patchValue(actions.desiredOutcome?.outcome3));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome4'].patchValue(actions.desiredOutcome?.outcome4));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].controls['outcome5'].patchValue(actions.desiredOutcome?.outcome5));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['potentialHazards'].controls['hazards1'].patchValue(actions.potentialHazards?.hazards1));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['potentialHazards'].controls['hazards2'].patchValue(actions.potentialHazards?.hazards2));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['potentialHazards'].controls['hazards3'].patchValue(actions.potentialHazards?.hazards3));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['potentialHazards'].controls['hazards4'].patchValue(actions.potentialHazards?.hazards4));
        (<FormGroup>this.workActionPlanForm.controls['planOfActionDetails']['controls']['potentialHazards'].controls['hazards5'].patchValue(actions.potentialHazards?.hazards5));
      });

      // Corrective action plan
      correctiveActionPlan.filter((actions: any) => {
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['issueDescription'].patchValue(actions.issueDescription));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['desiredOutcome'].patchValue(actions.desiredOutcome));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['actionPlanSponser'].patchValue(actions.actionPlanSponser));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['additionalNotes'].patchValue(actions.additionalNotes));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['strategicAction'].controls['action1'].patchValue(actions.strategicAction?.action1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['strategicAction'].controls['action2'].patchValue(actions.strategicAction?.action2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['strategicAction'].controls['action3'].patchValue(actions.strategicAction?.action3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['strategicAction'].controls['action4'].patchValue(actions.strategicAction?.action4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['strategicAction'].controls['action5'].patchValue(actions.strategicAction?.action5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept1'].patchValue(actions.deptResponsible?.dept1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept2'].patchValue(actions.deptResponsible?.dept2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept3'].patchValue(actions.deptResponsible?.dept3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept4'].patchValue(actions.deptResponsible?.dept4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['deptResponsible'].controls['dept5'].patchValue(actions.deptResponsible?.dept5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource1'].patchValue(actions.resourcesRequired?.resource1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource2'].patchValue(actions.resourcesRequired?.resource2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource3'].patchValue(actions.resourcesRequired?.resource3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource4'].patchValue(actions.resourcesRequired?.resource4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['resourcesRequired'].controls['resource5'].patchValue(actions.resourcesRequired?.resource5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['stackHolders'].controls['stackHolder1'].patchValue(actions.stackHolders?.stackHolder1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['stackHolders'].controls['stackHolder2'].patchValue(actions.stackHolders?.stackHolder2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['stackHolders'].controls['stackHolder3'].patchValue(actions.stackHolders?.stackHolder3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['stackHolders'].controls['stackHolder4'].patchValue(actions.stackHolders?.stackHolder4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['stackHolders'].controls['stackHolder5'].patchValue(actions.stackHolders?.stackHolder5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['constraints'].controls['constraints1'].patchValue(actions.constraints?.constraints1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['constraints'].controls['constraints2'].patchValue(actions.constraints?.constraints2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['constraints'].controls['constraints3'].patchValue(actions.constraints?.constraints3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['constraints'].controls['constraints4'].patchValue(actions.constraints?.constraints4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['constraints'].controls['constraints5'].patchValue(actions.constraints?.constraints5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['metric'].controls['metric1'].patchValue(actions.metric?.metric1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['metric'].controls['metric2'].patchValue(actions.metric?.metric2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['metric'].controls['metric3'].patchValue(actions.metric?.metric3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['metric'].controls['metric4'].patchValue(actions.metric?.metric4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['metric'].controls['metric5'].patchValue(actions.metric?.metric5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate1'].patchValue(actions.dateTodue?.dueDate1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate2'].patchValue(actions.dateTodue?.dueDate2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate3'].patchValue(actions.dateTodue?.dueDate3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate4'].patchValue(actions.dateTodue?.dueDate4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['dateTodue'].controls['dueDate5'].patchValue(actions.dateTodue?.dueDate5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['priority'].controls['priority1'].patchValue(actions.priority?.priority1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['priority'].controls['priority2'].patchValue(actions.priority?.priority2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['priority'].controls['priority3'].patchValue(actions.priority?.priority3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['priority'].controls['priority4'].patchValue(actions.priority?.priority4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['priority'].controls['priority5'].patchValue(actions.priority?.priority5));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['executionTimeline'].controls['executionTimeline1'].patchValue(actions.executionTimeline?.executionTimeline1));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['executionTimeline'].controls['executionTimeline2'].patchValue(actions.executionTimeline?.executionTimeline2));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['executionTimeline'].controls['executionTimeline3'].patchValue(actions.executionTimeline?.executionTimeline3));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['executionTimeline'].controls['executionTimeline4'].patchValue(actions.executionTimeline?.executionTimeline4));
        (<FormGroup>this.correctiveActionPlanForm.controls['planOfActionDetails']['controls']['executionTimeline'].controls['executionTimeline5'].patchValue(actions.executionTimeline?.executionTimeline5));
      });

    });


  }
  onSubmitOfActionPlan(){}

}
