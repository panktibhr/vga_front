import { Component, Input, OnInit } from '@angular/core';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss']
})
export class DropDownComponent implements OnInit {
  sections: any = [];
  optionValue: any;
  dropDownValues: any = [];
  dropdown: any = [];
  values: any;
  answerDetail: any;

  constructor(private sectionService: SectionMasterService) {
  }

  ngOnInit(): void {
    this.getAllSections();
    this.getDropDownValue();
  }

  @Input() answerValue:any;
  @Input() answerValueType: any;
  @Input() createform: any;
  @Input() vgaReportForm: any;
  @Input() questions:any;
  @Input() answerType:any;
  @Input() title:any;
  @Input() subQuestionIdArr:any;
  @Input() subQuestionsArr:any;
  @Input() arr:any;
  @Input() reportAnswerValues:any;
  @Input() submittedAnswer: any;
  @Input() byDefaultValue: any;

  getAllSections(){
    this.sectionService.getAllSec().subscribe((res: any) => {
      this.sections = res.res;
      this.sections.forEach((res: any)=>{
        if(res.section_name === 'Valuegrowth Matrix'){
        }
      })
    })
  }

  getDropDownValue(){
    setTimeout(() => {
      let arr = [];
      document.querySelectorAll('option').forEach(opt=>{
        arr.push(opt.value)
        if( opt.value == this.byDefaultValue ){
          opt.selected=true;
        }
      });
    }, 3000);
  }

}
