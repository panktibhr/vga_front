import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VgaReportComponent } from './vga-report.component';

describe('VgaReportComponent', () => {
  let component: VgaReportComponent;
  let fixture: ComponentFixture<VgaReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VgaReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VgaReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
