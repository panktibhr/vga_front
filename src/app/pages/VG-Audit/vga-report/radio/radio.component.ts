import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit{

  @Input() sectionName:any;
  @Input() answerValue:any;
  @Input() answerValueType: any;
  @Input() createform: any;
  @Input() vgaReportForm: any;
  @Input() questions:any;
  @Input() answerType:any;
  @Input() title:any;
  @Input() subQuestionIdArr:any;
  @Input() subQuestionsArr:any;
  @Input() arr:any;
  @Input() radio:any;
  @Input() radio2:any;
  @Input() reportAnswerValues:any;
  @Input() submittedAnswer: any;
  @Input() onClickOfRadio: any;
  @Input() radioBtn: any;
  @Input() byDefaultValue: any;
  @Input() question: any;
  @Input() reportAnswerValue:any;
  @Input() counter:any;
  @Input() subQuestionsForCompetencyMapping:any;
  @Input() questionType: any
  @Input() seqNumber: any
  // @Input() answers:any;
  sectionId: any;
  answers:any = [];

  constructor(private toastr: ToastrService, private actRoute: ActivatedRoute){
    this.actRoute.params.subscribe((res: any) => {
      this.sectionId = res.secId;
      setTimeout(() => {
        let answers = CryptoJS.AES.decrypt(localStorage.getItem('answers'), 'secret key 123').toString(CryptoJS.enc.Utf8);
        this.answers = JSON.parse(answers);
        // console.log(`this.answers --- `, this.answers)
      },1000)
    })
  }

  ngOnInit(){
    // console.log(this.seqNumber)
    // setTimeout(() => {
      this.toastr.info('', 'Wait until data is loading...', {
        timeOut: 2300,
      });
      this.getRadioValue();
    // },3000)
  }

  getRadioValue(){
    setTimeout(() => {
      if(this.sectionName !== 'Competency Mapping'){
        if((this.reportAnswerValues && this.reportAnswerValues.length > 0) || (this.reportAnswerValue[this.counter] && this.reportAnswerValue[this.counter].length > 0)){
          this.reportAnswerValues?.map((res: any)=> {
            document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
              obj.name === res.title && obj.value === res.answer_value ? obj.checked = true : null;
            });
          });
          this.reportAnswerValue[this.counter].map((res: any) => {
            document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
              obj.name === res.title && obj.value === res.answer_value ? obj.checked = true : null;
            });
          });

        } else{
          document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
            obj.value === this.byDefaultValue ? obj.checked = true : null;
          });

        }
      } else {
        if(this.reportAnswerValues.length !== 0){
          if((this.reportAnswerValues && this.reportAnswerValues.length > 0) || (this.reportAnswerValue[this.counter] && this.reportAnswerValue[this.counter].length > 0)){
            this.reportAnswerValues?.map((res: any)=> {
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
                obj.name === res.title && obj.value === res.answer_value ? obj.checked = true : null;
              });
            });
            this.reportAnswerValue[this.counter].map((res: any) => {
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
                obj.name === res.title && obj.value === res.answer_value ? obj.checked = true : null;
              });
            });

          } else{
            document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
              obj.value === this.byDefaultValue ? obj.checked = true : null;
            });

          }
        } else {
          if(this.answers && this.answers.length > 0){
            this.answers.map((res: any)=> {
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
                obj.name === res.title && obj.value === res.answer_value ? obj.checked = true : null;
                if(obj.checked === true){
                  obj.disabled = true;
                }
              });
            });

          } else{
            document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
              obj.value === this.byDefaultValue ? obj.checked = true : null;
            });
          }
        }
      }
    }, 3000);
  }

}
