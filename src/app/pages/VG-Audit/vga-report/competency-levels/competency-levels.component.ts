import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';
import { clientEmployees } from 'src/app/shared/models/client-employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-competency-levels',
  templateUrl: './competency-levels.component.html',
  styleUrls: ['./competency-levels.component.scss']
})
export class CompetencyLevelsComponent implements OnInit {

  _clientEmp: clientEmployees;
  clientId: string;
  topLevel: any = 'Top Level';
  managerialLevel: any = "Managerial Level";
  executiveLevel: any = "Executive Level";
  disableButton: any = false;
  disabledButton: any = true;
  value: any;

  constructor(private location: Location, private clientEmployeeService: ClientEmployeeService, private router: Router) {
    this._clientEmp = new clientEmployees();
    this.clientId = sessionStorage.getItem('clientId');
  }

  ngOnInit(): void {
  }

  getEmployees(level){
    console.log(this.clientId, level)
    this.router.navigate(['pages/VGAudit/VGAuditReport/levels/', this.clientId, level])
  }

  backToSubSection(){
    this.location.back();
  }

}
