import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioQuestionsComponent } from './radio-question.component';

describe('RadioQuestionsComponent', () => {
  let component: RadioQuestionsComponent;
  let fixture: ComponentFixture<RadioQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
