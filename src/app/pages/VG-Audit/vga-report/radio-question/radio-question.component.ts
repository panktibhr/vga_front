import { Location } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';
import { EmployeePerformanceService } from 'src/app/shared/services/employee-performance/employee-performace.service';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { ReportAnswerValuesService } from 'src/app/shared/services/report-AnswerValues/report-answer-values.service';
import { environment } from 'src/environments/environment';
import { RadioComponent } from '../radio/radio.component';

@Component({
  selector: 'app-radio-questions',
  templateUrl: './radio-question.component.html',
  styleUrls: ['./radio-question.component.scss'],
})
export class RadioQuestionsComponent implements OnInit, OnDestroy {
  counter = 0;
  secId: any;
  status: any;
  userObj: any;
  roleType: any;
  questionId: any;
  answerType: any;
  subSectionID: any;
  questionName: any;
  clientEmployeeId: any;
  answerDetailObj: any;
  vgAuditDetails: any;
  byDefaultValue: any;
  sequence_number: any;
  clientEmployeeName: any;
  vgaReportForm: FormGroup;
  vgAuditDetailsFromLocalStorage: string;
  vgAudit: any = [];
  subQuestionId: any = [];
  getQuestionData: any =[];
  reportAnswerValue:any = [];
  answerDetailArray: any = [];
  subQuestionArray: any = [];
  subQuestionsArray: any = [];
  empPerformanceQuestion: any = [];
  radio = ['Exceptional', 'Marginal', 'Satisfactory', 'Unsatisfactory'];
  @ViewChild(RadioComponent) childRadio: RadioComponent;
  subscription: Subscription;

  constructor(private location: Location, private questionService: QuestionMasterService, private actRoute: ActivatedRoute, private fb: FormBuilder, private router: Router, private reportAnswerService: ReportAnswerValuesService,private toastr: ToastrService, private clientEmployeeService: ClientEmployeeService, private employeePerformanceService: EmployeePerformanceService) {
    this.actRoute.paramMap.subscribe((url: any) => {
      this.secId = url.params.secId;
      this.questionId = url.params.queId;
      this.clientEmployeeId = url.params.employeeId;
      this.sequence_number = url.params.seqNo;
    });
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
    this.vgAuditDetailsFromLocalStorage = CryptoJS.AES.decrypt(localStorage.getItem('vgAuditDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);

    setTimeout(() => {
      this.childRadio.getRadioValue();
    }, 2000);

  }

  ngOnInit(): void {
    this.getQuestionOfEmployeePerformance();
    this.getVgAuditDetails();
    this.getEmpName();
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  onClickOfPrevious() {
    this.location.back();
  }

  getQuestionOfEmployeePerformance(){
    const sectionObj = {"sectionId" : this.secId, "employeeId" : this.clientEmployeeId};
    this.subscription = this.employeePerformanceService.getQuestionOfEmployeePerformance(sectionObj).subscribe((res: any) => {
      this.getQuestionData.push(res.res);
      this.getQuestionData[0].map((question: any) => {
        if(question.id === this.questionId){
          this.answerType = question.answer_type;
          this.byDefaultValue = question.by_default_value;
          this.questionName = question.question;
          const empPerformanceQuestionArray: any = question.employeePerformance;
          empPerformanceQuestionArray.forEach(element => {
            this.empPerformanceQuestion.push(element);
            this.subQuestionArray.push(element.subQuestions);
            this.subQuestionId.push(element.id);
            this.reportAnswerValue.push(element.reportanswers);
          });
        }
      });
      this.compareQuestionsData();
    });
  }

  createform() {
    this.vgaReportForm = this.fb.group({
      UserId: [this.userObj.id],
      QuestionId: [this.questionId],
      AnswerDetails: this.fb.array(this.subQuestionsArray),
      remarks: [''],
      consultantReview: [''],
      vgaAuditNumber: [this.vgAudit.vga_audit_number],
      status: ['Reviewed', []],
      employeeId: [''],
      employeePerformanceId: ['']
    })
  }

  BuildFormDynamic(obj): FormGroup {
    return this.fb.group({
      TitleId: [obj.titleId],
      Title: [obj.Title],
      AnswerValue: ['', Validators.required]
    })
  }

  getVgAuditDetails() {
    this.vgAuditDetails = JSON.parse(this.vgAuditDetailsFromLocalStorage);
    this.vgAudit = this.vgAuditDetails.audits || this.vgAuditDetails.res.audits;
  }

  compareQuestionsData(){
    const remarksElement: any = document.getElementById('remarks');
    const consultant: any = document.getElementById('consultant');
    const consultantReviews: any = document.getElementById('consultantReview');
    const status:any = document.getElementById('status');
    setTimeout(() => {
      if (this.roleType === 'Consultant') {
        document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
        remarksElement.disabled = true;
      }
    },1000);
    if(this.questionId === this.empPerformanceQuestion[this.counter]?.question_id){
      this.subQuestionArray.map((res: any) => {
        res.forEach(element => {
          if(element.employee_performance_questions_id === this.subQuestionId[this.counter]){
            this.answerDetailObj = {
              "titleId" : element.id,
              "Title" : element.title
            }
            this.subQuestionsArray.push(this.answerDetailObj);
            this.answerDetailArray.push(this.BuildFormDynamic(this.answerDetailObj));
          }
        });
        this.reportAnswerValue[this.counter].map((res: any)=> {
          let remarks = res.remarks;
          let consultantReview = res.consultant_review;
          this.status = res.question_status;
          setTimeout(() => {
            remarksElement.value = '';
            remarksElement.value = remarks;
            if(consultantReview && this.roleType !== 'Consultant'){
              consultant.style.visibility = "visible";
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              remarksElement.disabled = true;
              consultantReviews.value = consultantReview;
              consultantReviews.disabled = true;
              this.vgaReportForm.get('status').patchValue(this.status);
              status.disabled = true;
            } else {
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              remarksElement.disabled = true;
              this.vgaReportForm.get('consultantReview').enable();
              this.vgaReportForm.get('status').patchValue(this.status);
              consultantReviews.innerText = consultantReview;
              this.vgaReportForm.get('status').enable();
            }
          },2000);
        });
        if((this.reportAnswerValue.length === 0 && this.roleType !== 'Consultant') || ( this.roleType !== 'Consultant')){
          if(consultant)
            consultant.style.visibility = "hidden";
        } else {
          if(consultant)
            consultant.style.visibility = "visible";
        }
      });
      this.createform();
    }
  }

  OnNextButton(){
    this.counter += 1;
    this.subQuestionsArray = [];
    const remarks: any = document.getElementById('remarks');
    const consultantReview: any = document.getElementById('consultantReview');
    remarks.value = '';
    remarks.disabled = false;
    consultantReview.value = '';
    if(this.counter == this.subQuestionArray.length - 1){
      document.getElementById('next').innerText = "Close";
    }
    this.childRadio?.getRadioValue();
    if(this.counter == this.subQuestionArray.length){
      this.router.navigate([`/pages/VGAudit/vgaudits/${this.secId}/${this.questionId}/${this.sequence_number}`]);
    }
    this.compareQuestionsData();
  }

  onSubmit(){
    const remarks: any = document.getElementById('remarks');
    const consultantReview: any = document.getElementById('consultantReview');
    const consultant: any = document.getElementById('consultant');
    // this.vgaReportForm.markAllAsTouched();
    if(this.vgaReportForm.valid){
      let reportAnswerForm = {
        'UserId': this.vgaReportForm.get('UserId').value,
        'QuestionId': this.vgaReportForm.get('QuestionId').value,
        'AnswerDetails': this.vgaReportForm.get('AnswerDetails').value,
        'remarks': remarks.value,
        'vgaAuditNumber': this.vgaReportForm.get('vgaAuditNumber').value,
        'employeeId': this.clientEmployeeId,
        'employeePerformanceQuestionId': this.empPerformanceQuestion[this.counter].id,
        'consultantReview': consultantReview.value,
        'status': this.vgaReportForm.get('status').value
      }
      this.reportAnswerService.employeePerformanceValue({ "Answer": reportAnswerForm }).subscribe((res: any) => {
        if(res){
          let object = {
            'userId': this.vgaReportForm.get('UserId').value,
            'employeeId': this.clientEmployeeId,
            'questionId': this.questionId,
            'vgAuditNo': this.vgAudit.vga_audit_number,
            'sectionId' : this.secId,
            'employeePerformanceQuestionId': this.empPerformanceQuestion[this.counter].id
          }
          this.employeePerformanceService.getAnswersOfEmployeePerformance(object).subscribe(() => {});
          document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
          remarks.disabled = true;
          consultant.disabled = true;
          let vgAuditNo = {
            'vgAuditNo' : this.vgAudit.vga_audit_number
          }
          let obj = {
            'employeeId': this.clientEmployeeId,
            'questionId': this.questionId,
            'vgAuditNo': this.vgAudit.vga_audit_number
          }
          this.questionService.postVgAuditReportObj(this.secId, vgAuditNo).subscribe((res: any) => {
            if(res){
              this.toastr.success('Answer saved successfully!');
            }
          });
        }
      },(error: HttpErrorResponse) => {
        if (error) {
          if (error.error.message === "please, submit the answers first!") {
            this.toastr.error('Please select atleast one value!')
          } else if(error.error.message === "Answers of this Questions are already Submitted."){
            this.toastr.error('Response already submitted');
          } else if(error.error.errors[0].msg === "answer value required."){
            this.toastr.error('Please select all values');
          }
        }
      });
    }
  }

  onClickOfRadio(target){
    let answerDetail = this.vgaReportForm.get('AnswerDetails').value;
    let title;
    const ansDetails = answerDetail.map((obj: any) => {
      title = obj.Title;
      if(title == target.name){
        obj.AnswerValue = target.value;
        return obj;
      } else {
        return obj;
      }
    });
    this.vgaReportForm.get('AnswerDetails').patchValue(ansDetails);
  }

  getEmpName(){
    this.subscription = this.clientEmployeeService.getClientEmployee().subscribe((res: any) => {
      let response = res.res;
      response.forEach(element => {
        if(element.id === this.clientEmployeeId){
          this.clientEmployeeName = element.name;
        }
      });
    });
  }

}
