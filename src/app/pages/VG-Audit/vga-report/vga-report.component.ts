import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerValue } from 'src/app/shared/models/questions';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { environment } from 'src/environments/environment';
import { ReportAnswerValuesService } from '../../../shared/services/report-AnswerValues/report-answer-values.service';
import * as CryptoJS from 'crypto-js';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import { VgAuditService } from 'src/app/shared/services/vg-audit/vg-audit.service';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';
import { EmployeePerformanceService } from 'src/app/shared/services/employee-performance/employee-performace.service';
import { DropDownComponent } from './drop-down/drop-down.component';
import { RadioComponent } from './radio/radio.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RadioQuestionsComponent } from './radio-question/radio-question.component';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';
import { clientEmployees } from 'src/app/shared/models/client-employee';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/services/data.service';
import { EmployeePerformance } from 'src/app/shared/models/employee-performance';

@Component({
  selector: 'app-vga-report',
  templateUrl: './vga-report.component.html',
  styleUrls: ['./vga-report.component.scss']
})
export class VgaReportComponent implements OnInit, OnDestroy{

  answerType: any;
  answerValueType: any;
  answerValue: any = [];
  questionId: any;
  questions: any;
  seqNo: any;
  vgaReportForm: FormGroup;
  ansValue: any = AnswerValue;
  queId: any;
  title: any = [];
  sectionId: any;
  subSectionId: any;
  titleOfQuestion: any;
  subQuestionsArr: any = [];
  arr: any = [];
  userObj: any;
  titleId: any;
  subQuestionIdArr: any = [];
  answer: any;
  getQuestionsBySectionOrSubSection: any = [];
  uniqueOrderData: any[];
  submittedAnserValues: any;
  reportAnswerValues: any = [];
  submittedAnswer: any;
  vgAudit: any = [];
  vgAuditDetails: any;
  vgAuditDetailsFromLocalStorage: any;
  que: any;
  roleType: any;
  consultantReview: string='';
  clientName: any;
  auditDetails: any;
  secId: any;
  auditData: any = [];
  byDefaultValueofSubSection: any;
  byDefaultValueofquestion: any;
  sectionDetails: any;
  _secId: any;
  status: any;
  addEmployeeForm: FormGroup;
  employeName: any = [];
  subQuestions: any = [];
  emp_que_id: any;
  clientEmp: clientEmployees;
  subSectionID: any;
  isEditable: boolean;
  questionsId: any;
  sequenceNo: any;
  clientEmployeName: any;
  listOfEmployees: any = [];
  selectedEmployeeId: any;
  managerEmployeeName: any = [];
  page: number = 1;
  previousPage: any;
  totalItems: any;
  employeePerformance: EmployeePerformance;
  radio = ['Exceptional', 'Marginal', 'Satisfactory', 'Unsatisfactory'];
  radio2 = ['5', '4', '3', '2', '1'];
  @ViewChild(DropDownComponent) childDropDown: DropDownComponent;
  @ViewChild(RadioComponent) childRadio: RadioComponent;
  subscription: Subscription;
  seqNumber: any;
  vgaAuditNumber: any;
  listedEmployeesId: any = [];
  subSubQuestions: any = [];
  sectionName: any;
  subQuestionsForCompetencyMapping: any = [];
  questionType: any;
  topLevelQuestions: any;
  managerialLevelQuestions: any;
  executiveLevelQuestions: any;
  clientEmployeeNameForCompetencyMapping: string;
  clientEmpId: any;
  answers: any;
  remarks: any;

  constructor(private actRoute: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private reportAnswerService: ReportAnswerValuesService, private toastr: ToastrService,
    private location: Location, private vgAuditService: VgAuditService, private clientService: ClientsService,
    private questionService: QuestionMasterService,
    private modalService: NgbModal, private clientEmployeeService: ClientEmployeeService, private clientEmpName: DataService, private employeePerformanceService: EmployeePerformanceService
    ) {
    this.clientEmp = new clientEmployees();
    this.employeePerformance = new EmployeePerformance();
    this.actRoute.paramMap.subscribe((url: any) => {
      this.secId = url.params.secId;
      this.questionsId = url.params.queId;
      this.sequenceNo = url.params.seqNo;
      this.clientEmpId = url.params.clientEmpId;
    });
    console.log(this.actRoute)
    this.getEmpName();
    if( localStorage.getItem('clientEmployeeName')) {
      this.clientEmployeeNameForCompetencyMapping = localStorage?.getItem('clientEmployeeName');
    }
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
    this.sectionDetails = CryptoJS.AES.decrypt(localStorage.getItem('sectionDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
    this._secId = JSON.parse(this.sectionDetails)
    this.sectionName = this._secId.section_name;
    this.vgAuditDetailsFromLocalStorage = CryptoJS.AES.decrypt(localStorage.getItem('vgAuditDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
    this.ansValue = new AnswerValue();
    this.actRoute.params.subscribe((res: any) => {
      this.sectionId = res.secId;
      this.subSectionId = res.subSecId;
      this.questionId = res.queId;
      this.seqNo = +res.seqNo;
      this.que = CryptoJS.AES.decrypt(localStorage.getItem('lengthOfQuestion'), 'secret key 123').toString(CryptoJS.enc.Utf8);
      let topLevelQuestions = CryptoJS.AES.decrypt(localStorage.getItem('topLevelQuestions'), 'secret key 123').toString(CryptoJS.enc.Utf8);
      let answers = CryptoJS.AES.decrypt(localStorage.getItem('answers'), 'secret key 123').toString(CryptoJS.enc.Utf8);
      let managerialLevelQuestions = CryptoJS.AES.decrypt(localStorage.getItem('managerialLevelQuestions'), 'secret key 123').toString(CryptoJS.enc.Utf8);
      let executiveLevelQuestions = CryptoJS.AES.decrypt(localStorage.getItem('executiveLevelQuestions'), 'secret key 123').toString(CryptoJS.enc.Utf8);
      setTimeout(() => {
        this.getQuestionsBySectionOrSubSection = JSON.parse(this.que);
        // console.log(this.getQuestionsBySectionOrSubSection)
        this.topLevelQuestions = JSON.parse(topLevelQuestions);

        this.answers = JSON.parse(answers);
        // console.log(`this.answers - `, this.answers)
        this.managerialLevelQuestions = JSON.parse(managerialLevelQuestions);
        this.executiveLevelQuestions = JSON.parse(executiveLevelQuestions);
        this.ngOnInit();
        if (this.seqNo === this.getQuestionsBySectionOrSubSection.length - 1) {
          document.getElementById('next').style.visibility = "hidden";
          document.getElementById('submit').style.float = "right";
        }
      }, 1000);
    });
    setTimeout(() => {
      let vgAudits = JSON.parse(this.vgAuditDetailsFromLocalStorage);
      this.vgaAuditNumber = vgAudits.audits.vga_audit_number;    //updated by nikesh Thakkar
      this.getEmployeeOfManager();
      this.auditData = vgAudits.auditsDetails;
      let questions =[];
      let section = JSON.parse(this.sectionDetails);
      let subSec = section.subSection;
      subSec.map((res: any)=>{
        if(res.id === this.sectionId){
          let que = res.questions;
          que.map((res: any)=>{
            if(res.sub_section_id === this.sectionId){
              questions.push(res);
            }
            if(res.question === this.questions){
              this.byDefaultValueofSubSection = res.by_default_value;
            }
          })
        }
      })
      questions.map((res: any) => {
        if(res.question === this.questions){
          this.byDefaultValueofSubSection = res.by_default_value;
        }
      });

      this.childRadio?.getRadioValue();
      section.questions?.map((res: any) => {
        if(res.id === this.questionId){
          this.byDefaultValueofquestion = res.by_default_value;
        }
      })
    }, 2000);
  }

  ngOnInit(): void {
    this.createform();
    this.getVgAuditDetails();
    this.getEmployeeListing(this.questionId, this.vgAudit.vga_audit_number, this.employeePerformance);
    this.getQuestionDetailsById();
    this.populateDataOnInit();
    this.getAllClients();
    this.addEmployeeForm = this.fb.group({
      employeeName: ['', Validators.required]
    });
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  populateDataOnInit() {
    setTimeout(() => {
      let response;
      let consultant: any = document.getElementById('consultant');
      this.reportAnswerValues.map((res: any)=> {
        document.querySelectorAll('#radioBtn')?.forEach((obj: any) => {
          obj.name === res.title && obj.value === res.answer_value ? obj.checked = true : null;
        })
        response = res;
      } );
      if((this.reportAnswerValues.length > 0 && this.roleType === 'Consultant') || this.roleType === 'Consultant'){
        this.vgaReportForm.get('AnswerDetails').disable();
        document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
        this.vgaReportForm.get('remarks').disable();
        this.vgaReportForm.get('remarks').patchValue(response.remarks);
      } else {
        const reportAnswerForSubsection = this.vgaReportForm.get('AnswerDetails')['controls'].map((res: any) => {
          if(res.get('AnswerValue').value){
            return null;
          } else{
            res.get('AnswerValue').patchValue(this.byDefaultValueofSubSection);
            if(consultant)
              consultant.style.visibility = "hidden";
          }
          res.touched = true;
          return res;
        });
        const reportAnswerForQuestion = this.vgaReportForm.get('AnswerDetails')['controls'].map((res: any) => {
          if(res.get('AnswerValue').value){
            return null;
          } else{
            res.get('AnswerValue').patchValue(this.byDefaultValueofquestion);
            if(consultant)
              consultant.style.visibility = "hidden";
          }
          res.touched = true;
          return res;
        });
        this.vgaReportForm.get('AnswerDetails').patchValue(reportAnswerForSubsection);
        this.vgaReportForm.get('AnswerDetails').patchValue(reportAnswerForQuestion);
        // this.vgaReportForm.get('remarks').enable();
        // this.vgaReportForm.get('remarks').patchValue(response.remarks);
        // if(response.remarks){
        //   this.vgaReportForm.get('remarks').disable();
        // }
      }
    }, 2000);
  }

  getQuestionDetailsById() {
    this.getQuestionsBySectionOrSubSection.map((res: any) => {
      console.log('res ---- ', res);
      if (this.questionId === res.id) {
        this.seqNumber = res.sequence_number;
        this.questions = res.question;
        this.answerType = res.answer_type;
        this.answerValueType = res.answer_value;
        this.answerValue = res.subquestions;
        this.submittedAnserValues = res.submittedAnswerValues;
        this.reportAnswerValues = res.reportanswers;
        this.questionType = res.question_type;
        this.subQuestions.push(res.subquestions);
        this.subQuestionsForCompetencyMapping = this.subQuestions[0];
        this.subQuestions[0].forEach((subQue: any) => {
          this.emp_que_id =  subQue.employee_performance_questions_id;
          this.subSubQuestions = subQue.subSubQuestions;
        });
        setTimeout(() => {
          let consultant = document.getElementById('consultant')
          if(this.reportAnswerValues.length === 0 && this.roleType === 'Client' && consultant){
            consultant.style.visibility = "hidden";
          }
        },2000)
        this.reportAnswerValues.map((res: any)=> {
          console.log(res)
          let remarks = res.remarks;
          let consultantReview = res.consultant_review;
          this.status = res.question_status;
          setTimeout(() => {
            this.vgaReportForm.get('remarks').patchValue(remarks);
            if(consultantReview && this.roleType !== 'Consultant'){
              this.vgaReportForm.get('AnswerDetails').disable();
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
              this.vgaReportForm.get('status').patchValue(this.status);
              this.vgaReportForm.get('consultantReview').disable();
              this.vgaReportForm.get('status').disable();
            } else {
              this.vgaReportForm.get('AnswerDetails').disable();
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('consultantReview').enable();
              this.vgaReportForm.get('status').patchValue(this.status);
              this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
              this.vgaReportForm.get('status').enable();
            }
            let consultant = document.getElementById('consultant')
            console.log(consultant)
            if((this.reportAnswerValues.length === 0 && this.roleType !== 'Consultant') || (consultantReview === null && this.roleType !== 'Consultant')){
              if(consultant)
                consultant.style.visibility = "hidden";
            } else {
              if(consultant)
                consultant.style.visibility = "visible";
            }
          },2000);
          if(this.seqNo === this.getQuestionsBySectionOrSubSection.length){
            setTimeout(() => {
              document.getElementById('next').style.visibility = "hidden";
              document.getElementById('submit').style.float = "right";
            }, 950);
          }
        });
        this.title = [];
        this.subQuestionIdArr = [];
        this.subQuestionsArr = [];
        for (let i = 0; this.answerValue.length; i++) {
          this.queId = this.answerValue[i].question_id;
          if (this.queId == this.questionId) {
            this.titleId = this.answerValue[i].id;
            this.titleOfQuestion = this.answerValue[i].title;
          }
          this.subQuestionIdArr.push(this.titleId);
          this.title.push(this.titleOfQuestion)

          let obj = {
            "subQuestionId": this.subQuestionIdArr[i],
            "title": this.title[i]
          }
          this.subQuestionsArr.push(obj);
          this.arr = [];
          for (let i = 0; i < this.subQuestionsArr.length; i++) {
            this.arr.push(this.BuildFormDynamic(this.subQuestionsArr[i]))
          }
          this.createform();
        }
      }
    });

    this.topLevelQuestions?.map((res: any) => {
      console.log('res --- ', res)
      if (this.questionId === res.id) {
        let ansObj = {
          "questionId": this.questionId,
          "vgAuditNo": this.vgAudit.vga_audit_number,
          "clientEmployeeId": this.clientEmpId
        }
        this.questionService.getReportAnswers(ansObj).subscribe((res: any) => {
          var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
          localStorage.setItem('answers', encryptAnswers);
          if(res.res.length > 0){
            this.remarks = res.res[0].remarks;
            setTimeout(() => {
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('remarks').patchValue(this.remarks);
            }, 500)
          }
        })
        this.seqNumber = res.sequence_number;
        this.questions = res.question;
        this.answerType = res.answer_type;
        this.answerValueType = res.answer_value;
        this.answerValue = res.subquestions;
        this.submittedAnserValues = res.submittedAnswerValues;
        this.reportAnswerValues = res.reportanswers;
        this.questionType = res.question_type;
        this.subQuestions.push(res.subquestions);
        this.subQuestionsForCompetencyMapping = this.subQuestions[0];
        this.subQuestions[0].forEach((subQue: any) => {
          this.emp_que_id =  subQue.employee_performance_questions_id;
        });
        setTimeout(() => {
          let consultant = document.getElementById('consultant')
          if(this.reportAnswerValues.length === 0 && this.roleType === 'Client' && consultant){
            consultant.style.visibility = "hidden";
          }
        },2000)
        this.reportAnswerValues.map((res: any)=> {
          console.log(res)
          let remarks = res.remarks;
          let consultantReview = res.consultant_review;
          this.status = res.question_status;
          setTimeout(() => {
            this.vgaReportForm.get('remarks').patchValue(remarks);
            if(consultantReview && this.roleType !== 'Consultant'){
              this.vgaReportForm.get('AnswerDetails').disable();
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
              this.vgaReportForm.get('status').patchValue(this.status);
              this.vgaReportForm.get('consultantReview').disable();
              this.vgaReportForm.get('status').disable();
            } else {
              this.vgaReportForm.get('AnswerDetails').disable();
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('consultantReview').enable();
              this.vgaReportForm.get('status').patchValue(this.status);
              this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
              this.vgaReportForm.get('status').enable();
            }
            let consultant = document.getElementById('consultant')
            console.log(consultant);
            console.log(`this.roleType - `, this.roleType)
            if((this.reportAnswerValues.length === 0 && this.roleType !== 'Consultant') || (consultantReview === null && this.roleType !== 'Consultant')){
              if(consultant)
                consultant.style.visibility = "hidden";
            } else {
              if(consultant)
                consultant.style.visibility = "visible";
            }
          },2000);
          if(this.seqNo === this.getQuestionsBySectionOrSubSection.length){
            setTimeout(() => {
              document.getElementById('next').style.visibility = "hidden";
              document.getElementById('submit').style.float = "right";
            }, 950);
          }
        });
        this.title = [];
        this.subQuestionIdArr = [];
        this.subQuestionsArr = [];
        for (let i = 0; this.answerValue.length; i++) {
          this.queId = this.answerValue[i].question_id;
          if (this.queId == this.questionId) {
            this.titleId = this.answerValue[i].id;
            this.titleOfQuestion = this.answerValue[i].title;
          }
          this.subQuestionIdArr.push(this.titleId);
          this.title.push(this.titleOfQuestion)

          let obj = {
            "subQuestionId": this.subQuestionIdArr[i],
            "title": this.title[i]
          }
          this.subQuestionsArr.push(obj);
          this.arr = [];
          for (let i = 0; i < this.subQuestionsArr.length; i++) {
            this.arr.push(this.BuildFormDynamic(this.subQuestionsArr[i]))
          }
          this.createform();
        }
      }
    });

    this.managerialLevelQuestions?.map((res: any) => {
      console.log(res)
      if (this.questionId === res.id) {
        let ansObj = {
          "questionId": this.questionId,
          "vgAuditNo": this.vgAudit.vga_audit_number,
          "clientEmployeeId": this.clientEmpId
        }
        this.questionService.getReportAnswers(ansObj).subscribe((res: any) => {
          var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
          localStorage.setItem('answers', encryptAnswers);
          if(res.res.length > 0){
            this.remarks = res.res[0].remarks;
            setTimeout(() => {
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('remarks').patchValue(this.remarks);
            }, 500)
          }
        })
        this.seqNumber = res.sequence_number;
        this.questions = res.question;
        this.answerType = res.answer_type;
        this.answerValueType = res.answer_value;
        this.answerValue = res.subquestions;
        this.submittedAnserValues = res.submittedAnswerValues;
        this.reportAnswerValues = res.reportanswers;
        this.questionType = res.question_type;
        this.subQuestions.push(res.subquestions);
        this.subQuestionsForCompetencyMapping = this.subQuestions[0];
        this.subQuestions[0].forEach((subQue: any) => {
          this.emp_que_id =  subQue.employee_performance_questions_id;
        });
        setTimeout(() => {
          let consultant = document.getElementById('consultant')
          console.log(consultant);
          if(this.reportAnswerValues.length === 0 && this.roleType === 'Client' && consultant){
            consultant.style.visibility = "hidden";
          }
        },2000)
        this.reportAnswerValues.map((res: any)=> {
          console.log(res)
          let remarks = res.remarks;
          let consultantReview = res.consultant_review;
          this.status = res.question_status;
          setTimeout(() => {
            this.vgaReportForm.get('remarks').patchValue(remarks);
            if(consultantReview && this.roleType !== 'Consultant'){
              this.vgaReportForm.get('AnswerDetails').disable();
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
              this.vgaReportForm.get('status').patchValue(this.status);
              this.vgaReportForm.get('consultantReview').disable();
              this.vgaReportForm.get('status').disable();
            } else {
              this.vgaReportForm.get('AnswerDetails').disable();
              document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('consultantReview').enable();
              this.vgaReportForm.get('status').patchValue(this.status);
              this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
              this.vgaReportForm.get('status').enable();
            }
            let consultant = document.getElementById('consultant')
            console.log(consultant);
            console.log(`this.roleType - `, this.roleType)
            if((this.reportAnswerValues.length === 0 && this.roleType !== 'Consultant') || (consultantReview === null && this.roleType !== 'Consultant')){
              if(consultant)
                consultant.style.visibility = "hidden";
            } else {
              if(consultant)
                consultant.style.visibility = "visible";
            }
          },2000);
          if(this.seqNo === this.getQuestionsBySectionOrSubSection.length){
            setTimeout(() => {
              document.getElementById('next').style.visibility = "hidden";
              document.getElementById('submit').style.float = "right";
            }, 950);
          }
        });
        this.title = [];
        this.subQuestionIdArr = [];
        this.subQuestionsArr = [];
        for (let i = 0; this.answerValue.length; i++) {
          this.queId = this.answerValue[i].question_id;
          if (this.queId == this.questionId) {
            this.titleId = this.answerValue[i].id;
            this.titleOfQuestion = this.answerValue[i].title;
          }
          this.subQuestionIdArr.push(this.titleId);
          this.title.push(this.titleOfQuestion)

          let obj = {
            "subQuestionId": this.subQuestionIdArr[i],
            "title": this.title[i]
          }
          this.subQuestionsArr.push(obj);
          this.arr = [];
          for (let i = 0; i < this.subQuestionsArr.length; i++) {
            this.arr.push(this.BuildFormDynamic(this.subQuestionsArr[i]))
          }
          this.createform();
        }
      }
    });

    this.executiveLevelQuestions?.map((res: any) => {
      console.log(res)
      if (this.questionId === res.id) {
        let ansObj = {
          "questionId": this.questionId,
          "vgAuditNo": this.vgAudit.vga_audit_number,
          "clientEmployeeId": this.clientEmpId
        }
        this.questionService.getReportAnswers(ansObj).subscribe((res: any) => {
          var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
          localStorage.setItem('answers', encryptAnswers);
          if(res.res.length > 0){
            this.remarks = res.res[0].remarks;
            setTimeout(() => {
              this.vgaReportForm.get('remarks').disable();
              this.vgaReportForm.get('remarks').patchValue(this.remarks);
            }, 500)
          }
        })
        this.seqNumber = res.sequence_number;
        this.questions = res.question;
        this.answerType = res.answer_type;
        this.answerValueType = res.answer_value;
        this.answerValue = res.subquestions;
        this.submittedAnserValues = res.submittedAnswerValues;
        this.reportAnswerValues = res.reportanswers;
        this.questionType = res.question_type;
        this.subQuestions.push(res.subquestions);
        this.subQuestionsForCompetencyMapping = this.subQuestions[0];
        this.subQuestions[0].forEach((subQue: any) => {
          this.emp_que_id =  subQue.employee_performance_questions_id;
        });
        setTimeout(() => {
          let consultant = document.getElementById('consultant')
          console.log(consultant);
          if(this.reportAnswerValues.length === 0 && this.roleType === 'Client' && consultant){
            consultant.style.visibility = "hidden";
          }
        },2000)
        if(this.reportAnswerValues.length !== 0){
          this.reportAnswerValues.map((res: any)=> {
            let remarks = res.remarks;
            let consultantReview = res.consultant_review;
            this.status = res.question_status;
            setTimeout(() => {
              this.vgaReportForm.get('remarks').patchValue(remarks);
              if(consultantReview && this.roleType !== 'Consultant'){
                this.vgaReportForm.get('AnswerDetails').disable();
                document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
                this.vgaReportForm.get('remarks').disable();
                this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
                this.vgaReportForm.get('status').patchValue(this.status);
                this.vgaReportForm.get('consultantReview').disable();
                this.vgaReportForm.get('status').disable();
              } else {
                this.vgaReportForm.get('AnswerDetails').disable();
                document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
                this.vgaReportForm.get('remarks').disable();
                this.vgaReportForm.get('consultantReview').enable();
                this.vgaReportForm.get('status').patchValue(this.status);
                this.vgaReportForm.get('consultantReview').patchValue(consultantReview);
                this.vgaReportForm.get('status').enable();
              }
              let consultant = document.getElementById('consultant')
              console.log(consultant);
              console.log(`this.roleType - `, this.roleType)
              if((this.reportAnswerValues.length === 0 && this.roleType !== 'Consultant') || (consultantReview === null && this.roleType !== 'Consultant')){
                if(consultant)
                  consultant.style.visibility = "hidden";
              } else {
                if(consultant)
                  consultant.style.visibility = "visible";
              }
            },2000);
            if(this.seqNo === this.getQuestionsBySectionOrSubSection.length){
              setTimeout(() => {
                document.getElementById('next').style.visibility = "hidden";
                document.getElementById('submit').style.float = "right";
              }, 950);
            }
          });
        }
        this.title = [];
        this.subQuestionIdArr = [];
        this.subQuestionsArr = [];
        for (let i = 0; this.answerValue.length; i++) {
          this.queId = this.answerValue[i].question_id;
          if (this.queId == this.questionId) {
            this.titleId = this.answerValue[i].id;
            this.titleOfQuestion = this.answerValue[i].title;
          }
          this.subQuestionIdArr.push(this.titleId);
          this.title.push(this.titleOfQuestion)

          let obj = {
            "subQuestionId": this.subQuestionIdArr[i],
            "title": this.title[i]
          }
          this.subQuestionsArr.push(obj);
          this.arr = [];
          for (let i = 0; i < this.subQuestionsArr.length; i++) {
            this.arr.push(this.BuildFormDynamic(this.subQuestionsArr[i]))
          }
          this.createform();
        }
      }
    });

  }

  createform() {
    this.vgaReportForm = this.fb.group({
      UserId: [this.userObj.id],
      QuestionId: [this.questionId],
      clientEmployeeId: [this.clientEmpId ? this.clientEmpId : null],
      AnswerDetails: this.fb.array(this.arr),
      remarks: [''],
      consultantReview: [''],
      vgaAuditNumber: [this.vgAudit.vga_audit_number],
      status: ['Reviewed', []]
    })
  }

  BuildFormDynamic(title): FormGroup {
    return this.fb.group({
      SubQuestionId: [title.subQuestionId],
      Title: [title.title],
      AnswerValue: ['', Validators.required],
      AnswerValue1: [''],
      AnswerValue2: ['']
    })
  }

  onClickOfPrevious() {
    let submitButton = document.getElementById('submit');
    let nextButton = document.getElementById('next');
    console.log(`this.questionId --- `, this.questionId)
    this.location.back();
    this.populateDataOnInit();

    this.childDropDown?.getDropDownValue();
    this.childRadio?.getRadioValue();
    setTimeout(() => {
      if(submitButton){
        submitButton.style.float = "left";
        submitButton.style.marginTop = '5%';
      }
      if(nextButton)
        nextButton.style.visibility = "visible";
    }, 950);
    let ansObj = {
      "questionId": this.questionId,
      "vgAuditNo": this.vgAudit.vga_audit_number,
      "clientEmployeeId": this.clientEmpId
    }
    this.questionService.getReportAnswers(ansObj).subscribe((res: any) => {
      console.log(res)
      var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
      localStorage.setItem('answers', encryptAnswers);
      if(res.res.length > 0){
        this.remarks = res.res[0].remarks;
        console.log(`this.remarks --- `, this.remarks)
        // setTimeout(() => {
          this.vgaReportForm.get('remarks').disable();
          this.vgaReportForm.get('remarks').patchValue(this.remarks);
        // }, 1000)
      }
    })
  }

  onClickOfNextQuestion() {
    this.vgaReportForm.get('remarks').enable();
    this.seqNo += 1;
    if (this.seqNo <= this.getQuestionsBySectionOrSubSection.length && this.questionType === 6) {
      if(this.seqNo === this.getQuestionsBySectionOrSubSection.length){
        setTimeout(() => {
          document.getElementById('next').style.visibility = "hidden";
          document.getElementById('submit').style.float = "right";
        }, 950);
      }
      for (let i = 1; i<this.getQuestionsBySectionOrSubSection.length; i++) {
        if (this.seqNo == this.getQuestionsBySectionOrSubSection[i].sequence_number) {
          this.questions = this.getQuestionsBySectionOrSubSection[i].question;
          this.answerType = this.getQuestionsBySectionOrSubSection[i].answer_type;
          this.answerValueType = this.getQuestionsBySectionOrSubSection[i].answer_value;
          this.answerValue = this.getQuestionsBySectionOrSubSection[i].subquestions;
          this.byDefaultValueofquestion = this.getQuestionsBySectionOrSubSection[i].by_default_value;
          this.reportAnswerValues = this.getQuestionsBySectionOrSubSection[i].reportanswers;
          // if(this.seqNo === 2){
          //   this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.getQuestionsBySectionOrSubSection[i].id, this.getQuestionsBySectionOrSubSection[i].sequence_number, this.clientEmpId]).then(() => {
          //     this.populateDataOnInit();
          //     this.childDropDown?.getDropDownValue();
          //     this.childRadio?.getRadioValue();
          //   })
          // } else{
            this.router.navigate(['/pages/VGAudit/vgaudits/subSections', this.getQuestionsBySectionOrSubSection[i].id, this.getQuestionsBySectionOrSubSection[i].sequence_number]).then(() => {
              this.populateDataOnInit();
              this.childDropDown?.getDropDownValue();
              this.childRadio?.getRadioValue();
            })
          // }
        }
      }
    } else if (this.seqNo <= this.topLevelQuestions.length && this.questionType === 6) {
      if(this.seqNo === this.topLevelQuestions.length){
        setTimeout(() => {
          document.getElementById('next').style.visibility = "hidden";
          document.getElementById('submit').style.float = "right";
        }, 950);
      }
      for (let i = 1; i<this.topLevelQuestions.length; i++) {
        if (this.seqNo == this.topLevelQuestions[i].sequence_number) {
          this.questions = this.topLevelQuestions[i].question;
          this.answerType = this.topLevelQuestions[i].answer_type;
          this.answerValueType = this.topLevelQuestions[i].answer_value;
          this.answerValue = this.topLevelQuestions[i].subquestions;
          this.byDefaultValueofquestion = this.topLevelQuestions[i].by_default_value;
          this.reportAnswerValues = this.topLevelQuestions[i].reportanswers;
          let sectionDetails = JSON.parse(this.sectionDetails)
          let subSections = sectionDetails.subSection;

          subSections.filter((subSec: any) => {
            if(subSec.id === this.secId && this.clientEmpId == undefined){
              this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.topLevelQuestions[i].id, this.topLevelQuestions[i].sequence_number]).then(() => {
                this.populateDataOnInit();
                this.childDropDown?.getDropDownValue();
                this.childRadio?.getRadioValue();
              })
            } else if(this.clientEmpId !== undefined){
              this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.topLevelQuestions[i].id, this.topLevelQuestions[i].sequence_number, this.clientEmpId]).then(() => {
                this.populateDataOnInit();
                this.childDropDown?.getDropDownValue();
                this.childRadio?.getRadioValue();
              })
            } else{
              this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.topLevelQuestions[i].id, this.topLevelQuestions[i].sequence_number]).then(() => {
                this.populateDataOnInit();
                this.childDropDown?.getDropDownValue();
                this.childRadio?.getRadioValue();
              })
            }
          })

          let ansObj = {
            "questionId": this.topLevelQuestions[i].id,
            "vgAuditNo": this.vgAudit.vga_audit_number,
            "clientEmployeeId": this.clientEmpId
          }
          this.questionService.getReportAnswers(ansObj).subscribe((res: any) => {
            console.log(res)
            var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
            localStorage.setItem('answers', encryptAnswers);
            if(res.res.length > 0){
              this.remarks = res.res[0].remarks;
              // setTimeout(() => {
                console.log(`this.remarks --- `, this.remarks)
                this.vgaReportForm.get('remarks').disable();
                // this.vgaReportForm.get('remarks').patchValue(this.remarks);
              // }, 500)
            }
          })
        }
      }
    } else if (this.seqNo <= this.managerialLevelQuestions.length && this.questionType === 7) {
      if(this.seqNo === this.managerialLevelQuestions.length){
        setTimeout(() => {
          document.getElementById('next').style.visibility = "hidden";
          document.getElementById('submit').style.float = "right";
        }, 950);
      }
      for (let i = 1; i<this.managerialLevelQuestions.length; i++) {
        if (this.seqNo == this.managerialLevelQuestions[i].sequence_number) {
          this.questions = this.managerialLevelQuestions[i].question;
          this.answerType = this.managerialLevelQuestions[i].answer_type;
          this.answerValueType = this.managerialLevelQuestions[i].answer_value;
          this.answerValue = this.managerialLevelQuestions[i].subquestions;
          this.byDefaultValueofquestion = this.managerialLevelQuestions[i].by_default_value;
          this.reportAnswerValues = this.managerialLevelQuestions[i].reportanswers;
          if(this.clientEmpId !== undefined){
            this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.managerialLevelQuestions[i].id, this.managerialLevelQuestions[i].sequence_number, this.clientEmpId]).then(() => {
              this.populateDataOnInit();
              this.childDropDown?.getDropDownValue();
              this.childRadio?.getRadioValue();
            })
          } else{
            this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.managerialLevelQuestions[i].id, this.managerialLevelQuestions[i].sequence_number]).then(() => {
              this.populateDataOnInit();
              this.childDropDown?.getDropDownValue();
              this.childRadio?.getRadioValue();
            })
          }
          let ansObj = {
            "questionId": this.managerialLevelQuestions[i].id,
            "vgAuditNo": this.vgAudit.vga_audit_number,
            "clientEmployeeId": this.clientEmpId
          }
          this.questionService.getReportAnswers(ansObj).subscribe((res: any) => {
            console.log(res)
            var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
            localStorage.setItem('answers', encryptAnswers);
            if(res.res.length > 0){
              this.remarks = res.res[0].remarks;
              // setTimeout(() => {
                console.log(`this.remarks --- `, this.remarks)
                this.vgaReportForm.get('remarks').disable();
                // this.vgaReportForm.get('remarks').patchValue(this.remarks);
              // }, 500)
            }
          })
        }

      }

    } else if (this.seqNo <= this.executiveLevelQuestions.length && this.questionType === 8) {
      if(this.seqNo === this.executiveLevelQuestions.length){
        setTimeout(() => {
          document.getElementById('next').style.visibility = "hidden";
          document.getElementById('submit').style.float = "right";
        }, 950);
      }
      for (let i = 1; i<this.executiveLevelQuestions.length; i++) {
        if (this.seqNo == this.executiveLevelQuestions[i].sequence_number) {
          this.questions = this.executiveLevelQuestions[i].question;
          this.answerType = this.executiveLevelQuestions[i].answer_type;
          this.answerValueType = this.executiveLevelQuestions[i].answer_value;
          this.answerValue = this.executiveLevelQuestions[i].subquestions;
          this.byDefaultValueofquestion = this.executiveLevelQuestions[i].by_default_value;
          this.reportAnswerValues = this.executiveLevelQuestions[i].reportanswers;
          if(this.clientEmpId !== undefined){
            this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.executiveLevelQuestions[i].id, this.executiveLevelQuestions[i].sequence_number, this.clientEmpId]).then(() => {
              this.populateDataOnInit();
              this.childDropDown?.getDropDownValue();
              this.childRadio?.getRadioValue();
            })
          } else{
            this.router.navigate(['/pages/VGAudit/vgaudits', this.secId, this.executiveLevelQuestions[i].id, this.executiveLevelQuestions[i].sequence_number]).then(() => {
              this.populateDataOnInit();
              this.childDropDown?.getDropDownValue();
              this.childRadio?.getRadioValue();
            })
          }
          let ansObj = {
            "questionId": this.executiveLevelQuestions[i].id,
            "vgAuditNo": this.vgAudit.vga_audit_number,
            "clientEmployeeId": this.clientEmpId
          }
          this.questionService.getReportAnswers(ansObj).subscribe((res: any) => {
            console.log(res)
            var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
            localStorage.setItem('answers', encryptAnswers);
          })
        }
      }
    } else if(this.seqNo <= this.getQuestionsBySectionOrSubSection.length){
      if(this.seqNo === this.getQuestionsBySectionOrSubSection.length){
        setTimeout(() => {
          document.getElementById('next').style.visibility = "hidden";
          document.getElementById('submit').style.float = "right";
        }, 950);
      }
      for (let i = 1; i<this.getQuestionsBySectionOrSubSection.length; i++) {
        if (this.seqNo == this.getQuestionsBySectionOrSubSection[i].sequence_number) {
          this.questions = this.getQuestionsBySectionOrSubSection[i].question;
          this.answerType = this.getQuestionsBySectionOrSubSection[i].answer_type;
          this.answerValueType = this.getQuestionsBySectionOrSubSection[i].answer_value;
          this.answerValue = this.getQuestionsBySectionOrSubSection[i].subquestions;
          this.byDefaultValueofquestion = this.getQuestionsBySectionOrSubSection[i].by_default_value;
          this.reportAnswerValues = this.getQuestionsBySectionOrSubSection[i].reportanswers;
          this.router.navigate(['/pages/VGAudit/vgaudits/subSections', this.getQuestionsBySectionOrSubSection[i].id, this.getQuestionsBySectionOrSubSection[i].sequence_number]).then(() => {
            this.populateDataOnInit();
            this.childDropDown?.getDropDownValue();
            this.childRadio?.getRadioValue();
          })
        }
      }
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    console.log(this.vgaReportForm.value);
    this.vgaReportForm.markAllAsTouched();
    let vgAuditNo = this.vgAudit.vga_audit_number;
    let vgAuditObj = this.vgAudit;
    if (this.roleType == 'Consultant' && this.vgaReportForm.valid) {
      if (this.vgaReportForm.valid) {
        setTimeout(() => {
          this.vgaReportForm.get('AnswerDetails').disable();
          this.vgaReportForm.get('remarks').disable();
        }, 1500);
        let consultantReviewForm = {
          'UserId': this.vgaReportForm.value.UserId,
          'QuestionId': this.vgaReportForm.value.QuestionId,
          'consultantReview': this.vgaReportForm.value.consultantReview || null,
          'status': this.vgaReportForm.value.status,
          'vgaAuditNumber': this.vgaReportForm.get('vgaAuditNumber').value
        }
        this.reportAnswerService.reportAnswerValue({ "Answer": consultantReviewForm }).subscribe((res: any) => {
          if (res) {
            this.vgAuditService.updateVgAudits(vgAuditNo,vgAuditObj).subscribe((res: any) => {
              if(res.success == true){
                this.toastr.success('Consultant review submitted!');
                var encryptVgAuditDetails = CryptoJS.AES.encrypt(JSON.stringify(res), 'secret key 123').toString();
                localStorage.setItem('vgAuditDetails',encryptVgAuditDetails);
              } else{
                this.toastr.error('Something went wrong');
              }
            })
          }
        },
          (error: HttpErrorResponse) => {
            if (error) {
              this.toastr.error('Answers are not submitted yet!')
            }
          }
        )
     }else {
      this.validateAllFormFields(this.vgaReportForm);
    }
   } else {
      if (this.vgaReportForm.valid) {
        let reportAnswerForm = {
          'UserId': this.vgaReportForm.get('UserId').value,
          'QuestionId': this.vgaReportForm.get('QuestionId').value,
          'AnswerDetails': this.vgaReportForm.get('AnswerDetails').value,
          'remarks': this.vgaReportForm.get('remarks').value,
          'vgaAuditNumber': this.vgaReportForm.get('vgaAuditNumber').value,
          'clientEmployeeId': this.vgaReportForm.get('clientEmployeeId').value,
        }
        console.log(`reportAnswerForm --- `, reportAnswerForm)
        this.reportAnswerService.reportAnswerValue({ "Answer": reportAnswerForm }).subscribe((response: any) => {
          if (response) {
            this.vgaReportForm.get('AnswerDetails').disable();
            document.querySelectorAll('#radioBtn')?.forEach((obj: any) => obj.disabled = true);
            this.vgaReportForm.get('remarks').disable();
            this.vgAuditService.updateVgAudits(vgAuditNo,vgAuditObj).subscribe((res: any) => {
              if(res.success == true){
                var encryptVgAuditDetails = CryptoJS.AES.encrypt(JSON.stringify(res), 'secret key 123').toString();
                localStorage.setItem('vgAuditDetails',encryptVgAuditDetails);
                let vgAuditNo = {
                  'vgAuditNo' : this.vgAudit.vga_audit_number,
                  'clientEmployeeId': this.clientEmpId
                }
                this.questionService.postVgAuditReportObj(this._secId.id,vgAuditNo).subscribe((res: any) =>{
                  if(res.success === true){
                    this.toastr.success('Answer saved successfully!')
                    let sectionDetails = res.res.sections[0];
                    var encryptVgAuditDetails = CryptoJS.AES.encrypt(JSON.stringify(sectionDetails), 'secret key 123').toString();
                    localStorage.setItem('sectionDetails',encryptVgAuditDetails);
                  }
                })
              } else{
                this.toastr.error(res.message);
              }
            });
          }
        },
          (error: HttpErrorResponse) => {
            if (error) {
              if (error.error.message === "please, submit the answers first!") {
                this.toastr.error('Please select atleast one value!')
              } else if(error.error.message === "Answers of this Questions are already Submitted."){
                this.toastr.error('Response is already Submitted!');
              } else if(error.error.errors[0].msg === "answer value required."){
                this.toastr.error('Please select all values');
              }
            }
          }
        )
      // }else {
      //   this.validateAllFormFields(this.vgaReportForm);
      // }
    }
  }
  }

  onClearLocalStorage() {
    localStorage.clear();
  }

  getVgAuditDetails() {
    this.vgAuditDetails = JSON.parse(this.vgAuditDetailsFromLocalStorage);
    this.vgAudit = (this.vgAuditDetails.audits) || (this.vgAuditDetails.res.audits);
    this.auditDetails = (this.vgAuditDetails.auditsDetails) || (this.vgAuditDetails.res.auditsDetails);
  }

  onClickOfRadio(target){
    let answerDetail = this.vgaReportForm.get('AnswerDetails').value;
    let title;
    const ansDetails = answerDetail.map((obj: any) => {
      title = obj.Title;
      if(title == target.name){
        obj.AnswerValue = target.value;
        return obj;
      } else {
        return obj;
      }
    });
    console.log(ansDetails)
    this.vgaReportForm.get('AnswerDetails').patchValue(ansDetails);
  }

  getAllClients(){
    this.clientService.allClients().subscribe((res: any) => {
      let clients = res.res;
      clients.map((client: any)=>{
        if(client.id === this.vgAudit.client_id){
          this.clientName = client.name;
        }
      })
    })
  }

  addNewEmployee(content){
    this.modalService.open(content, { centered: false });
  }

  onSelect(event){
    this.selectedEmployeeId = event;
  }

  onSubmitOfEmployee(){
    this.isEditable = true;
    if(this.addEmployeeForm.valid){
      let obj = {
        'userId': this.vgaReportForm.value.UserId,
        'employeeId': this.selectedEmployeeId,
        'questionId': this.questionsId,
        'vgAuditNo': this.vgAudit.vga_audit_number,
        'sectionId' : this.sectionId,
        'employeePerformanceQuestionId': null
      }
      this.employeePerformanceService.getAnswersOfEmployeePerformance(obj).subscribe(() => {});
      this.modalService.dismissAll();
      this.clientEmployeName = this.addEmployeeForm.controls['employeeName'].value;
      this.clientEmpName.changeMessage(this.clientEmployeName);
      this.router.navigate([`/pages/VGAudit/vgaudits/${this.secId}/${this.questionsId}/${this.sequenceNo}/employee/${this.clientEmployeName}`]);
    }
  }

  getEmpName(){
    this.subscription = this.clientEmployeeService.getClientEmployee().subscribe((res: any) => {
      let clientEmployees = res.res;
      this.employeePerformanceService.getAllEmployeePerformanceUsers(this.questionId, this.vgAudit.vga_audit_number).subscribe((res: any) => {
        let employeePerformanceUsers = res.res;
        employeePerformanceUsers.map((empUsers: any)=>{
          this.listedEmployeesId.push(empUsers.employee_id);
        });
      });
      let employees:any = res.res;
      employees.forEach(element => {
        setTimeout(() => {
          if(this.listedEmployeesId.includes(element.id)) null
          else
            this.employeName.push(element);
        }, 500);
      });
    });
  }

  getEmployeeListing(queId, vgAuditNo, employeeListObj){
    this.employeePerformanceService.getEmployeesListing(queId, vgAuditNo, employeeListObj).subscribe((res: any)=>{
      if(res){
        this.totalItems = res.res.total;
        this.listOfEmployees = res.res.results;
      } else {
        this.totalItems = 0;
        this.listOfEmployees = [];
      }
    });
  }

  onClickOfEditEmployee(id){
    this.employeePerformanceService.getEmployeesOfEmployeePerformanceById(id).subscribe((res: any) => {
      this.clientEmployeName = res.res.clientEmployee.id;
      this.clientEmpName.changeMessage(this.clientEmployeName);
      if(res){
        this.router.navigate([`/pages/VGAudit/vgaudits/${this.secId}/${this.questionsId}/${this.sequenceNo}/employee/${this.clientEmployeName}/${id}`]);
      }
    })
  }

  getEmployeeOfManager(){
    this.subscription = this.employeePerformanceService.getManagerEmployeePerformance(this.vgaAuditNumber).subscribe((res: any) => {
      let employees:any = res.res;
      employees.forEach(element => {
        setTimeout(() => {
          if(this.listedEmployeesId.includes(element.employee_id)) null
          else
            this.managerEmployeeName.push(element);
        }, 500);
      });
    });
  }

  deleteEmpQuestion(id){
    if(window.confirm('Are you sure want to delete this record?')){
      this.onDeleteEmpPerformanceQuestion(id);
    }
  }

  onDeleteEmpPerformanceQuestion(i) {
    this.subscription = this.employeePerformanceService.deleteEmployeePerformanceQuestion(i).subscribe(() => {});
    this.getEmployeeListing(this.questionId, this.vgAudit.vga_audit_number, this.employeePerformance);
  }

  loadPage(page: number) {
		if (page !== this.previousPage) {
			this.employeePerformance.PageNo = page - 1;
      this.previousPage = page;
      this.getEmployeeListing(this.questionId, this.vgAudit.vga_audit_number, this.employeePerformance);
		}
  }
}
