import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { clientEmployees } from 'src/app/shared/models/client-employee';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-employee-levels',
  templateUrl: './employee-levels.component.html',
  styleUrls: ['./employee-levels.component.scss']
})
export class EmployeeLevelsComponent implements OnInit {

  _clientEmp: clientEmployees;
  clientId: string;
  employeeLevel: any;
  employees: any = [];
  subSectionId: any;

  constructor(private clientEmployeeService: ClientEmployeeService, private activatedRoute: ActivatedRoute, private location: Location, private router: Router) {
    this._clientEmp = new clientEmployees();
    this.clientId = sessionStorage.getItem('clientId');
    this.employeeLevel = this.activatedRoute.snapshot.params.level;
    this.subSectionId = localStorage.getItem('subSectionId');
  }

  ngOnInit(): void {
    this.getEmployeesByClientIdAndEmployeeLevel();
  }

  getEmployeesByClientIdAndEmployeeLevel(){
    this.clientEmployeeService.getClientEmployeesByclientIdAndEmployeeLevel(this.clientId, this.employeeLevel, this._clientEmp).subscribe((res: any) => {
      this.employees = res.res.results;
    })
  }

  viewDetails(emp){
    let clientEmployeeName = emp.name;
    localStorage.setItem('clientEmployeeName', clientEmployeeName)
    this.router.navigate(['/pages/VGAudit/VGAuditReport/questions/', this.subSectionId, emp.id, this.employeeLevel])
  }

  backToSubSection(){
    this.location.back();
  }

}
