import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVgAuditComponent } from './new-vg-audit.component';

describe('NewVgAuditComponent', () => {
  let component: NewVgAuditComponent;
  let fixture: ComponentFixture<NewVgAuditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVgAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVgAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
