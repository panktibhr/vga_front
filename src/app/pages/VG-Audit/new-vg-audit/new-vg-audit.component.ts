import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { DataService } from 'src/app/shared/services/data.service';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { VgAuditService } from 'src/app/shared/services/vg-audit/vg-audit.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Clients } from 'src/app/shared/models/clients';
@Component({
  selector: 'app-new-vg-audit',
  templateUrl: './new-vg-audit.component.html',
  styleUrls: ['./new-vg-audit.component.scss'],
})
export class NewVgAuditComponent implements OnInit, OnDestroy {

  vgAuditReportForm: FormGroup;
  button: string;
  sections: any = [];
  sectionObj: any = [];
  subSecObj: any = [];
  questionObj: any = [];
  questionsId: any;
  que: any = [];
  question: any = [];
  getQuestionsBySubSection: any = [];
  getQuestionsBysection: any = [];
  sectionId: any;
  subSectionId: any;
  allSections: any = [];
  sec: any = [];
  clients: any = [];
  vgAuditNo: any;
  vgAuditById: any;
  header: string;
  subscription: Subscription;
  userObj: any;
  arr: any = [];
  vgAuditdetails: any;
  section_id: any = [];
  checkBox: any;
  sectionsOfClient: any = [];
  clientId: any;
  sectionValue: any = [];
  userName: any;
  roleType: string;
  _clients: Clients;

  constructor(private router: Router, private questionService: QuestionMasterService, private dataService: DataService, private sectionService: SectionMasterService,
    private clientService: ClientsService, private fb: FormBuilder, private vgAuditService: VgAuditService, private toastr: ToastrService, private actRoute: ActivatedRoute) {
      this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
      this.userName = this.userObj.name;
      this.roleType = this.userObj.roleName;
      this._clients = new Clients();
      this.vgAuditReportForm = this.fb.group({
        'userId': [this.userObj.id],
        'clientId': [this.clientId],
        'fromDate': ['', Validators.required],
        'toDate': ['', Validators.required],
        'sectionsList': this.fb.array([], this.router.url === '/pages/VGAudit/VGAuditReport' ? Validators.required : null )
      });
    this.vgAuditNo = this.actRoute.snapshot.params.vgAuditNo;
  }

  ngOnInit(): void {
    if (this.router.url === '/pages/VGAudit/VGAuditReport') {
      this.button = "Create"
      this.header = "New VG Audit Report";
      // this.getVGAuditorDetail();
      this.getClientDetails();
    } else if (this.router.url === window.location.pathname) {
      this.button = "Update"
      this.header = "Edit VG Audit Report"
      this.getVGAuditorDetail();
      this.getClientDetails();
    } else {
      this.button = "Update";
      this.header = "Edit VG Audit Report"
      this.getVGAuditorDetail();
      this.getClientDetails();
    }
    // this.getVGAuditorDetail();
    // this.getClientDetails();
    this.getAllClients();
    setTimeout(() => {
      this.getAllSections();
    },3500)
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  onChange(event){
    if(this.roleType === 'Super Admin'){
      this.sectionsOfClient = [];
      let clientId = event.slice(3);
      this.subscription = this.clientService.getClientsById(clientId).subscribe((res: any) => {
        res.res.section_details.filter((sections: any) => {
          this.sectionsOfClient.push(JSON.parse(sections).id);
        });
      let obj;
        setTimeout(() => {
          let sectionsList: FormArray = this.vgAuditReportForm.get('sectionsList') as FormArray;
          this.checkBox = document.querySelectorAll('#checkId');
          this.checkBox.checked = false;
          sectionsList.clear();
          for(let i=0; i<this.checkBox.length; i++){
            this.sectionsOfClient.filter((sectionId: any) => {
              if(this.checkBox[i].value === sectionId){
                this.checkBox[i].checked = true;
                obj = {
                  "sectionId" : this.checkBox[i].value
                }
                sectionsList.push(new FormControl(obj));
                this.sectionValue.push(this.checkBox[i].value);
              }
            });
            this.checkBox[i].disabled = true;
          }
        }, 3500);
      });
    }

    // const sectionsList: FormArray = this.vgAuditReportForm.get('sectionsList') as FormArray;
    // if (event.target.checked === true) {
    //   let obj = {
    //     "sectionId" : event.target.value
    //   }
    //   sectionsList.push(new FormControl(obj));
    // }else {
    //   let i: number = 0;
    //   sectionsList.controls.forEach((item: FormControl) => {
    //     if (item.value.sectionId === event.target.value) {
    //       sectionsList.removeAt(i);
    //       return;
    //     }
    //     i++;
    //   });
    // }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    console.log(this.vgAuditReportForm.value)
    this.vgAuditReportForm.markAllAsTouched();
    if(this.vgAuditReportForm.valid){
      if (this.router.url === '/pages/VGAudit/VGAuditReport') {
        // this.vgAuditReportForm.get('vgaAuditNo').patchValue(`VGARPT_${this.i}`);
        // this.vgAuditReportForm.get('sectionsList').patchValue(this.arr);

          this.subscription = this.vgAuditService.postVgAuditReport(this.vgAuditReportForm.value).subscribe((res: any) => {
            if(res){
              this.toastr.success('VG Audit Report created successfully!');
              this.vgAuditReportForm.reset();
              setTimeout(() => {
                this.router.navigate(['pages/VGAudit/SearchVGAudit']);
              }, 1500)
              // this.arr = [];
            }
          })
      }else{
        this.editVGAuditDetails();
      }
    }
  }

  getAllClients() {
    this.subscription = this.clientService.getAllClients(this._clients).subscribe((res: any) => {
      this.clients = res.res.results;
    })
  }

  getAllSections() {
      this.subscription = this.sectionService.getAllSec().subscribe((res: any) => {
        this.allSections = res.res;
        this.allSections.forEach((res: any) => {
          // if (res.isdefault == true) {
            this.sec.push(res);
          // }
        });
      });
    if(this.roleType !== 'Super Admin'){
      setTimeout(() => {
        const sectionsList: FormArray = this.vgAuditReportForm.get('sectionsList') as FormArray;
        this.checkBox = document.querySelectorAll('#checkId');
        this.checkBox.checked = false;
        for(let i=0; i<this.checkBox.length; i++){
          this.sectionsOfClient.filter((sectionId: any) => {
            if(this.checkBox[i].value === sectionId){
              this.checkBox[i].checked = true;
              let obj = {
                "sectionId" : this.checkBox[i].value
              }
              sectionsList.push(new FormControl(obj));
              this.sectionValue.push(this.checkBox[i].value);
            }
          });
          this.checkBox[i].disabled = true;
        }
      }, 3500);
    }
  }

  getSubSectionsOfSection(secId) {
    this.subscription = this.questionService.getSubSectionsBySection(secId).subscribe((res: any) => {
      if (res.res.length === 0) {
        this.subscription = this.questionService.getQuestionsBySections(secId).subscribe((res: any) => {
          this.getQuestionsBysection = res.res;
          this.dataService.changeMessage(this.getQuestionsBysection);
          return res;
        })
      }
      this.sectionId = secId;
      // this.router.navigate(['/pages/VGAudit/vgaudits', secId])
    })
  }

  getQuestionsOnclickOfSubSection(subSecId) {
    this.subscription = this.questionService.getQuestionsBySubSections(subSecId).subscribe((res: any) => {
      this.getQuestionsBySubSection = res.res;
      this.subSectionId = subSecId;
      this.dataService.changeMessage(this.getQuestionsBySubSection);
    })
  }

  onClickOfQuestion(id) {
    this.subscription = this.questionService.getQuestionById(id).subscribe((res: any) => {
      this.question = res.res.question;
      if (res) {
        this.router.navigate(['/pages/VGAudit/vgaudits', res.res.id, res.res.sequence_number])
      }
    })
  }

  onClickOfSectionQuestion(id, sequence_number) {
    this.router.navigate(['/pages/VGAudit/vgaudits', id, sequence_number]);
  }

  getVGAuditorDetail(){
    this.subscription = this.vgAuditService.getVgAuditById(this.vgAuditNo).subscribe((res: any) => {
      setTimeout(() => {
        this.vgAuditById = res.res.auditsByNo;
        this.clientId = this.vgAuditById.client_id;
        this.vgAuditdetails = res.res.auditDetailsData;
        let secId = res.res.auditDetailsData;
        secId.map((data: any) => this.section_id.push(data.section_id));
        this.vgAuditReportForm.get('clientId').patchValue(this.vgAuditById.client_id);
        this.vgAuditReportForm.get('fromDate').patchValue(this.vgAuditById.audit_from_date.split('T')[0]);
        this.vgAuditReportForm.get('toDate').patchValue(this.vgAuditById.audit_to_date.split('T')[0]);
      },500)
      let clientid: any = document.getElementById('clientId');
      clientid.disabled = true;
      this.toastr.info('', 'Wait until data is loading...', {
        timeOut: 3500,
      });
      setTimeout(() => {
        this.checkBox = document.querySelectorAll('#checkId');
        for(let i=0; i<this.checkBox.length; i++){
           this.section_id.map((secId: any) => {
              if(this.checkBox[i].value === secId)
                 this.checkBox[i].checked = true;
            });
            this.checkBox[i].disabled = true;
        }
      }, 3500);
    });
  }

  editVGAuditDetails(){
    this.putVGAuditData(this.vgAuditNo, this.vgAuditReportForm.value);
  }

  putVGAuditData(vgAuditNo, vgAuditObj){
    this.subscription = this.vgAuditService.updateVgAudits(vgAuditNo, vgAuditObj).subscribe((res: any) => {
      if(res.success == true){
        this.toastr.success('Audit Details Updated Successfully! Please wait for Section details..');
        this.router.navigate([`/pages/VGAudit/VGAuditReport/${vgAuditNo}/sections`]);
        var encryptVgAuditDetails = CryptoJS.AES.encrypt(JSON.stringify(res.res), 'secret key 123').toString();
        localStorage.setItem('vgAuditDetails',encryptVgAuditDetails);
      } else{
        this.toastr.error('Something went wrong');
      }
    })
  }

  backToSection(){
    this.router.navigate(['/pages/VGAudit/SearchVGAudit']);
  }

  getClientDetails() {
    if(this.roleType !== 'Super Admin') {
      this.subscription = this.clientService.getClientForUsers(this.userObj.id).subscribe((res: any) => {
        this.clientId = res.res.id;
        sessionStorage.setItem('clientId', this.clientId);
        res.res.section_details.filter((sections: any) => {
          this.sectionsOfClient.push(JSON.parse(sections).id);
        });
      })
    }
  }
}
