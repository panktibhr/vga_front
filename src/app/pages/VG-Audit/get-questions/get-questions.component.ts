import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { vgAudit } from 'src/app/shared/models/vg-audit';
import { DataService } from 'src/app/shared/services/data.service';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { VgAuditService } from 'src/app/shared/services/vg-audit/vg-audit.service';
import * as CryptoJS from 'crypto-js';
import { SubSectionService } from 'src/app/shared/services/sub-section/sub-section.service';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
@Component({
  selector: 'app-get-questions',
  templateUrl: './get-questions.component.html',
  styleUrls: ['./get-questions.component.scss']
})
export class GetQuestionsComponent implements OnInit, OnDestroy {

  queSubscription: Subscription;
  subSectionID: any;
  section_id: any;
  get_questions: any = [];
  sequence_number: any;
  getVGAuditSubscription: Subscription;
  vgAudit: any;
  reportAnswerValue: any;
  _vgAudits: vgAudit;
  vgAuditDetails: any;
  vgAuditDetailsFromLocalStorage: string;
  auditDetails: any = [];
  sections: any = [];
  subSections: any = [];
  questions: any = [];
  reportanswers: any = [];
  answersOfQuestionValueIsTrue: any = [];
  clientName: any;
  allSubSections: any = [];
  subSectionName: any;
  sequenceNo: any;
  allSections: any;
  sectionName: any;
  sectionDetails: string;
  questionsOfManagerialLevel: any = [];
  secName: any;
  questionsOfExecutiveLevel: any = [];
  employeeLevel: any;
  clientEmployeeId: any;
  clientEmployeeName: string;
  questionsOfTopLevel: any = [];
  sectionId: any;
  clientId: string;
  ansObj: { questionId: any; vgAuditNo: any; clientEmployeeId: any; };

  constructor(private actRoute: ActivatedRoute, private router: Router, private location: Location, private vgAuditService: VgAuditService, private dataService: DataService,
    private subSectionService: SubSectionService, private sectionService: SectionMasterService, private questionService: QuestionMasterService) {
    this.subSectionID = this.actRoute.snapshot.params.id;
    this.employeeLevel = this.actRoute.snapshot.params.empLevel;
    this.clientEmployeeId = this.actRoute.snapshot.params.clientEmpId;
    this.vgAuditDetailsFromLocalStorage = CryptoJS.AES.decrypt(localStorage.getItem('vgAuditDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
    this.vgAuditDetails = JSON.parse(this.vgAuditDetailsFromLocalStorage);
    this.sectionDetails = CryptoJS.AES.decrypt(localStorage.getItem('sectionDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
    this.secName = JSON.parse(this.sectionDetails).section_name;
    this.clientId = sessionStorage.getItem('clientId');
    this.clientEmployeeName = localStorage.getItem('clientEmployeeName');
   }

  ngOnInit(): void {
    this.getVgAuditDetails();
    this.getQuestions();
    this.getAllSections();
    this.getAllSubSections();
    this.sectionProcess();
  }

  appendProgressColor() {
    console.log(this.questions)
    this.questions.map((obj: any) => {
      if(obj.is_submitted_questions == true){
        document.getElementById(obj.id).style.backgroundColor = '#B6E719';
      } else{
        document.getElementById(obj.id).style.backgroundColor = '#FF8022';
      }
    });
  }

  ngOnDestroy(){
    this.queSubscription?.unsubscribe();
    this.getVGAuditSubscription?.unsubscribe();
  }

  getVgAuditDetails(){
    this.vgAudit = (this.vgAuditDetails.audits) || (this.vgAuditDetails.res.audits);
    this.auditDetails = (this.vgAuditDetails.auditsDetails) || (this.vgAuditDetails.res.auditsDetails);
  }

  getAllSections(){
    this.sectionService.getAllSec().subscribe((res: any) => {
      this.allSections = res.res;
      this.allSections.map((res: any)=>{
        if(res.id == this.subSectionID){
          this.sectionName = res.section_name;
        }
      })
    })
  }

  getAllSubSections(){
    this.subSectionService.getAllSubSection().subscribe((res: any) => {
      this.allSubSections = res.res;
      this.allSubSections.map((res: any)=>{
        if(res.id === this.subSectionID){
          this.subSectionName = res.sub_section_name;
          this.sequenceNo = res.sequence_number;
        }
      })
    })
  }

  getQuestions(){
    let section = JSON.parse(this.sectionDetails);
    let subSec = section.subSection;
    subSec.map((res: any)=>{
      this.sectionId = res.section_id;
      if(res.id === this.subSectionID){
        let que = res.questions;
        que.map((res: any)=>{
          if(res.sub_section_id === this.subSectionID){
            if(this.secName === 'Competency Mapping'){
              if(res.question_type === 6){
                this.questionsOfTopLevel.push(res);
              } else if(res.question_type === 7){
                this.questionsOfManagerialLevel.push(res)
              } else if(res.question_type === 8){
                this.questionsOfExecutiveLevel.push(res);
              }
            } else{
              this.questions.push(res);
            }
          }
        })
      }
    })

    if(subSec.length === 0){
      let que = section.questions;
      que.map((res: any)=>{
        if(res.section_id === this.subSectionID){
          this.questions.push(res);
        }
      })
    }
    this.questions.map((que: any)=>{
      let statusColor = que.successcount;
      setTimeout(() => {
        if(statusColor === true){
          document.getElementById(que.id).style.backgroundColor = '#B6E719';
        } else{
          document.getElementById(que.id).style.backgroundColor = '#FF8022';
        }
      },1000)
    })
    this.questionsOfTopLevel.map((que: any)=>{
      this.ansObj = {
        "questionId": que.id,
        "vgAuditNo": this.vgAudit.vga_audit_number,
        "clientEmployeeId": this.clientEmployeeId ? this.clientEmployeeId : null
      }
      this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
        let reportanswers = res.res;
        setTimeout(() => {
          if(reportanswers.length > 0){
            document.getElementById(que.id).style.backgroundColor = '#B6E719';
          } else {
            document.getElementById(que.id).style.backgroundColor = '#FF8022';
          }
        }, 1000)
      })
      let statusColor = que.successcount;
      setTimeout(() => {
        if(statusColor === true){
          document.getElementById(que.id).style.backgroundColor = '#B6E719';
        } else{
          document.getElementById(que.id).style.backgroundColor = '#FF8022';
        }
      },1000)
    })
    this.questionsOfManagerialLevel.map((que: any)=>{
      this.ansObj = {
        "questionId": que.id,
        "vgAuditNo": this.vgAudit.vga_audit_number,
        "clientEmployeeId": this.clientEmployeeId ? this.clientEmployeeId : null
      }
      this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
        let reportanswers = res.res;
        setTimeout(() => {
          if(reportanswers.length > 0){
            document.getElementById(que.id).style.backgroundColor = '#B6E719';
          } else {
            document.getElementById(que.id).style.backgroundColor = '#FF8022';
          }
        }, 1000)
      })
      let statusColor = que.successcount;
      setTimeout(() => {
        if(statusColor === true){
          document.getElementById(que.id).style.backgroundColor = '#B6E719';
        } else{
          document.getElementById(que.id).style.backgroundColor = '#FF8022';
        }
      },1000)
    })
    this.questionsOfExecutiveLevel.map((que: any)=>{
      this.ansObj = {
        "questionId": que.id,
        "vgAuditNo": this.vgAudit.vga_audit_number,
        "clientEmployeeId": this.clientEmployeeId ? this.clientEmployeeId : null
      }
      this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
        let reportanswers = res.res;
        setTimeout(() => {
          if(reportanswers.length > 0){
            document.getElementById(que.id).style.backgroundColor = '#B6E719';
          } else {
            document.getElementById(que.id).style.backgroundColor = '#FF8022';
          }
        }, 1000)
      })
      let statusColor = que.successcount;
      setTimeout(() => {
        if(statusColor === true){
          document.getElementById(que.id).style.backgroundColor = '#B6E719';
        } else{
          document.getElementById(que.id).style.backgroundColor = '#FF8022';
        }
      },1000)
    })
  }

  sectionProcess(){
    let vgAuditNo = {
      'vgAuditNo' : this.vgAudit.vga_audit_number,
      'clientId': this.clientId
    }
    this.questionService.postVgAuditReportObjForCompetency(this.sectionId, vgAuditNo).subscribe((res: any) => {
      console.log(res);
    })
  }

  onClickOfQuestion(queId, sequNo){
    console.log(queId, sequNo)
    this.ansObj = {
      "questionId": queId,
      "vgAuditNo": this.vgAudit.vga_audit_number,
      "clientEmployeeId": this.clientEmployeeId
    }
    this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
      let ans = res.res;
      var encryptAnswers = CryptoJS.AES.encrypt(JSON.stringify(ans), 'secret key 123').toString();
      localStorage.setItem('answers', encryptAnswers);
    })
    this.sequence_number = sequNo;
    var encryptQuestions = CryptoJS.AES.encrypt(JSON.stringify(this.questions), 'secret key 123').toString();
    localStorage.setItem('lengthOfQuestion',encryptQuestions);
    var encryptTopQuestions = CryptoJS.AES.encrypt(JSON.stringify(this.questionsOfTopLevel), 'secret key 123').toString();
    localStorage.setItem('topLevelQuestions',encryptTopQuestions);
    var encryptManagerialQuestions = CryptoJS.AES.encrypt(JSON.stringify(this.questionsOfManagerialLevel), 'secret key 123').toString();
    localStorage.setItem('managerialLevelQuestions',encryptManagerialQuestions);
    var encryptExecutiveQuestions = CryptoJS.AES.encrypt(JSON.stringify(this.questionsOfExecutiveLevel), 'secret key 123').toString();
    localStorage.setItem('executiveLevelQuestions',encryptExecutiveQuestions);
    if(this.sequenceNo === 2 && this.secName === 'Competency Mapping'){
      this.router.navigate([`/pages/VGAudit/vgaudits/${this.subSectionID}/${queId}/${this.sequence_number}/${this.clientEmployeeId}`])
    } else{
      this.router.navigate([`/pages/VGAudit/vgaudits/${this.subSectionID}/${queId}/${this.sequence_number}`])
    }
  }

  backToSubSection(){
    // this.router.navigate(['/pages/VGAudit/VGAuditReport/subsection/', this.sub_section_id])
    this.location.back();
  }

}
