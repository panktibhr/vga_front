import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewVgAuditComponent } from './new-vg-audit/new-vg-audit.component';
import { SearchVgAuditComponent } from './search-vg-audit/search-vg-audit.component';
import { ValueGrowthAuditReportComponent } from './value-growth-audit-report/value-growth-audit-report.component';
import { VgaReportComponent } from './vga-report/vga-report.component';
import { EditVgAuditComponent } from './edit-vg-audit/edit-vg-audit.component';
import { GetSubsectionComponent } from './get-subsection/get-subsection.component';
import { GetQuestionsComponent } from './get-questions/get-questions.component';
import { RadioQuestionsComponent } from './vga-report/radio-question/radio-question.component';
import { CompetencyLevelsComponent } from './vga-report/competency-levels/competency-levels.component';
import { EmployeeLevelsComponent } from './vga-report/employee-levels/employee-levels.component';

const managementRoutes: Routes = [
  { path: 'VGAuditReport', component: NewVgAuditComponent },
  { path: 'VGAuditReport/:id/sections', component: EditVgAuditComponent },
  { path: 'VGAuditReport/subsection/:id', component: GetSubsectionComponent },
  { path: 'VGAuditReport/questions/:id', component: GetQuestionsComponent },
  { path: 'VGAuditReport/questions/:id/:clientEmpId/:empLevel', component: GetQuestionsComponent },
  { path: 'VGAuditReport/levels', component: CompetencyLevelsComponent },
  { path: 'VGAuditReport/levels/:id/:level', component: EmployeeLevelsComponent },
  { path: 'SearchVGAudit', component:SearchVgAuditComponent},
  { path: 'SearchVGAudit/:reports', component:SearchVgAuditComponent},
  { path: 'vgaudits', component: VgaReportComponent },
  { path: 'vgaudits/:id', component: VgaReportComponent },
  { path: 'vgaudits/:secId/:subSecId', component: VgaReportComponent },
  { path: 'vgaudits/subSections/:subSecId/:queId/:seqNo', component: VgaReportComponent },
  { path: 'vgaudits/subSections/:subSecId/:queId/:seqNo/:clientEmpId', component: VgaReportComponent },
  { path: 'vgaudits/:secId/:queId/:seqNo', component: VgaReportComponent },
  { path: 'vgaudits/:secId/:queId/:seqNo/:clientEmpId', component: VgaReportComponent },
  { path: 'ValueGrowthAuditReport/:vgAuditNo', component: ValueGrowthAuditReportComponent },
  { path: 'VGAuditReport/:vgAuditNo', component: NewVgAuditComponent },
  { path: 'vgaudits/:secId/:queId/:seqNo/employee/:employeeId', component: RadioQuestionsComponent },
  { path: 'vgaudits/:secId/:queId/:seqNo/employee/:employeeId/:employeePerformanceUserId', component: RadioQuestionsComponent },
]
@NgModule({
  imports: [RouterModule.forChild(managementRoutes)],
  exports: [RouterModule]
})
export class VGAuditRoutingModule { }
