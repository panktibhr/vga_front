import { Component, OnInit } from '@angular/core';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';

@Component({
  selector: 'app-vga-report-details',
  templateUrl: './vga-report-details.component.html',
  styleUrls: ['./vga-report-details.component.scss']
})
export class VgaReportDetailsComponent implements OnInit {

  vgAudit: any = [];
  vgAuditDetailsFromLocalStorage: string;
  vgAuditDetails: any;
  clientName: any;
  auditDetails: any;

  constructor(private clientService: ClientsService) {
    this.vgAuditDetailsFromLocalStorage = CryptoJS.AES.decrypt(localStorage.getItem('vgAuditDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
  }

  ngOnInit(): void {
    this.getVgAuditDetails();
    this.getAllClients();
  }
  getVgAuditDetails() {
    this.vgAuditDetails = JSON.parse(this.vgAuditDetailsFromLocalStorage);
    this.vgAudit = this.vgAuditDetails.audits || this.vgAuditDetails.res.audits;
    this.auditDetails = (this.vgAuditDetails.auditsDetails) || (this.vgAuditDetails.res.auditsDetails);
  }

  getAllClients(){
    this.clientService.allClients().subscribe((res: any) => {
      let clients = res.res;
      clients.map((client: any)=>{
        if(client.id === this.vgAudit.client_id){
          this.clientName = client.name;
          sessionStorage.setItem('clientId', client.id);
        }
      })
    })
  }

}
