import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VgaReportDetailsComponent } from './vga-report-details.component';

describe('VgaReportDetailsComponent', () => {
  let component: VgaReportDetailsComponent;
  let fixture: ComponentFixture<VgaReportDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VgaReportDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VgaReportDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
