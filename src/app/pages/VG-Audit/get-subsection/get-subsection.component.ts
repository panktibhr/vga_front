import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/services/data.service';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
import { SubSectionService } from 'src/app/shared/services/sub-section/sub-section.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-get-subsection',
  templateUrl: './get-subsection.component.html',
  styleUrls: ['./get-subsection.component.scss']
})
export class GetSubsectionComponent implements OnInit, OnDestroy {

  sectionID: any;
  subSecSubscription: Subscription;
  sectionSubscription: Subscription;
  getVGAuditSubscription: Subscription;
  vgAudit: any = [];
  vgAuditDetails: any;
  vgAuditDetailsFromLocalStorage: string;
  statusColor: any;
  sections:any = [];
  subSections: any = [];
  subSecColorArr: any = [];
  auditDetails: any = [];
  questions: any = [];
  answersOfQuestionValueIsTrue: any = [];
  answersOfQuestionValueIsFalse: any = [];
  clientName: any;
  allSections: any = [];
  sectionName: any;
  sequenceNo: any;
  selectedSection:any;
  sectionDetails: string;

  constructor(private actRoute: ActivatedRoute, private router: Router, private location: Location,
    private sectionService: SectionMasterService, private subSectionService: SubSectionService, private serviceData: DataService) {
    this.sectionID = this.actRoute.snapshot.params.id;
    this.serviceData.changeMessage(this.sectionID);
    localStorage.setItem('section-id', CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(this.sectionID), environment.SECRET_KEY.trim()).toString());
    this.vgAuditDetailsFromLocalStorage = CryptoJS.AES.decrypt(localStorage.getItem('vgAuditDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
    this.sectionDetails = CryptoJS.AES.decrypt(localStorage.getItem('sectionDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
  }

  ngOnInit(): void {
    this.getVgAuditDetails();
    this.getSubSectionsBySection();
    this.getAllSections();
    this.getStatusOfSubSection();
  }

  ngOnDestroy(){
    this.subSecSubscription?.unsubscribe();
    this.sectionSubscription?.unsubscribe();
    this.getVGAuditSubscription?.unsubscribe();
  }

  getVgAuditDetails(){
    this.vgAuditDetails = JSON.parse(this.vgAuditDetailsFromLocalStorage);
    this.vgAudit = (this.vgAuditDetails.audits) || (this.vgAuditDetails.res.audits);
    this.auditDetails = (this.vgAuditDetails.auditsDetails) || (this.vgAuditDetails.res.auditsDetails);
  }

  getAllSections(){
    this.sectionService.getAllSec().subscribe((res: any) => {
      this.allSections = res.res;
      this.allSections.map((res: any)=>{
        if(res.id === this.sectionID){
          this.sectionName = res.section_name;
          this.sequenceNo = res.sequence_number;
        }
      })
    })
  }

  getSubSectionsBySection(){
    let section = JSON.parse(this.sectionDetails);
    let subSec = section.subSection;
    subSec.map((res: any)=>{
      if(res.section_id === this.sectionID){
        this.subSections.push(res);
      }
    })
    this.subSections.map((subSec: any)=>{
      let statusColor = subSec.subsectionColor;
      setTimeout(() => {
        if(statusColor === 'green'){
            document.getElementById(subSec.id).style.backgroundColor = '#B6E719';
        } else if(statusColor === 'red'){
            document.getElementById(subSec.id).style.backgroundColor = '#FF8022';
        } else{
            document.getElementById(subSec.id).style.backgroundColor = '#FFCA06';
        }
      },500)
    })
  }

  getQuestions(subSection){
    let id = subSection.id;
    let seqNoOfSubSection = subSection.sequence_number;
    if(seqNoOfSubSection === 2 && this.sectionName === 'Competency Mapping'){
      localStorage.setItem('subSectionId', id);
      this.router.navigate(['/pages/VGAudit/VGAuditReport/levels']);
    } else{
      this.router.navigate(['/pages/VGAudit/VGAuditReport/questions/', id]);
    }
  }

  getStatusOfSubSection(){
    let obj = {
      'sectionId': this.sectionID,
      'vgaAuditNumber': this.vgAudit.vga_audit_number
    }
    console.log(obj)
    this.subSectionService.postSubSectionStatus(obj).subscribe((res: any) => {
      if(res){
        let statusColor = res.status.statusColorOfSubSection;
        this.subSections.map((subSec: any)=>{
          if(subSec.sequence_number === 2){
            setTimeout(() => {
              if(statusColor === 'green'){
                  document.getElementById(subSec.id).style.backgroundColor = '#B6E719';
              } else if(statusColor === 'red'){
                  document.getElementById(subSec.id).style.backgroundColor = '#FF8022';
              } else{
                  document.getElementById(subSec.id).style.backgroundColor = '#FFCA06';
              }
            },500)
          }
        })
      }
    })
  }

  backToSection(){
    this.location.back();
  }

}
