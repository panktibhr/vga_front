import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetSubsectionComponent } from './get-subsection.component';

describe('GetSubsectionComponent', () => {
  let component: GetSubsectionComponent;
  let fixture: ComponentFixture<GetSubsectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetSubsectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetSubsectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
