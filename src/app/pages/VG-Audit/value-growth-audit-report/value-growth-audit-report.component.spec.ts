import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueGrowthAuditReportComponent } from './value-growth-audit-report.component';

describe('ValueGrowthAuditReportComponent', () => {
  let component: ValueGrowthAuditReportComponent;
  let fixture: ComponentFixture<ValueGrowthAuditReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueGrowthAuditReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueGrowthAuditReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
