import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { clientEmployees } from 'src/app/shared/models/client-employee';
import { EmployeePerformance } from 'src/app/shared/models/employee-performance';
import { vgAudit } from 'src/app/shared/models/vg-audit';
import { EmployeePerformanceService } from 'src/app/shared/services/employee-performance/employee-performace.service';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { ReportAnswerValuesService } from 'src/app/shared/services/report-AnswerValues/report-answer-values.service';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
import { VgAuditService } from 'src/app/shared/services/vg-audit/vg-audit.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-value-growth-audit-report',
  templateUrl: './value-growth-audit-report.component.html',
  styleUrls: ['./value-growth-audit-report.component.scss']
})
export class ValueGrowthAuditReportComponent implements OnInit, OnDestroy {

  @ViewChild('tabset') tabset: TabsetComponent;
  @ViewChild('first') first: TabDirective;
  @ViewChild('second') second: TabDirective;
  @ViewChild('third') third: TabDirective;
  @ViewChild('forth') forth: TabDirective;
  @ViewChild('fifth') fifth: TabDirective;
  subSectionNo: number = 0;
  organizationDiagnosisSeqNo: number = 0;
  competencyMappingSeqNo: number = 0;
  performanceEvaluationSeqNo: number = 0;
  planOfActionSeqNo: number = 0;
  performanceEvaluationCounter: number = 0;
  competencyMappingCounter: number = 0;
  _vgAudits : vgAudit;
  _clientEmployees: clientEmployees;
  employeePerformance: EmployeePerformance;
  vgaReportForm: FormGroup;
  q: any
  queId: any;
  userObj: any;
  sectionID: any;
  vgAuditNo: any;
  clientId: any;
  sectionId: any;
  questionId: any;
  questionFor: any;
  sectionName: any;
  statusValue: any;
  questionType: any;
  totalAverage: any;
  questionName: any;
  getQuestionId: any;
  subSectionName: any;
  getQuestionName: any;
  lengthOfQuestions: any;
  averageOfQuestion: any;
  sequenceNoOfQuestion: any;
  sequenceNoOfSubSection: any;
  updatedConsultantReview: any;
  txtAreaValue: String;
  subSec: any = [];
  secName: any = [];
  question: any = [];
  sections: any = [];
  questions: any = [];
  employeeId: any = [];
  subSection: any = [];
  employeeName: any = [];
  reportAnswer: any = [];
  section_name: any = [];
  getQuestions: any = [];
  employeeData: any = [];
  vgAuditDetails: any = [];
  subQuestions:any = [];
  employeePerformanceId: any = [];
  individualPerformanceData: any = [];
  employeePerformanceQuestion: any = [];
  getQuestionsBySectionOrSubSection: any = [];
  PdfObj: { vgAuditNo: any; sectionId: any; clientId: any; };
  consultantViewForPerformanceEvaluation: any;
  consultantReviewFormForFirstSection: { UserId: any; QuestionId: any; consultantReview: String; vgaAuditNumber: any; };
  consultantReviewFormForSecondSection: { UserId: any; SectionId: any; QuestionId: any; consultantReview: any; vgaAuditNumber: any; clientEmployeeId: any; };
  consultantReviewFormForThirdSection: { UserId: any; QuestionId: any; consultantReview: String; vgaAuditNumber: any; employeeId?: any; employeePerformanceQuestionId?: any; };
  subscription: Subscription;
  consultantViewForPlanOfAction: any;
  consultantReviewFormForFifthSection: { UserId: any; ClientId: any; QuestionId: any; consultantReview: any; vgaAuditNumber: any;employeeId:any;employeePerformanceQuestionId:any;}; //  
  answersOfStrategicSphere1: any = [];
  answersOfStrategicSphere2: any = [];
  initiative1: any = [];
  initiative2: any = [];
  initiative3: any = [];
  consultantReviewForPlanOfActions: any = [];
  consultantReviewForOnClickOfNextForSection5: any = [];
  c1: any;
  c2: any;
  c3: any;
  questionIdForSection5: any;
  answersOfDefiniteDirection1: any = [];
  answersOfDefiniteDirection2: any = [];
  answersOfDefiniteDirection3: any = [];
  demands: any = [];
  policies: any = [];
  supply: any = [];
  answersOfConcreteCommecement1: any = [];
  answersOfConcreteCommecement2: any = [];
  answersOfConcreteCommecement3: any = [];
  answersOfPurposefulPlanning1: any = [];
  answersOfPurposefulPlanning2: any = [];
  answersOfPurposefulPlanning3: any = [];
  answersOfGrowthFrameOfStrategy: any = [];
  answersOfGrowthFrameOfSustainability: any = [];
  answersOfGrowthFrameOfSolutions: any = [];
  answersOfGrowthFrameOfSystem: any = [];
  answersOfGrowthFrameOfStyleOfLeadership: any = [];
  answersOfGrowthFrameOfSkills: any = [];
  answersOfGrowthFrameOfStructure: any = [];
  answersOfGrowthFrameOfSharedValues: any = [];
  answersOfGrowthFrameOfStandardOperationProducers: any = [];
  answersOfGrowthTimeline: any = [];
  answersOfStrategicActionDescriptions: any = [];
  answersOfDeptResponsible: any = [];
  answersOfdateTobegin: any = [];
  answersOfDateTodue: any = [];
  answersOfResourcesRequired: any = [];
  answersOfDesiredOutcome: any = [];
  answersOfPotentialazards: any = [];
  answersOfStrategicAction: any = [];
  answersOfStackHolders: any = [];
  answersOfConstraints: any = [];
  answersOfMetric: any = [];
  answersOfPriority: any = [];
  answersOfExecutionTimeline: any = [];
  clientEmployeeOfTopLevel: any = [];
  subSectionOfImportance: any;
  subSectionOfPerformance: any;
  performanceHighImportanceLow: any = [];
  performanceLowImportanceLow: any = [];
  performanceHighImportanceHigh: any = [];
  performanceLowImportanceHigh: any = [];
  topLevelQuestionsOfImportance: any = [];
  managerialLevelQuestionsOfImportance: any = [];
  executiveLevelQuestionsOfImportance: any = [];
  ansObj: any = [];
  topLevel:any;
  average: number;
  topLevelImportanceQuestionForAverage: any;
  topLevelPerformanceQuestionForAverage: any;
  averageOfTopLevelImportance: number;
  consultantViewForCompetencyMapping: any;
  competencyMapping: { topLevel: any; managerialLevel: any; executiveLevel: any; }[];
  index: any;
  levels: string;
  levelIndex: number;
  consulatantReview: any;
  topLevelArr: any[] = [];
  managerialLevelArr: any[] = [];
  executiveLevelArr: any[] = [];

  constructor(private sectionService: SectionMasterService, private reportAnswerService: ReportAnswerValuesService,private toastr: ToastrService,
    private actRoute: ActivatedRoute, private vgAuditService: VgAuditService, private fb: FormBuilder, private questionService: QuestionMasterService, private employeePerformanceService: EmployeePerformanceService) {
    this._vgAudits = new vgAudit();
    this._clientEmployees = new clientEmployees();
    this.employeePerformance = new EmployeePerformance();
    setTimeout(() => {
      this.ngOnInit();
      this.postAverage();
      if(localStorage.getItem('lengthOfQuestion')) {
        this.q = CryptoJS.AES.decrypt(localStorage.getItem('lengthOfQuestion'), 'secret key 123').toString(CryptoJS.enc.Utf8);
        this.getQuestionsBySectionOrSubSection = JSON.parse(this.q);
      }
      if((this.organizationDiagnosisSeqNo === this.lengthOfQuestions) || (this.planOfActionSeqNo === this.lengthOfQuestions) ){
        document.getElementById('next').style.visibility = "hidden";
        document.getElementById('submit').style.float = "right";
      }
    },1000)
  }

  ngOnInit(): void {
    this.vgAuditNo = this.actRoute.snapshot.params.vgAuditNo;
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.getAllSections();
    this.getVgAuditDetailsByVgAuditNumber();
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  postAverage(){
    if(this.sectionId !== this.vgAuditDetails[4].section_id){
      let averageObj:any = {
        'sectionId': this.vgAuditDetails[0].section_id,
        'vgAuditNo': this.vgAuditNo
      }
      this.subscription = this.questionService.postAverageOfQuestion(averageObj).subscribe(()=>{});
    }
  }

  getVgAuditDetailsByVgAuditNumber(){
    this.vgAuditService.getVgAuditById(this.vgAuditNo).subscribe((res: any) => {
      this.clientId =res.res.auditsByNo.client_id;
      this.vgAuditDetails = res.res.auditDetailsData;
      let vgAuditNo = {
        'vgAuditNo' : this.vgAuditNo,
        'clientId': this.clientId
      }
      this.subscription = this.questionService.postVgAuditReportObj(this.vgAuditDetails[0].section_id, vgAuditNo).subscribe((res: any) => {
        this.sectionId = res.res.sections[0].id;
        this.sectionName = res.res.sections[0].section_name;
        this.subSection = res.res.sections[0].subSection;
        this.subSectionName = res.res.sections[0].subSection[0].sub_section_name;
        this.question = res.res.sections[0].subSection[0].questions[0].question;
        this.queId = res.res.sections[0].subSection[0].questions[0].id;
        this.sequenceNoOfQuestion = res.res.sections[0].subSection[0].questions[0].sequence_number;
        this.sequenceNoOfSubSection = res.res.sections[0].subSection[0].sequence_number;
        this.averageOfQuestion = res.res.sections[0].subSection[0].questions[0].reportanswers[0].question_average;
        this.totalAverage = res.res.sections[0].subSection[0].questions[0].reportanswers[0].total_average;
        this.questionType = res.res.sections[0].subSection[0].questions[0].question_type;
        setTimeout(() => {
          let average = document.querySelector('#average');
          if(average){
            let parentElement = average.parentElement;
            parentElement.style.backgroundColor = 'darkgrey';
          }
        }, 100);
        if(this.subSectionNo === 0){
          document.getElementById('previous').style.display = 'none';
        }
        this.createForm();
        this.txtAreaValue = res.res.sections[0].subSection[0].questions[0].reportanswers[0].consultant_review;
        // this.statusValue = res.res.sections[0].subSection[0].questions[this.seqNo].reportanswers[0].question_status;
        this.patchConsultantReview(this.txtAreaValue);
      });
    });
  }

  createForm(){
    if(this.sectionId === this.vgAuditDetails[2].section_id){
      this.vgaReportForm = this.fb.group({
        UserId: [this.userObj.id],
        QuestionId: [this.queId],
        vgaAuditNumber: [this.vgAuditNo],
        consultantReview: [this.consultantViewForPerformanceEvaluation],
        employeeId: [this.employeeId[this.performanceEvaluationCounter]],
        employeePerformanceQuestionId: [this.employeePerformanceId[this.performanceEvaluationCounter]]
        // status: [this.statusValue]
      });
    } else if(this.sectionId === this.vgAuditDetails[0].section_id){
      this.vgaReportForm = this.fb.group({
        UserId: [this.userObj.id],
        QuestionId: [this.queId],
        vgaAuditNumber: [this.vgAuditNo],
        consultantReview: [this.txtAreaValue],
        employeeId: [this.employeeId[this.performanceEvaluationCounter]],
        employeePerformanceQuestionId: [this.employeePerformanceId[this.performanceEvaluationCounter]]
        // status: [this.statusValue]
      });
    } else if(this.sectionId === this.vgAuditDetails[4].section_id){
      this.vgaReportForm = this.fb.group({
        UserId: [this.userObj.id],
        ClientId: [this.clientId],
        QuestionId: [this.queId],
        vgaAuditNumber: [this.vgAuditNo],
        consultantReview: this.fb.group({
          A: this.c1,
          B: this.c2,
          C: this.c3
        })
      });
    }
  }

  getAllSections(){
    this.sectionService.getAllSec().subscribe((res: any) => {
      this.sections = res.res;
      this.sections.map((res: any)=>{
        this.secName.push(res.section_name);
        this.sectionID = res.id;
      })
    })
  }

  onClickOfSection(sectionId){
    this.sectionId = sectionId;
    let vgAuditNo = {
      'vgAuditNo' : this.vgAuditNo,
      'clientId': this.clientId
    }
    let averageObj:any = {
      'sectionId': sectionId,
      'vgAuditNo': this.vgAuditNo
    }
    if(this.sectionId !== this.vgAuditDetails[4].section_id){
      this.subscription = this.questionService.postAverageOfQuestion(averageObj).subscribe((res: any)=>{
        this.individualPerformanceData.push(res.res.section?.individualPerformanceEvaluation);
      });
    }
    this.totalAverage = '';
    this.averageOfQuestion = '';
    this.subSectionNo = 0;
    this.sequenceNoOfQuestion = 1;
    this.organizationDiagnosisSeqNo = 0;
    this.performanceEvaluationCounter = 0;
    this.planOfActionSeqNo = 0;
    document.querySelectorAll('#averag-box')?.forEach((ele: any) => {
      ele.style.backgroundColor = '#D2D6DE';
    });
    document.querySelectorAll('#average-box')?.forEach((ele: any) => {
      ele.style.backgroundColor = '#D2D6DE';
    });

    this.questionService.postVgAuditReportObjForCompetency(this.sectionId, vgAuditNo).subscribe((res: any) => {
      console.log('res --- ', res)
      let section2 = res.res.sections[0];
      if(this.sectionId === this.vgAuditDetails[1].section_id){
        this.sectionName = section2.section_name;
        this.clientEmployeeOfTopLevel = section2.topLevel;
        section2.subSection.filter((subSec: any) => {
          if(subSec.sequence_number === 1){
            this.subSectionOfImportance = subSec;
            this.subSectionOfImportance.questions.filter((que:any) => {
              if(que.question_type === 6){
                this.topLevelQuestionsOfImportance.push(que);
              } else if(que.question_type === 7){
                this.managerialLevelQuestionsOfImportance.push(que);
              } else if(que.question_type === 8){
                this.executiveLevelQuestionsOfImportance.push(que);
              }
            })
          } else if(subSec.sequence_number === 2){
            this.subSectionOfPerformance = subSec;
          }
        })

        this.competencyMapping = [{
          "topLevel": section2.topLevel,
          "managerialLevel": section2.managerialLevel,
          "executiveLevel": section2.executiveLevel
        }];
        // console.log(`competencyMapping --- `, this.competencyMapping)

        let topLevel = [];
        let managerialLevel = [];
        let executiveLevel = [];
        this.competencyMapping[0].topLevel.map((top) => {
          top.questions.map((que) => {
            this.ansObj = { "questionId": que.id, "vgAuditNo": this.vgAuditNo, "clientEmployeeId": top.id }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              if(res.res && res.res.length > 0) topLevel.push(top)
              else {}
            })
          })
        })
        this.competencyMapping[0].managerialLevel.map((managerial) => {
          managerial.questions.map((que) => {
            this.ansObj = { "questionId": que.id, "vgAuditNo": this.vgAuditNo, "clientEmployeeId": managerial.id }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              if(res.res && res.res.length > 0) managerialLevel.push(managerial)
              else {}
            })
          })
        })
        this.competencyMapping[0].executiveLevel.map((executive) => {
          executive.questions.map((que) => {
            this.ansObj = { "questionId": que.id, "vgAuditNo": this.vgAuditNo, "clientEmployeeId": executive.id }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              if(res.res && res.res.length > 0) executiveLevel.push(executive)
              else {}
            })
          })
        })

        setTimeout(() => {
          this.performanceHighImportanceLow = [];
          this.performanceLowImportanceLow = [];
          this.performanceHighImportanceHigh = [];
          this.performanceLowImportanceHigh = [];
          this.topLevelArr = [...new Set(topLevel.map(item => item.id))].map(top => { return topLevel.find(items => items.id === top)})
          // console.log('topLevel --- ', this.topLevelArr) ;
          this.managerialLevelArr = [...new Set(managerialLevel.map(item => item.id))].map(managerial => { return managerialLevel.find(items => items.id === managerial)})
          // console.log('managerialLevel --- ', this.managerialLevelArr) ;
          this.executiveLevelArr = [...new Set(executiveLevel.map(item => item.id))].map(executive => { return executiveLevel.find(items => items.id === executive)})
          // console.log('executiveLevel --- ', this.executiveLevelArr) ;

          this.topLevelArr[0].questions.map((employeeCompetency: any) => {
            this.levelIndex = 1;
            this.levels = 'Top Level';
            this.index = 0;
            this.topLevel = this.clientEmployeeOfTopLevel[0].name;

            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": this.clientEmployeeOfTopLevel[0].id
            }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;

              this.topLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / 4;

                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                      console.log('performanceHighImportanceLow  - ', this.performanceHighImportanceLow)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                      console.log('performanceLowImportanceLow - ', this.performanceLowImportanceLow)
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceHighImportanceHigh - ', this.performanceHighImportanceHigh)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceLowImportanceLow - ', this.performanceLowImportanceHigh)
                    }
                  }
                }
              })
            })
          })
        }, 500)
      }
    })

    this.subscription = this.questionService.postVgAuditReportObj(this.sectionId, vgAuditNo).subscribe((res: any) => {
      // For 1st section
      if(this.sectionId === this.vgAuditDetails[0].section_id){
        this.sectionName = res.res.sections[0].section_name;
        this.subSection = res.res.sections[0].subSection;
        this.subSectionName = this.subSection[0].sub_section_name;
        this.sequenceNoOfSubSection = res.res.sections[0].subSection[0].sequence_number;
        this.question = res.res.sections[0].subSection[0].questions[0].question;
        this.questionId = res.res.sections[0].subSection[0].questions[0].id;
        this.sequenceNoOfQuestion = res.res.sections[0].subSection[0].questions[0].sequence_number;
        this.averageOfQuestion = res.res.sections[0].subSection[0].questions[0].reportanswers[0].question_average;
        this.totalAverage = res.res.sections[0].subSection[0].questions[0].reportanswers[0].total_average;
        this.txtAreaValue = res.res.sections[0].subSection[0].questions[0].reportanswers[0].consultant_review;
        setTimeout(() => {
          let average = document.querySelector('#average');
          if(average){
            let parentElement = average.parentElement;
            parentElement.style.backgroundColor = 'darkgrey';
          }
        }, 100);
        this.patchConsultantReview(this.txtAreaValue);
      }

      //For 3rd section
      if(this.sectionId === this.vgAuditDetails[2].section_id){
        this.getQuestions = res.res.sections[0].questions;
        this.getQuestionId = this.getQuestions[0].id;
        this.getQuestionName = this.getQuestions[0].question;
        this.questionFor = this.getQuestions[1].question;
        let reportAnswer = [], consultantReviewValue = [];
        reportAnswer = res.res.sections[0].questions[0].reportanswers;
        reportAnswer.filter((reportAnswers: any) => {
          setTimeout(() => {
            if(reportAnswers.employee_id === this.employeeId[this.performanceEvaluationCounter]){
              if(this.employeePerformanceId[this.performanceEvaluationCounter] === reportAnswers.employee_performance_questions_id){
                if(this.getQuestions[this.performanceEvaluationSeqNo].id === reportAnswers.question_id){
                  consultantReviewValue.push(reportAnswers.consultant_review);
                  this.consultantViewForPerformanceEvaluation = consultantReviewValue[0];
                  this.patchConsultantReview(this.consultantViewForPerformanceEvaluation);
                }
              }
            }
          }, 1000);
        });
        this.getEmployeeListing();
        this.toastr.info('', 'Wait until data is loading...', {
          timeOut: 5800,
        });
        setTimeout(() => {
          this.employeeData.filter((employeeData: any) => {
            if(this.employeeName[this.performanceEvaluationCounter] === employeeData.clientEmployee.name){
              if(this.employeePerformanceQuestion[this.performanceEvaluationCounter] === employeeData.employeePerformance.name){
                if(this.getQuestions[this.performanceEvaluationSeqNo].id === employeeData.question_id){
                  this.averageOfQuestion = employeeData.question_average;
                } else if (this.getQuestions[this.performanceEvaluationSeqNo].id !== employeeData.question_id){
                    this.totalAverage = employeeData.question_average;
                }
              }
            }
          });
          setTimeout(() => {
            let average = document.querySelector('#averag');
            if(average){
              let parentElement = average.parentElement;
              parentElement.style.backgroundColor = 'darkgrey';
            }
          }, 100);
        }, 6100);
      }
      // For 4th section
      if(this.sectionId === this.vgAuditDetails[3].section_id){
        this.reportAnswer = res.res.sections[0].questions[0].reportanswers;
        document.getElementById('next').innerText = 'Save';
      }
      // For 5th section
      if(this.sectionId === this.vgAuditDetails[4].section_id){
        this.questionIdForSection5 = res.res.sections[0].subSection[0].questions[0].id;
        this.reportAnswerService.getAnswerValues(this.questionIdForSection5, this.vgAuditNo).subscribe((data: any) => {
          this.answersOfStrategicSphere1 = data.res[0].answer_value.strategicSphere1;
          this.answersOfStrategicSphere2 = data.res[0].answer_value.strategicSphere2;
          this.initiative1 = data.res[0].answer_value.strategicSphere3.initiative1;
          this.initiative2 = data.res[0].answer_value.strategicSphere3.initiative2;
          this.initiative3 = data.res[0].answer_value.strategicSphere3.initiative3;
          this.consultantReviewForPlanOfActions = data.res[0].consultant_review;
        })
        this.subSection = res.res.sections[0].subSection;
        this.subSectionName = this.subSection[0].sub_section_name;
        this.questionName = res.res.sections[0].subSection[0].questions[0].question;
        this.sequenceNoOfQuestion = res.res.sections[0].subSection[0].questions[0].sequence_number;
      }
      // this.statusValue = res.res.sections[0].subSection[0].questions[this.seqNo].reportanswers[0].question_status;
      document.getElementById('previous').style.display = 'none';
    });
  }

  viewReport(){
    this.PdfObj = {
      "vgAuditNo": this.vgAuditNo,
      "sectionId": this.sectionId,
      "clientId": this.clientId
    }
    this.subscription = this.questionService.generatePDF(this.PdfObj).subscribe((res:Blob) => {
      var file = new Blob([res], { type: 'application/pdf' })
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    })
  }

  goToPreviousQuestion(){
    document.getElementById('next').innerText = 'Save & Next';
    if(this.sectionId === this.vgAuditDetails[0].section_id){
      if(this.organizationDiagnosisSeqNo === 0) {
        this.organizationDiagnosisSeqNo = this.questions.length - 1;
        this.subSectionNo -= 1;
      } else{
          this.organizationDiagnosisSeqNo -= 1;
      }
    } else if(this.sectionId === this.vgAuditDetails[1].section_id){
      if(this.levelIndex === 1){
        this.index = this.index - 1;
      } else if(this.levelIndex === 2){
        this.index = this.index - 1;
      } else if(this.levelIndex === 3){
        this.index = this.index - 1;
      } else {
        this.index -= 1;
      }
    } else if(this.sectionId === this.vgAuditDetails[2].section_id){
      if(this.performanceEvaluationSeqNo === 0){
        if(this.performanceEvaluationCounter !== 0) {
          this.performanceEvaluationCounter -= 1;
        }
      } else {
        this.performanceEvaluationSeqNo -= 1;
      }
    } else if(this.sectionId === this.vgAuditDetails[4].section_id){
      if(this.planOfActionSeqNo === 0) {
        this.planOfActionSeqNo = this.questions.length - 1;
        this.subSectionNo -= 1;
      } else{
        this.planOfActionSeqNo -= 1;
      }
    }

    this.onPrevious();
  }

  onPrevious(){
    let textareaForSectionThree: any = document.getElementById('textareaForSectionThree');
    document.querySelectorAll('#average-box')?.forEach((ele: any) => {
      ele.style.backgroundColor = '#D2D6DE';
    });
    document.querySelectorAll('#averag-box')?.forEach((ele: any) => {
      ele.style.backgroundColor = '#D2D6DE';
    });
    this.averageOfQuestion = '';
    this.totalAverage = '';

    if(this.sectionId === this.vgAuditDetails[0].section_id){

      const subSec = this.subSection[this.subSectionNo];
      this.questions = subSec.questions;
      if(this.organizationDiagnosisSeqNo < 2){
        document.getElementById('employee_name').style.display = 'block';
        // document.getElementById('employee_name_1').style.display = 'block';
      }
      this.subSectionName = subSec.sub_section_name;
      this.sequenceNoOfSubSection = subSec.sequence_number;
      if (this.organizationDiagnosisSeqNo < subSec.questions.length) {
        const questionObj = subSec.questions[this.organizationDiagnosisSeqNo];
        this.vgaReportForm.get('QuestionId').patchValue(questionObj.id);
        this.questionType = questionObj.question_type;
        this.question = questionObj.question;
        this.sequenceNoOfQuestion = questionObj.sequence_number;
        const reportAnswer = questionObj.reportanswers[0];
        this.txtAreaValue = reportAnswer.consultant_review || '';
        this.queId = questionObj.id;
        if(this.sectionId === this.vgAuditDetails[0].section_id){
          if(questionObj.id === reportAnswer.question_id){
            setTimeout(() => {
              let average = document.querySelector('#average');
              if(average){
                let parentElement = average.parentElement;
                parentElement.style.backgroundColor = 'darkgrey';
              }
            }, 100);
            this.averageOfQuestion = reportAnswer.question_average;
            this.totalAverage = reportAnswer.total_average;
          }
        }
      } else {
        if (this.subSectionNo < this.subSection.length - 1) {
          this.organizationDiagnosisSeqNo = subSec.questions.length - 1;
          if(this.subSectionNo !== 0) this.subSectionNo -= 1;
        }
      }
      this.subscription = this.reportAnswerService.getReportAnswersByQuestion(this.queId,this.vgAuditNo).subscribe((res: any) => {
        if(res){
          this.toastr.success('Consultant views submitted!')
          this.updatedConsultantReview = res.res[0].consultant_review;
          // this.statusValue = res.res[0].question_status;
          this.patchConsultantReview(this.updatedConsultantReview);
        }
      });
    } else if(this.sectionId === this.vgAuditDetails[1].section_id){
      const topLevel = this.topLevelArr;
      const managerialLevel = this.managerialLevelArr;
      const executiveLevel = this.executiveLevelArr;
      // console.log('this.index --- ', this.index)
      if(this.levelIndex === 1){
        this.topLevel = this.topLevelArr[this.index].name;
        topLevel[this.index].questions.map((employeeCompetency: any)=>{
          this.performanceHighImportanceLow = [];
          this.performanceLowImportanceLow = [];
          this.performanceHighImportanceHigh = [];
          this.performanceLowImportanceHigh = [];
          this.ansObj = {
            "questionId": employeeCompetency.id,
            "vgAuditNo": this.vgAuditNo,
            "clientEmployeeId": topLevel[this.index].id
          }
          this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
            let reportanswers = res.res;
            this.consulatantReview = reportanswers[0].consultant_review;
            // console.log('this.consulatantReview --- ', this.consulatantReview)

            this.topLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
              if(res?.res[0].question_id === employeeCompetency.id){
                var answersum = 0;
                reportanswers.map((ansValue: any) => {
                  answersum += parseInt(ansValue.answer_value);
                })
                this.average = answersum / 4;
                if(employeeCompetency.question === importanceOfCompetency.question){
                  if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                    this.performanceHighImportanceLow.push(employeeCompetency.question);
                  } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                    this.performanceLowImportanceLow.push(employeeCompetency.question);
                  } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                    this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                  } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                    this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                  }
                }
              }
            })
          })
        })
      } else if(this.levelIndex === 2){
        if(this.index === -1){
          this.levelIndex = 1;
          this.levels = 'Top Level';
          this.index = this.topLevelArr.length - 1;
          this.topLevel = this.topLevelArr[this.index].name;
          topLevel[this.index].questions.map((employeeCompetency: any)=>{
            this.performanceHighImportanceLow = [];
            this.performanceLowImportanceLow = [];
            this.performanceHighImportanceHigh = [];
            this.performanceLowImportanceHigh = [];
            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": topLevel[this.index].id
            }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;
              // console.log('this.consulatantReview --- ', this.consulatantReview)

              this.topLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / 4;
                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                    }
                  }
                }
              })
            })
          })
        } else{
          this.topLevel = this.managerialLevelArr[this.index].name;
          managerialLevel[this.index].questions.map((employeeCompetency: any)=>{
            this.performanceHighImportanceLow = [];
            this.performanceLowImportanceLow = [];
            this.performanceHighImportanceHigh = [];
            this.performanceLowImportanceHigh = [];
            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": managerialLevel[this.index].id
            }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;
              // console.log('this.consulatantReview --- ', this.consulatantReview)

              this.managerialLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / 4;
                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                    }
                  }
                }
              })
            })
          })
        }
      } else if(this.levelIndex === 3){
        if(this.index === -1){
          this.levelIndex = 2;
          this.levels = 'Managerial Level';
          this.index = this.managerialLevelArr.length - 1;
          this.topLevel = this.managerialLevelArr[this.index].name;
          managerialLevel[this.index].questions.map((employeeCompetency: any)=>{
            this.performanceHighImportanceLow = [];
            this.performanceLowImportanceLow = [];
            this.performanceHighImportanceHigh = [];
            this.performanceLowImportanceHigh = [];
            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": managerialLevel[this.index].id
            }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;
              console.log('this.consulatantReview --- ', this.consulatantReview)

              this.managerialLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / 4;
                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                    }
                  }
                }
              })
            })
          })
        } else{
          this.topLevel = this.executiveLevelArr[this.index].name;
          executiveLevel[this.index].questions.map((employeeCompetency: any)=>{
            this.performanceHighImportanceLow = [];
            this.performanceLowImportanceLow = [];
            this.performanceHighImportanceHigh = [];
            this.performanceLowImportanceHigh = [];
            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": executiveLevel[this.index].id
            }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;
              // console.log('this.consulatantReview --- ', this.consulatantReview)

              this.executiveLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / 4;
                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                    }
                  }
                }
              })
            })
          })
        }
      }
    } else if(this.sectionId === this.vgAuditDetails[2].section_id){
      if(this.getQuestions.length){
        const questions = this.getQuestions[this.performanceEvaluationSeqNo];
        this.getQuestionName = questions?.question;
        this.sequenceNoOfQuestion = questions.sequence_number;
        const reportAnswer = questions.reportanswers[0];
        const reportAnswers = questions.reportanswers;
          reportAnswers.filter((reportAnswersObj: any) => {
            if(reportAnswersObj.employee_id === this.employeeId[this.performanceEvaluationCounter]){
              if(this.employeePerformanceId[this.performanceEvaluationCounter] === reportAnswersObj.employee_performance_questions_id){
                setTimeout(() => {
                  textareaForSectionThree.value = reportAnswersObj.consultant_review || '';
                }, 600);
              }
            }
          });
        textareaForSectionThree.value = reportAnswer.consultant_review;
        if(questions.id === reportAnswer.question_id){
          if(this.performanceEvaluationSeqNo < 2){
            setTimeout(() => {
              this.employeeData.filter((employeeData: any) => {
                if(this.employeeName[this.performanceEvaluationCounter] === employeeData.clientEmployee.name){
                  if(this.employeePerformanceQuestion[this.performanceEvaluationCounter] === employeeData.employeePerformance.name){
                    if(this.getQuestions[this.performanceEvaluationSeqNo].id === employeeData.question_id){
                      this.averageOfQuestion = employeeData.question_average;
                    } else if (this.getQuestions[this.performanceEvaluationSeqNo].id !== employeeData.question_id){
                      this.totalAverage = employeeData.question_average;
                    }
                  }
                }
              });
              setTimeout(() => {
                let average = document.querySelector('#averag');
                if(average){
                  let parentElement = average.parentElement;
                  parentElement.style.backgroundColor = 'darkgrey';
                }
              }, 100);

            }, 500);
          } else {
            setTimeout(() => {
              let average = document.querySelector('#averag');
              if(average){
                let parentElement = average.parentElement;
                parentElement.style.backgroundColor = 'darkgrey';
              }
            }, 100);
            this.averageOfQuestion = reportAnswer.question_average;
            this.totalAverage = reportAnswer.total_average;
          }
        }
        if(this.performanceEvaluationCounter === 0 && this.sequenceNoOfQuestion === 1)
          document.getElementById('previous').style.display = 'none';
      }

      this.subscription = this.reportAnswerService.getReportAnswersByQuestion(this.getQuestionId,this.vgAuditNo).subscribe((res: any) => {
        if(res){
          this.toastr.success('Consultant views submitted!');
          res.res.filter((data: any) => {
            if(data.employee_id === this.employeeId[this.performanceEvaluationCounter]){
              if(this.employeePerformanceId[this.performanceEvaluationCounter] === data.employee_performance_questions_id){
                if(this.getQuestions[this.performanceEvaluationSeqNo].id === data.question_id){
                  setTimeout(() => {
                    this.updatedConsultantReview = data?.consultant_review;
                    // this.statusValue = res.res[0].question_status;
                    this.patchConsultantReview(this.updatedConsultantReview);
                  }, 600);
                }
              }
            }
          });
        }
      });
    } else if(this.sectionId === this.vgAuditDetails[4].section_id){

      const subSec = this.subSection[this.subSectionNo];
      // this.questions = subSec.questions;
      this.subSectionName = subSec.sub_section_name;
      this.sequenceNoOfSubSection = subSec.sequence_number;
      if (this.planOfActionSeqNo < subSec.questions.length) {
        const questionObj = subSec.questions[this.planOfActionSeqNo];
        this.questionIdForSection5 = questionObj.id;
        // this.vgaReportForm.get('QuestionId').patchValue(questionObj.id);
        this.questionType = questionObj.question_type;
        this.question = questionObj.question;
        this.sequenceNoOfQuestion = questionObj.sequence_number;
        // const reportAnswer = questionObj.reportanswers[0];
        // this.txtAreaValue = reportAnswer.consultant_review || '';
        // this.queId = questionObj.id;
        // if(this.sectionId === this.vgAuditDetails[0].section_id){
        //   if(questionObj.id === reportAnswer.question_id){
        //     setTimeout(() => {
        //       let average = document.querySelector('#average');
        //       if(average){
        //         let parentElement = average.parentElement;
        //         parentElement.style.backgroundColor = 'darkgrey';
        //       }
        //     }, 100);
        //     this.averageOfQuestion = reportAnswer.question_average;
        //     this.totalAverage = reportAnswer.total_average;
        //   }
        // }
      } else {
        if (this.subSectionNo < this.subSection.length - 1) {
          this.planOfActionSeqNo = subSec.questions.length - 1;
          if(this.subSectionNo !== 0) this.subSectionNo -= 1;
        }
      }
      this.reportAnswerService.getAnswerValues(this.questionIdForSection5, this.vgAuditNo).subscribe((res: any) => {
        console.log(`previous res - `, res)
        this.answersOfDefiniteDirection1 = res.res[0].answer_value.definiteDirection1;
        this.answersOfDefiniteDirection2= res.res[0].answer_value.definiteDirection2;
        this.answersOfDefiniteDirection3 = res.res[0].answer_value.definiteDirection3;
        this.demands = this.answersOfDefiniteDirection1?.demands;
        this.policies = this.answersOfDefiniteDirection1?.policies;
        this.supply = this.answersOfDefiniteDirection1?.supply;
        this.consultantReviewForOnClickOfNextForSection5 = res.res[0].consultant_review;
        // console.log(`this.consultantReviewForOnClickOfNextForSection5 - `, this.consultantReviewForOnClickOfNextForSection5)
        this.answersOfConcreteCommecement1 = res.res[0].answer_value?.concreteCommecement1;
        this.answersOfConcreteCommecement2 = res.res[0].answer_value?.concreteCommecement2;
        this.answersOfConcreteCommecement3 = res.res[0].answer_value?.concreteCommecement3;
        this.answersOfPurposefulPlanning1 = res.res[0].answer_value?.strategyManagementProcess;
        this.answersOfPurposefulPlanning2 = res.res[0].answer_value?.balanceScoreCard;
        this.answersOfPurposefulPlanning3 = res.res[0].answer_value?.strategicPlanningFramework;
        this.answersOfGrowthFrameOfStrategy = res.res[0].answer_value.strategic?.strategy;
        this.answersOfGrowthFrameOfSustainability = res.res[0].answer_value.strategic?.sustainability;
        this.answersOfGrowthFrameOfSolutions = res.res[0].answer_value.strategic?.solutions;
        this.answersOfGrowthFrameOfSystem = res.res[0].answer_value.managerial?.system;
        this.answersOfGrowthFrameOfStyleOfLeadership = res.res[0].answer_value.managerial?.styleOfLeadership;
        this.answersOfGrowthFrameOfSkills = res.res[0].answer_value.managerial?.skills;
        this.answersOfGrowthFrameOfStructure = res.res[0].answer_value.oprational?.structure;
        this.answersOfGrowthFrameOfSharedValues = res.res[0].answer_value.oprational?.sharedValues;
        this.answersOfGrowthFrameOfStandardOperationProducers = res.res[0].answer_value.oprational?.standardOperationProducers;
        this.answersOfGrowthTimeline = res.res[0].answer_value;
        this.answersOfStrategicActionDescriptions = res.res[0].answer_value?.strategicActionDescriptions;
        this.answersOfDeptResponsible = res.res[0].answer_value?.deptResponsible;
        this.answersOfdateTobegin = res.res[0].answer_value?.dateTobegin;
        this.answersOfDateTodue = res.res[0].answer_value?.dateTodue;
        this.answersOfResourcesRequired = res.res[0].answer_value?.resourcesRequired;
        this.answersOfDesiredOutcome = res.res[0].answer_value?.desiredOutcome;
        this.answersOfPotentialazards = res.res[0].answer_value?.potentialHazards;
        this.answersOfStrategicAction = res.res[0].answer_value?.strategicAction;
        this.answersOfStackHolders = res.res[0].answer_value?.stackHolders;
        this.answersOfConstraints = res.res[0].answer_value?.constraints;
        this.answersOfMetric = res.res[0].answer_value?.metric;
        this.answersOfPriority = res.res[0].answer_value?.priority;
        this.answersOfExecutionTimeline = res.res[0].answer_value?.executionTimeline;
      })
      // this.subscription = this.reportAnswerService.getReportAnswersByQuestion(this.queId,this.vgAuditNo).subscribe((res: any) => {
      //   if(res){
      //     this.toastr.success('Consultant views submitted!')
      //     this.updatedConsultantReview = res.res[0].consultant_review;
      //     // this.statusValue = res.res[0].question_status;
      //     this.patchConsultantReview(this.updatedConsultantReview);
      //   }
      // });
      if(this.subSectionNo === 0 && this.planOfActionSeqNo === 0){
        document.getElementById('previous').style.display = 'none';
      }
    }

  }

  goToNextQuestion() {
    if(this.sectionId === this.vgAuditDetails[0].section_id){
      const textareaForSectionOne: any = document.getElementById('textareaForSectionOne');
      this.txtAreaValue = textareaForSectionOne.value;
      this.consultantReviewFormForFirstSection = {
        'UserId': this.userObj?.id,
        'QuestionId': this.queId,
        'consultantReview': this.txtAreaValue,
        'vgaAuditNumber': this.vgAuditNo,
        // 'status': this.statusValue
      }
    } else if(this.sectionId === this.vgAuditDetails[1].section_id){
      const textareaForSectionTwo: any = document.getElementById('textareaForSectionTwo');
      this.consultantViewForCompetencyMapping = textareaForSectionTwo.value;
      if(this.levels === 'Top Level'){
        this.consultantReviewFormForSecondSection = {
          'UserId': this.userObj?.id,
          'SectionId': this.sectionId,
          'QuestionId': this.getQuestionId,
          'consultantReview': this.consultantViewForCompetencyMapping,
          'vgaAuditNumber': this.vgAuditNo,
          'clientEmployeeId': this.topLevelArr[this.index].id,
          // 'status': this.statusValue
        }
      } else if(this.levels === 'Managerial Level'){
        this.consultantReviewFormForSecondSection = {
          'UserId': this.userObj?.id,
          'SectionId': this.sectionId,
          'QuestionId': this.getQuestionId,
          'consultantReview': this.consultantViewForCompetencyMapping,
          'vgaAuditNumber': this.vgAuditNo,
          'clientEmployeeId': this.competencyMapping[0].managerialLevel[this.index].id,
          // 'status': this.statusValue
        }
      } else if(this.levels === 'Executive Level'){
        this.consultantReviewFormForSecondSection = {
          'UserId': this.userObj?.id,
          'SectionId': this.sectionId,
          'QuestionId': this.getQuestionId,
          'consultantReview': this.consultantViewForCompetencyMapping,
          'vgaAuditNumber': this.vgAuditNo,
          'clientEmployeeId': this.competencyMapping[0].executiveLevel[this.index].id,
          // 'status': this.statusValue
        }
      }
    } else if (this.sectionId === this.vgAuditDetails[2].section_id){
      const textareaForSectionThree: any = document.getElementById('textareaForSectionThree');
      this.consultantViewForPerformanceEvaluation = textareaForSectionThree.value;
      if(this.performanceEvaluationSeqNo < 2) {
        this.consultantReviewFormForThirdSection = {
          'UserId': this.userObj?.id,
          'QuestionId': this.getQuestionId,
          'consultantReview': this.consultantViewForPerformanceEvaluation,
          'vgaAuditNumber': this.vgAuditNo,
          'employeeId': this.employeeId[this.performanceEvaluationCounter],
          'employeePerformanceQuestionId': this.employeePerformanceId[this.performanceEvaluationCounter]
          // 'status': this.statusValue
        }
      } else {
        this.consultantReviewFormForThirdSection = {
          'UserId': this.userObj?.id,
          'QuestionId': this.getQuestionId,
          'consultantReview': this.consultantViewForPerformanceEvaluation,
          'vgaAuditNumber': this.vgAuditNo,
          // 'status': this.statusValue
        }
      }
    } else if (this.sectionId === this.vgAuditDetails[4].section_id){
      const consultant1ForSectionFive: any = document.getElementById('consultant1ForSectionFive');
      this.c1 = consultant1ForSectionFive?.value;
      const consultant2ForSectionFive: any = document.getElementById('consultant2ForSectionFive');
      this.c2 = consultant2ForSectionFive?.value;
      const consultant3ForSectionFive: any = document.getElementById('consultant3ForSectionFive');
      this.c3 = consultant3ForSectionFive?.value;
      this.consultantReviewFormForFifthSection = {
        'UserId': this.userObj?.id,
        'ClientId': this.clientId,
        'QuestionId': this.questionIdForSection5,
        'consultantReview': {'A': this.c1, 'B': this.c2, 'C': this.c3},
        'vgaAuditNumber': this.vgAuditNo,
        'employeeId': this.employeeId[this.performanceEvaluationCounter],
        'employeePerformanceQuestionId': this.employeePerformanceId[this.performanceEvaluationCounter]
        // 'status': this.statusValue
      }
    }
    if(this.vgaReportForm.valid){
      if(this.sectionId === this.vgAuditDetails[0].section_id){
        this.subscription = this.reportAnswerService.reportAnswerValue({ "Answer": this.consultantReviewFormForFirstSection }).subscribe((res: any) => {
          if (res) {
            if((this.organizationDiagnosisSeqNo === this.questions.length -1 )  && (this.subSectionNo === this.subSection.length - 1)) null
            else {
              document.getElementById('next').innerText = 'Save & Next';
              this.organizationDiagnosisSeqNo += 1;
              if(this.organizationDiagnosisSeqNo > 0){
                document.getElementById('previous').style.display = 'inline';
              }
              this.onSubmitAndNext();
            }
          }
        },
        (error: HttpErrorResponse) => {
          if (error) {
            this.toastr.error('Something went wrong!')
          }
        });
      } else if (this.sectionId === this.vgAuditDetails[1].section_id) {
        this.subscription = this.reportAnswerService.reportAnswerValue({ "Answer": this.consultantReviewFormForSecondSection}).subscribe((res: any) => {
          console.log(`res --- `, res)
          if(res){
            let counter = 0;
            if(counter === this.clientEmployeeOfTopLevel.length -1 )  null
            else {
                this.competencyMappingCounter += 1;
                if(this.competencyMappingCounter === this.clientEmployeeOfTopLevel.length){
                  counter += 1;
                }
                if(this.competencyMappingCounter > 0){
                  document.getElementById('previous').style.display = 'inline';
                }
                this.onSubmitAndNext();
            }
          }
        },
        (error: HttpErrorResponse) => {
          if (error) {
            this.toastr.error('Something went wrong!')
          }
        });
    } else if (this.sectionId === this.vgAuditDetails[2].section_id) {
        this.subscription = this.reportAnswerService.employeePerformanceValue({ "Answer": this.consultantReviewFormForThirdSection}).subscribe((res: any) => {
          if(res){
            if(this.performanceEvaluationSeqNo === this.employeeId.length -1 )  null
            else {
                this.performanceEvaluationCounter += 1;
                if(this.performanceEvaluationCounter === this.employeeName.length){
                  this.performanceEvaluationSeqNo += 1;
                  this.performanceEvaluationCounter = 0;
                }
                if(this.performanceEvaluationCounter > 0){
                  document.getElementById('previous').style.display = 'inline';
                }
                this.onSubmitAndNext();
            }
          }
        },
        (error: HttpErrorResponse) => {
          if (error) {
            this.toastr.error('Something went wrong!')
          }
        });
    } else if (this.sectionId === this.vgAuditDetails[4].section_id) {
        this.reportAnswerService.addReportAnswerValue(this.consultantReviewFormForFifthSection).subscribe((res: any)=>{
          if(res){
            this.toastr.success('Consultant Views submitted!')
          }
          document.getElementById('next').innerText = 'Save & Next';
          this.planOfActionSeqNo += 1;
          if(this.planOfActionSeqNo > 0){
            document.getElementById('previous').style.display = 'inline';
          }
          this.onSubmitAndNext();
        })
      }
    }
  }

  onSubmitAndNext(){
    let textareaForSectionThree: any = document.getElementById('textareaForSectionThree');
    document.querySelectorAll('#average-box')?.forEach((ele: any) => {
      ele.style.backgroundColor = '#D2D6DE';
    });
    document.querySelectorAll('#averag-box')?.forEach((ele: any) => {
      ele.style.backgroundColor = '#D2D6DE';
    });
    this.totalAverage = '';
    this.txtAreaValue = '';
    this.averageOfQuestion = '';
    this.consultantViewForPerformanceEvaluation = '';

    //Section-1
    if(this.sectionId === this.vgAuditDetails[0].section_id){
      const subSec = this.subSection[this.subSectionNo];
      this.questions = subSec.questions;
      this.subSectionName = subSec.sub_section_name;
      this.sequenceNoOfSubSection = subSec.sequence_number;
      if (this.organizationDiagnosisSeqNo < subSec.questions.length) {
        const questionObj = subSec.questions[this.organizationDiagnosisSeqNo];
        this.vgaReportForm.get('QuestionId').patchValue(questionObj.id);
        this.questionType = questionObj.question_type;
        this.question = questionObj.question;
        this.sequenceNoOfQuestion = questionObj.sequence_number;
        const reportAnswer = questionObj.reportanswers[0];
        this.txtAreaValue = reportAnswer.consultant_review || '';
        this.queId = questionObj.id;
        if(questionObj.id === reportAnswer.question_id){
          setTimeout(() => {
            let average = document.querySelector('#average');
            if(average){
              let parentElement = average.parentElement;
              parentElement.style.backgroundColor = 'darkgrey';
            }
          }, 100);
          this.averageOfQuestion = reportAnswer.question_average;
          this.totalAverage = reportAnswer.total_average;
        }
      } else {
        if (this.subSectionNo < this.subSection.length - 1) {
          this.organizationDiagnosisSeqNo = 0;
          this.subSectionNo += 1;
          this.onSubmitAndNext();
        }
      }
      if((this.organizationDiagnosisSeqNo === subSec.questions.length - 1 )&&(this.subSectionNo === this.subSection.length - 1))
        document.getElementById('next').innerText = 'Save';

        this.subscription = this.reportAnswerService.getReportAnswersByQuestion(this.queId,this.vgAuditNo).subscribe((res: any) => {
        if(res){
          this.toastr.success('Consultant views submitted!')
          this.updatedConsultantReview = res.res[0].consultant_review;
          // this.statusValue = res.res[0].question_status;
          this.patchConsultantReview(this.updatedConsultantReview);
        }
      });
    }

    // Section-2
    if(this.sectionId === this.vgAuditDetails[1].section_id){
      setTimeout(() => {
      const topLevel = this.topLevelArr;
      console.log('topLevel --- ', topLevel)
      const managerialLevel = this.managerialLevelArr;
      const executiveLevel = this.executiveLevelArr;
      this.index = topLevel.findIndex(x => x.name === this.topLevel);
      if(this.levels === "Top Level"){
        this.index++;
        if(topLevel.length > this.index){
          this.index = this.index;
          this.topLevel = topLevel[this.index].name;

          topLevel[this.index].questions.map((employeeCompetency: any)=>{
            this.performanceHighImportanceLow = [];
            this.performanceLowImportanceLow = [];
            this.performanceHighImportanceHigh = [];
            this.performanceLowImportanceHigh = [];

            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": topLevel[this.index].id
            }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;
              console.log('this.consulatantReview --- ', this.consulatantReview)

              this.topLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / 4;

                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                      console.log('performanceHighImportanceLow  - ', this.performanceHighImportanceLow)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                      console.log('performanceLowImportanceLow - ', this.performanceLowImportanceLow)
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceHighImportanceHigh - ', this.performanceHighImportanceHigh)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceLowImportanceLow - ', this.performanceLowImportanceHigh)
                    }
                  }
                }
              })
            })
          })
        }
      }
      if(topLevel.length === this.index || this.levels == "Managerial Level"){
        this.index = managerialLevel.findIndex(x => x.name === this.topLevel);
        this.levelIndex = 2;
        this.levels = "Managerial Level";
        this.index++;
        console.log('this.topLevel --- ', this.topLevel)
        console.log('this.index --- ', this.index)
        if(managerialLevel.length > this.index){
          this.index = this.index;
          this.topLevel = managerialLevel[this.index].name;

          managerialLevel[this.index].questions.map((employeeCompetency: any)=>{
            this.performanceHighImportanceLow = [];
            this.performanceLowImportanceLow = [];
            this.performanceHighImportanceHigh = [];
            this.performanceLowImportanceHigh = [];

            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": managerialLevel[this.index].id
            }
            console.log('this.ansObj --- ', this.ansObj)
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;
              console.log('this.consulatantReview --- ', this.consulatantReview)

              this.managerialLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / res.res.length;

                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                      console.log('performanceHighImportanceLow  - ', this.performanceHighImportanceLow)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                      console.log('performanceLowImportanceLow - ', this.performanceLowImportanceLow)
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceHighImportanceHigh - ', this.performanceHighImportanceHigh)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceLowImportanceHigh - ', this.performanceLowImportanceHigh)
                    }
                  }
                }
              })
            })
          })
        }
      }
      if(managerialLevel.length === this.index || this.levels == "Executive Level"){
        this.index = executiveLevel.findIndex(x => x.name === this.topLevel);
        this.levelIndex = 3;
        this.levels = "Executive Level";
        this.index++;
        if(executiveLevel.length > this.index){
          this.index = this.index;
          this.topLevel = executiveLevel[this.index].name;

          executiveLevel[this.index].questions.map((employeeCompetency: any)=>{
            this.performanceHighImportanceLow = [];
            this.performanceLowImportanceLow = [];
            this.performanceHighImportanceHigh = [];
            this.performanceLowImportanceHigh = [];

            this.ansObj = {
              "questionId": employeeCompetency.id,
              "vgAuditNo": this.vgAuditNo,
              "clientEmployeeId": executiveLevel[this.index].id
            }
            this.questionService.getReportAnswers(this.ansObj).subscribe((res: any) => {
              let reportanswers = res.res;
              this.consulatantReview = reportanswers[0].consultant_review;
              console.log('this.consulatantReview --- ', this.consulatantReview)

              this.executiveLevelQuestionsOfImportance.map((importanceOfCompetency:any)=>{
                if(res?.res[0].question_id === employeeCompetency.id){
                  var answersum = 0;
                  reportanswers.map((ansValue: any) => {
                    answersum += parseInt(ansValue.answer_value);
                  })
                  this.average = answersum / res.res.length;

                  if(employeeCompetency.question === importanceOfCompetency.question){
                    if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceHighImportanceLow.push(employeeCompetency.question);
                      console.log('performanceHighImportanceLow  - ', this.performanceHighImportanceLow)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average < 3){
                      this.performanceLowImportanceLow.push(employeeCompetency.question);
                      console.log('performanceLowImportanceLow - ', this.performanceLowImportanceLow)
                    } else if(this.average >= 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceHighImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceHighImportanceHigh - ', this.performanceHighImportanceHigh)
                    } else if(this.average < 3 && importanceOfCompetency.reportanswers[0].question_average >= 3){
                      this.performanceLowImportanceHigh.push(importanceOfCompetency.question);
                      console.log('performanceLowImportanceHigh - ', this.performanceLowImportanceHigh)
                    }
                  }
                }
              })
            })
          })
        }
        if(executiveLevel.length - 1 === this.index){
          document.getElementById('next').innerText = 'Save';
        }
      }
      }, 500)
    }

    //Section - 3
    if(this.sectionId === this.vgAuditDetails[2].section_id){
      if(this.performanceEvaluationSeqNo > 1) {
        document.getElementById('employee_name').style.display = 'none';
        document.getElementById('employee_name_1').style.display = 'none';
      }

      let questions;
      if(this.performanceEvaluationSeqNo < this.getQuestions.length){
        questions = this.getQuestions[this.performanceEvaluationSeqNo];
        this.getQuestionId = questions?.id;
        this.getQuestionName = questions?.question;
        this.sequenceNoOfQuestion = questions.sequence_number;
        this.subQuestions = questions?.subquestions;
        const reportAnswer = questions.reportanswers[0];
        const reportAnswers = questions.reportanswers;
        reportAnswers.filter((reportAnswersObj: any) => {
          if(reportAnswersObj.employee_id === this.employeeId[this.performanceEvaluationCounter]){
            if(this.employeePerformanceId[this.performanceEvaluationCounter] === reportAnswersObj.employee_performance_questions_id){
              setTimeout(() => {
                textareaForSectionThree.value = reportAnswersObj.consultant_review || '';
              }, 400);
            }
          }else {
            if(reportAnswersObj.sub_questions_id === this.subQuestions[this.performanceEvaluationCounter]) {
              setTimeout(() => {
                textareaForSectionThree.value = reportAnswersObj.consultant_review || '';
              }, 400);
            }
          }
        });
          if(questions.id === reportAnswer.question_id){
            if(this.performanceEvaluationSeqNo < 2){
              setTimeout(() => {
                this.employeeData.filter((employeeData: any) => {
                  if(this.employeeName[this.performanceEvaluationCounter] === employeeData.clientEmployee.name){
                    if(this.employeePerformanceQuestion[this.performanceEvaluationCounter] === employeeData.employeePerformance.name){
                      if(this.getQuestions[this.performanceEvaluationSeqNo].id === employeeData.question_id){
                        this.averageOfQuestion = employeeData.question_average;
                      } else if (this.getQuestions[this.performanceEvaluationSeqNo].id !== employeeData.question_id){
                        this.totalAverage = employeeData.question_average;
                      }
                    }
                  }
                });
                setTimeout(() => {
                  let average = document.querySelector('#averag');
                  if(average){
                    let parentElement = average.parentElement;
                    parentElement.style.backgroundColor = 'darkgrey';
                  }
                }, 100);

              }, 500);
            } else {
              setTimeout(() => {
                let average = document.querySelector('#averag');
                if(average){
                  let parentElement = average.parentElement;
                  parentElement.style.backgroundColor = 'darkgrey';
                }
              }, 100);
              this.averageOfQuestion = reportAnswer.question_average;
              this.totalAverage = reportAnswer.total_average;
              this.performanceEvaluationSeqNo += 1;
            }
          }
      }

      if(this.performanceEvaluationSeqNo === this.getQuestions.length - 1)
        document.getElementById('next').innerText = 'Save';

        this.subscription = this.reportAnswerService.getReportAnswersByQuestion(this.getQuestionId,this.vgAuditNo).subscribe((res: any) => {
        if(res){
          this.toastr.success('Consultant views submitted!');
          res.res.filter((data: any) => {
            if(data.employee_id === this.employeeId[this.performanceEvaluationCounter]){
              if(this.employeePerformanceId[this.performanceEvaluationCounter] === data.employee_performance_questions_id){
                if(this.getQuestions[this.performanceEvaluationSeqNo].id === data.question_id){
                  setTimeout(() => {
                    this.updatedConsultantReview = data?.consultant_review;
                    // this.statusValue = data[0].question_status;
                    this.patchConsultantReview(this.updatedConsultantReview);
                  }, 400);
                }
              }
            }else {
              if(this.getQuestions[this.performanceEvaluationSeqNo].id === data.question_id){
                setTimeout(() => {
                  this.updatedConsultantReview = data?.consultant_review;
                  // this.statusValue = data[0].question_status;
                  this.patchConsultantReview(this.updatedConsultantReview);
                }, 400);
              }
            }
          })
        }
      });
    }

    // Section-5
    if(this.sectionId === this.vgAuditDetails[4].section_id){
    const subSec = this.subSection[this.subSectionNo];
    this.questions = subSec.questions;
    this.subSectionName = subSec.sub_section_name;
    this.sequenceNoOfSubSection = subSec.sequence_number;
    if (this.planOfActionSeqNo < subSec.questions.length) {
      const questionObj = subSec.questions[this.planOfActionSeqNo];
      console.log(questionObj)
      // this.vgaReportForm.get('QuestionId').patchValue(questionObj.id);
      this.questionType = questionObj.question_type;
      this.question = questionObj.question;
      this.questionIdForSection5 = questionObj.id;
      this.sequenceNoOfQuestion = questionObj.sequence_number;
    } else {
      if (this.subSectionNo < this.subSection.length - 1) {
        this.planOfActionSeqNo = 0;
        this.subSectionNo += 1;
        this.onSubmitAndNext();
      }
    }
    setTimeout(() => {
      this.reportAnswerService.getAnswerValues(this.questionIdForSection5, this.vgAuditNo).subscribe((res: any) => {
        console.log(`next res - `, res)
        this.answersOfDefiniteDirection1 = res.res[0].answer_value.definiteDirection1;
        this.answersOfDefiniteDirection2= res.res[0].answer_value.definiteDirection2;
        this.answersOfDefiniteDirection3 = res.res[0].answer_value.definiteDirection3;
        this.demands = this.answersOfDefiniteDirection1?.demands;
        this.policies = this.answersOfDefiniteDirection1?.policies;
        this.supply = this.answersOfDefiniteDirection1?.supply;
        this.consultantReviewForOnClickOfNextForSection5 = res.res[0].consultant_review;
        // console.log(`this.consultantReviewForOnClickOfNextForSection5 - `, this.consultantReviewForOnClickOfNextForSection5)
        this.answersOfConcreteCommecement1 = res.res[0].answer_value?.concreteCommecement1;
        this.answersOfConcreteCommecement2 = res.res[0].answer_value?.concreteCommecement2;
        this.answersOfConcreteCommecement3 = res.res[0].answer_value?.concreteCommecement3;
        this.answersOfPurposefulPlanning1 = res.res[0].answer_value?.strategyManagementProcess;
        this.answersOfPurposefulPlanning2 = res.res[0].answer_value?.balanceScoreCard;
        this.answersOfPurposefulPlanning3 = res.res[0].answer_value?.strategicPlanningFramework;
        this.answersOfGrowthFrameOfStrategy = res.res[0].answer_value.strategic?.strategy;
        this.answersOfGrowthFrameOfSustainability = res.res[0].answer_value.strategic?.sustainability;
        this.answersOfGrowthFrameOfSolutions = res.res[0].answer_value.strategic?.solutions;
        this.answersOfGrowthFrameOfSystem = res.res[0].answer_value.managerial?.system;
        this.answersOfGrowthFrameOfStyleOfLeadership = res.res[0].answer_value.managerial?.styleOfLeadership;
        this.answersOfGrowthFrameOfSkills = res.res[0].answer_value.managerial?.skills;
        this.answersOfGrowthFrameOfStructure = res.res[0].answer_value.oprational?.structure;
        this.answersOfGrowthFrameOfSharedValues = res.res[0].answer_value.oprational?.sharedValues;
        this.answersOfGrowthFrameOfStandardOperationProducers = res.res[0].answer_value.oprational?.standardOperationProducers;
        this.answersOfGrowthTimeline = res.res[0].answer_value;
        this.answersOfStrategicActionDescriptions = res.res[0].answer_value?.strategicActionDescriptions;
        this.answersOfDeptResponsible = res.res[0].answer_value?.deptResponsible;
        this.answersOfdateTobegin = res.res[0].answer_value?.dateTobegin;
        this.answersOfDateTodue = res.res[0].answer_value?.dateTodue;
        this.answersOfResourcesRequired = res.res[0].answer_value?.resourcesRequired;
        this.answersOfDesiredOutcome = res.res[0].answer_value?.desiredOutcome;
        this.answersOfPotentialazards = res.res[0].answer_value?.potentialHazards;
        this.answersOfStrategicAction = res.res[0].answer_value?.strategicAction;
        this.answersOfStackHolders = res.res[0].answer_value?.stackHolders;
        this.answersOfConstraints = res.res[0].answer_value?.constraints;
        this.answersOfMetric = res.res[0].answer_value?.metric;
        this.answersOfPriority = res.res[0].answer_value?.priority;
        this.answersOfExecutionTimeline = res.res[0].answer_value?.executionTimeline;
      })
    },1000)

    if(this.sequenceNoOfSubSection === this.subSection.length){
      console.log(subSec.questions.length);
      if(this.planOfActionSeqNo === 2){
        document.getElementById('next').innerText = 'Save';
      }
    }

      //   this.subscription = this.reportAnswerService.getReportAnswersByQuestion(this.queId,this.vgAuditNo).subscribe((res: any) => {
      //   if(res){
      //     this.toastr.success('Consultant views submitted!')
      //     this.updatedConsultantReview = res.res[0].consultant_review;
      //     // this.statusValue = res.res[0].question_status;
      //     this.patchConsultantReview(this.updatedConsultantReview);
      //   }
      // });
    }
  }

  patchConsultantReview(value: any){
    if(this.sectionId === this.vgAuditDetails[0].section_id){
      const textareaForSectionOne: any = document.getElementById('textareaForSectionOne');
      textareaForSectionOne.value = value;
    } else if (this.sectionId === this.vgAuditDetails[2].section_id){
        const textareaForSectionThree: any = document.getElementById('textareaForSectionThree');
        textareaForSectionThree.value = value;
    }
  }

  getEmployeeListing(){
    this.subscription = this.employeePerformanceService.getAllEmployeesListing().subscribe((res: any)=>{
      if(res){
        res.res.map((data: any) => {
          this.employeeData.push(data);
          let employeeData = data.clientEmployee;
          if(data.question_id === this.getQuestions[this.performanceEvaluationSeqNo].id) {
            let employeePerformance: any = data.employeePerformance;
            this.employeePerformanceId.push(employeePerformance.id);
            this.employeeId.push(employeeData.id);
            this.employeePerformanceQuestion.push(employeePerformance.name);
            this.employeeName.push(employeeData.name);
          }
        });
      }
    });
  }

  }
