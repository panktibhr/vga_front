import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchVgAuditComponent } from './search-vg-audit.component';

describe('SearchVgAuditComponent', () => {
  let component: SearchVgAuditComponent;
  let fixture: ComponentFixture<SearchVgAuditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchVgAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchVgAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
