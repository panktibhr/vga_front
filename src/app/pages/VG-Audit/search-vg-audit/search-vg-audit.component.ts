import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { vgAudit } from 'src/app/shared/models/vg-audit';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
import { VgAuditService } from 'src/app/shared/services/vg-audit/vg-audit.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-search-vg-audit',
  templateUrl: './search-vg-audit.component.html',
  styleUrls: ['./search-vg-audit.component.scss']
})
export class SearchVgAuditComponent implements OnInit, OnDestroy {
  userObj: any;
  roleType: any;
  section1: any;
  section2: any;
  section3: any;
  section4: any;
  section5: any;
  vgAudit:any = [];
  vgAuditsForDropDown: any = [];
  _vgAudits : vgAudit;
  subscription: Subscription;
  statusFromUrl: any;
  clientId: any;
  consultantId: any;
  vgAuditsOfClientsByConsultant: any = [];
  vgAuditsOfClientsByUser: any = [];
  vgAuditsOfClientsByUserForDropDown: any = [];

  constructor(private router: Router, private vgAuditService: VgAuditService, private sectionService: SectionMasterService, private questionService: QuestionMasterService,
    private toastr: ToastrService, private actRoute: ActivatedRoute) {
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
    if(this.roleType === 'Consultant'){
      this.consultantId = CryptoJS.AES.decrypt(localStorage.getItem('ConsultantId'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8);
    }
    this._vgAudits = new vgAudit();
    this.statusFromUrl = this.actRoute.snapshot.params.reports;
    this.clientId = sessionStorage.getItem('clientId');
  }

  ngOnInit(): void {
    if(this.router.url === '/pages/VGAudit/SearchVGAudit'){
      this.getVgAudits(this._vgAudits);
      this.getAllVgAudits(this._vgAudits);
      this.getAllSections();
    } else {
      this.getVgAudits(this._vgAudits);
      this.getAllSections();
    }
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  navigateToValueGrowthAuditReport(vgAuditNo){
    let averageObj:any = {
      'sectionId': this.section1,
      'vgAuditNo': vgAuditNo
    }
    this.questionService.postAverageOfQuestion(averageObj).subscribe((res: any)=>{
      if(res){
        this.router.navigate(['/pages/VGAudit/ValueGrowthAuditReport', vgAuditNo]);
      }
    }, (error: HttpErrorResponse) => {
      if (error) {
        this.toastr.error('Please wait! Report is not submitted yet!')
      }
    });
  }

  getVgAudits(vgAuditObj){
    if(this.roleType === 'Client'){
      this.subscription = this.vgAuditService.getAllVgAudits(this.clientId, vgAuditObj).subscribe((res:any) => {
      if(this.router.url === '/pages/VGAudit/SearchVGAudit'){
        this.vgAudit = res.res;
      } else {
        res.res.filter((vgReports: any) => {
          if(vgReports.status === this.statusFromUrl){
            this.vgAudit.push(vgReports);
          }
        });
      }
      });
    } else if(this.roleType === 'Consultant'){
      this.subscription = this.vgAuditService.getVgAuditByConsultantId(this.consultantId).subscribe((res: any) => {
        this.vgAuditsOfClientsByConsultant = res.res;
      });
    } else if(this.roleType === 'Super Admin'){
      this.subscription = this.vgAuditService.getVgaAuditReportsOfClientsByUserId(this._vgAudits).subscribe((res: any) => {
        this.vgAuditsOfClientsByUser = res.res;
      });
    }
  }

  getAllVgAudits(vgAuditObj){
    if(this.roleType === 'Client'){
      this.subscription = this.vgAuditService.getAllVgAudits(this.clientId, vgAuditObj).subscribe((res:any) => {
        this.vgAuditsForDropDown = res.res;
      });
    } else if(this.roleType === 'Consultant'){
      this.subscription = this.vgAuditService.getVgAuditByConsultantId(this.consultantId).subscribe((res: any) => {
        this.vgAuditsOfClientsByConsultant = res.res;
      });
    } else if(this.roleType === 'Super Admin'){
      this.subscription = this.vgAuditService.getVgaAuditReportsOfClientsByUserId(this._vgAudits).subscribe((res: any) => {
        this.vgAuditsOfClientsByUserForDropDown = res.res;
      });
    }
  }

  navigateToEditVGAuditReport(seq_num){
    this.router.navigate(['/pages/VGAudit/VGAuditReport/', seq_num]);
  }

  onChange(searchVgAuditNo){
    this._vgAudits.vgaAuditNo = searchVgAuditNo;
    this.getVgAudits(this._vgAudits);
  }

  onSearch(searchFromDate, searchToDate, searchVgAuditNo){
    this._vgAudits.fromDate = searchFromDate;
    this._vgAudits.toDate = searchToDate;
    this._vgAudits.vgaAuditNo = searchVgAuditNo;
    this.getVgAudits(this._vgAudits);
  }

  onClear(clearFromDate, cleartoDate, clearVgAuditNo){
    this._vgAudits.fromDate = clearFromDate;
    this._vgAudits.toDate = cleartoDate;
    this._vgAudits.vgaAuditNo = clearVgAuditNo;
    this.getVgAudits(this._vgAudits);
  }

  getAllSections(){
    this.sectionService.getAllSec().subscribe((res: any) => {
      res.res.map((res: any)=>{
        if(res.sequence_number === 1){
          this.section1 = res.id;
        } else if(res.sequence_number === 2){
          this.section2 = res.id;
        } else if(res.sequence_number === 3){
          this.section3 = res.id;
        } else if(res.sequence_number === 4){
          this.section4 = res.id;
        } else if(res.sequence_number === 5){
          this.section5 = res.id;
        }
      })
    })
  }

  viewReport(clientId, sectionId, vgAuditNo){
    let PdfObj = {
      "vgAuditNo": vgAuditNo,
      "sectionId": sectionId,
      "clientId": clientId
    }
    this.questionService.generatePDF(PdfObj).subscribe((res:Blob) => {
      var file = new Blob([res], { type: 'application/pdf' })
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, (error) => {
      if(error){
        this.toastr.error('Report is not submitted yet!!');
      }
    })
  }

}
