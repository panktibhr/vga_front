import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVgAuditComponent } from './edit-vg-audit.component';

describe('EditVgAuditComponent', () => {
  let component: EditVgAuditComponent;
  let fixture: ComponentFixture<EditVgAuditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVgAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVgAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
