import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/services/data.service';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { SubSectionService } from 'src/app/shared/services/sub-section/sub-section.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-vg-audit',
  templateUrl: './edit-vg-audit.component.html',
  styleUrls: ['./edit-vg-audit.component.scss']
})
export class EditVgAuditComponent implements OnInit, OnDestroy {

  getSectionsOfVgAudit: any = [];
  vgAuditNumber: any;
  secSubscription: Subscription;
  allSecSubscription: Subscription;
  getVGAuditSubscription: Subscription;
  vgAudit: any;
  userObj: any;
  roleType: any;
  vgAuditDetailsFromLocalStorage: any;
  vgAuditDetails: any;
  statusColor: any;
  secColorArr: any = [];
  auditDetails: any = [];
  sections: any;
  clientName: any;
  sectionsOfVgReport: any = [];
  sectionDetails: any;
  auditDetailsLength: number = 0;
  auditDetailsCounter: number = 1;
  section2: any;
  clientId: string;

  constructor(private router: Router, private actRoute: ActivatedRoute, private location: Location, private subSectionService: SubSectionService,
    private questionService: QuestionMasterService, private toastr: ToastrService) {
    this.vgAuditNumber = this.actRoute.snapshot.params.id;
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
    this.vgAuditDetailsFromLocalStorage = CryptoJS.AES.decrypt(localStorage.getItem('vgAuditDetails'), 'secret key 123').toString(CryptoJS.enc.Utf8);
    this.clientId = localStorage.getItem('clientId')
    console.log(JSON.parse(this.clientId))
  }

  ngOnInit(): void {
    this.getVgAuditDetails();
    this.getSectionDetailsOfVgAudit();
  }

  ngOnDestroy(){
    this.secSubscription?.unsubscribe();
    this.allSecSubscription?.unsubscribe();
    this.getVGAuditSubscription?.unsubscribe();
  }

  async getAuditDetails(){
    let vgAuditNo = {
      'vgAuditNo' : this.vgAudit.vga_audit_number,
      'clientId': this.auditDetails[this.auditDetailsCounter].client_id,
      'clientEmployeeId': ''
    }
    console.log(vgAuditNo)
    this.questionService.postVgAuditReportObj(this.auditDetails[this.auditDetailsCounter].section_id, vgAuditNo).subscribe(async (res: any) =>{
      console.log(res)
      if(res.res.sections[0].sequence_number === 2){
        this.section2 = res.res.sections[0];
      }
      if(res.success === true){
        let sectionName = res.res.sections[0].section_name;
        this.toastr.success(`${sectionName} get successfully!`)
        this.sections = res.res.sections[0];
        this.sectionsOfVgReport.push(this.sections);
        this.sectionsOfVgReport.map((sec: any)=>{
          if(sec.sequence_number !== 2){
            let statusColor = sec.sectionColor;
            setTimeout(() => {
              if(statusColor === 'green'){
                document.getElementById(sec.id).style.backgroundColor = '#B6E719';
              } else if(statusColor === 'red'){
                document.getElementById(sec.id).style.backgroundColor = '#FF8022';
              } else{
                document.getElementById(sec.id).style.backgroundColor = '#FFCA06';
              }
            },500);
          }
        });
        if ( this.auditDetailsCounter !== this.auditDetailsLength - 1 ) {
          this.auditDetailsCounter++;
          await this.getAuditDetails();
          if(this.section2){
            this.getStatusOfSubSection();
          }
        }
      }
    })
  }

  getSectionDetailsOfVgAudit = async () => {
    this.auditDetailsLength = this.auditDetails.length;
    this.auditDetailsCounter = 0;
    await this.getAuditDetails();
  }

  getVgAuditDetails(){
    this.vgAuditDetails = JSON.parse(this.vgAuditDetailsFromLocalStorage);
    this.vgAudit = (this.vgAuditDetails.audits) || (this.vgAuditDetails.res.audits);
    this.auditDetails = (this.vgAuditDetails.auditsDetails) || (this.vgAuditDetails.res.auditsDetails);
  }

  onClickOfSection(id){
    let subSection;
    this.sectionsOfVgReport.map((res: any)=>{
      subSection = res.subSection;
      if(id === res.id){
        this.sectionDetails = res;
        var encryptVgAuditDetails = CryptoJS.AES.encrypt(JSON.stringify(this.sectionDetails), 'secret key 123').toString();
        localStorage.setItem('sectionDetails',encryptVgAuditDetails);
        if(subSection.length === 0){
          this.router.navigate(['/pages/VGAudit/VGAuditReport/questions/', id]);
        } else{
          this.router.navigate(['/pages/VGAudit/VGAuditReport/subsection/', id]);
        }
      }
    })
  }

  getStatusOfSubSection(){
    let obj = {
      'sectionId': this.section2.id,
      'vgaAuditNumber': this.vgAudit.vga_audit_number
    }
    this.subSectionService.postSubSectionStatus(obj).subscribe((res: any) => {
      if(res){
        let statusColor = res.status.statusColorOfSection;
        this.sectionsOfVgReport.map((sec: any)=>{
          if(sec.sequence_number === 2){
            setTimeout(() => {
              if(statusColor === 'green'){
                  document.getElementById(sec.id).style.backgroundColor = '#B6E719';
              } else if(statusColor === 'red'){
                  document.getElementById(sec.id).style.backgroundColor = '#FF8022';
              } else{
                  document.getElementById(sec.id).style.backgroundColor = '#FFCA06';
              }
            },700)
          }
        })
      }
    })
  }

  backToUpdateVGAudit(){
    this.location.back();
  }

}
