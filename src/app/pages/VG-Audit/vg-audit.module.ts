import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VGAuditRoutingModule } from './vg-audit.routing.module';
import { ModalModule } from 'ngb-modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NewVgAuditComponent } from './new-vg-audit/new-vg-audit.component';
import { SearchVgAuditComponent } from './search-vg-audit/search-vg-audit.component';
import { VgaReportComponent } from './vga-report/vga-report.component';
import { ValueGrowthAuditReportComponent } from './value-growth-audit-report/value-growth-audit-report.component';
import { HttpTokenInterceptor } from '../../shared/interceptor/http-token.interceptor';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DropDownComponent } from './vga-report/drop-down/drop-down.component';
import { RadioComponent } from './vga-report/radio/radio.component';
import { CheckBoxComponent } from './vga-report/check-box/check-box.component';
import { TextBoxComponent } from './vga-report/text-box/text-box.component';
import { EditVgAuditComponent } from './edit-vg-audit/edit-vg-audit.component';
import { GetSubsectionComponent } from './get-subsection/get-subsection.component';
import { GetQuestionsComponent } from './get-questions/get-questions.component';
import { VgaReportDetailsComponent } from './vga-report-details/vga-report-details.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { RadioQuestionsComponent } from './vga-report/radio-question/radio-question.component';
import { CompetencyLevelsComponent } from './vga-report/competency-levels/competency-levels.component';
import { EmployeeLevelsComponent } from './vga-report/employee-levels/employee-levels.component';

@NgModule({
  declarations: [
    NewVgAuditComponent, EditVgAuditComponent, SearchVgAuditComponent, VgaReportComponent, ValueGrowthAuditReportComponent,
    DropDownComponent, RadioComponent, CheckBoxComponent, GetSubsectionComponent, GetQuestionsComponent,
    TextBoxComponent, VgaReportDetailsComponent, RadioQuestionsComponent, CompetencyLevelsComponent, EmployeeLevelsComponent
  ],
  imports: [
    CommonModule,
    ModalModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    VGAuditRoutingModule,
    NgbPaginationModule,
    // NgMultiSelectDropDownModule,
    TabsModule.forRoot(),
  ],
  exports: [
    NewVgAuditComponent, EditVgAuditComponent, SearchVgAuditComponent, VgaReportComponent, ValueGrowthAuditReportComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true
    }
  ]
})
export class VGAuditModule { }
