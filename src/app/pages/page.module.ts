import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageRoutingModule } from './page-routing.module';
import { PagesComponent } from './pages.component';
import { ThemeModule } from '../theme/theme.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTokenInterceptor } from '../shared/interceptor/http-token.interceptor';
import { PagesNotFoundComponent } from './pages-not-found/pages-not-found.component';
import { ManagementModule } from './management/management.module';
import { VGAuditModule } from './VG-Audit/vg-audit.module';
import { OrganizationalOverviewComponent } from './organizational-overview/organizational-overview.component';
import { RolePemissionsComponent } from './masters/role-pemissions/role-pemissions.component';
import { SearchEmployeeComponent } from './Employee/search-employee/search-employee.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewEmployeeComponent } from './Employee/new-employee/new-employee.component';
import { SearchClientComponent } from './Clients/search-client/search-client.component';
import { ClientMasterComponent } from './Clients/client-master/client-master.component';
import { SearchConsultantsComponent } from './Consultants/search-consultants/search-consultants.component';
import { NewConsultantComponent } from './Consultants/new-consultant/new-consultant.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FilterPipe } from '../shared/pipes/filter.pipe';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchSectionComponent } from './Sections/search-section/search-section.component';
import { SectionMasterComponent } from './Sections/section-master/section-master.component';
import { SearchSubSectionComponent } from './Sub-Sections/search-sub-section/search-sub-section.component';
import { SubSectionMasterComponent } from './Sub-Sections/sub-section-master/sub-section-master.component';
import { QuestionMasterComponent } from './Questions/question-master/question-master.component';
import { SearchQuestionComponent } from './Questions/search-question/search-question.component';
import { NewClientEmployeeComponent } from './client-employee/new-client-employee/new-client-employee.component';
import { SearchClientEmployeeComponent } from './client-employee/search-client-employee/search-client-employee.component';
import { ModalModule } from 'ngb-modal';
import { CountryComponent } from './masters/country/country.component';
import { StatesComponent } from './masters/states/states.component';
import { CitiesComponent } from './masters/cities/cities.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [PagesComponent, DashboardComponent, PagesNotFoundComponent, OrganizationalOverviewComponent, RolePemissionsComponent, SearchEmployeeComponent, NewEmployeeComponent, SearchClientComponent, ClientMasterComponent, SearchConsultantsComponent, NewConsultantComponent, FilterPipe, SectionMasterComponent, SearchSectionComponent, SearchSubSectionComponent, SubSectionMasterComponent, QuestionMasterComponent, SearchQuestionComponent, NewClientEmployeeComponent, SearchClientEmployeeComponent, CountryComponent, StatesComponent, CitiesComponent],

  imports: [CommonModule, PageRoutingModule, ThemeModule, ManagementModule, FormsModule,
    ReactiveFormsModule,VGAuditModule, NgbPaginationModule, ModalModule, TooltipModule.forRoot(),NgMultiSelectDropDownModule.forRoot()],

  exports: [PagesComponent, DashboardComponent, PagesNotFoundComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true
    },
    DatePipe
  ],

  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PageModule {}
