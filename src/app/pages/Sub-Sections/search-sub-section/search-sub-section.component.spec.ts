import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSubSectionComponent } from './search-sub-section.component';

describe('SearchSubSectionComponent', () => {
  let component: SearchSubSectionComponent;
  let fixture: ComponentFixture<SearchSubSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchSubSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSubSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
