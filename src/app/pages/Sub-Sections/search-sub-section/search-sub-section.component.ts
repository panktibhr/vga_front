import { ToastrService } from 'ngx-toastr';
import { SubSections } from './../../../shared/models/sub-sections';
import { Observable } from 'rxjs';
import { SubSectionService } from './../../../shared/services/sub-section/sub-section.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalManager } from 'ngb-modal';

@Component({
  selector: 'app-search-sub-section',
  templateUrl: './search-sub-section.component.html',
  styleUrls: ['./search-sub-section.component.scss'],
})
export class SearchSubSectionComponent implements OnInit {
  subSection$:any = [];
  _subSection: SubSections;
  sub_section$: any = [];
  selfSubSection$: any;
  subSection: any = [];
  searchSubSection: String;
  matchSubSectionObj: any;
  page: any = 0;
  itemsPerPage: number = 5;
  previousPage: any;
  totalItems: any;
  _searchSubSectionsDeleteId: any;
  deleteModalRef: any;
  @ViewChild('contentDelete') contentDelete;
  constructor(
    private router: Router,
    private subSectionService: SubSectionService,
    private toastr: ToastrService,
    private modalService: ModalManager
  ) {
    this._subSection = new SubSections();
  }

  ngOnInit(): void {
    this.getAllSubSections(this._subSection);
  }

  getAllSubSections(subSectionObj) {
    this.subSectionService
      .getAllSubSections(subSectionObj)
      .subscribe((res: any) => {
        if(res){
          this.totalItems = res.res.total;
          console.log(res.res.total);
          this.subSection$ = res.res.results;
        }else{
          this.subSection$ = [];
          this.totalItems = 0;
        }
      });
  }

  onEditSubSectionDetails(id) {
    this.router.navigate(['/pages/EditSubSection', id]);
  }
  onDeleteSubSection(id) {
      this._searchSubSectionsDeleteId= id;
      this.deleteModalRef = this.modalService.open(this.contentDelete, {
        size: "md",
        modalClass: 'mymodal',
        hideCloseButton: false,
        centered: false,
        backdrop: true,
        animation: true,
      })
      this.deleteModalRef.onClose.subscribe(() => {
      })

  }

  OnCanceleDeleteModal() {
    this.modalService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
    this.deleteSubSection(this._searchSubSectionsDeleteId);
        this.modalService.close(this.deleteModalRef);

  }

  deleteSubSection(id) {
    this.subSectionService.deleteSubSections(id).subscribe((res: any) => {
      if (res.success) {
        this.toastr.success('Sub section deleted Successfully');
        this.getAllSubSections(this._subSection);
      } else {
        this.toastr.error('Something is wrong');
      }
    });
  }
  getSelfSubSectionName(id) {
    this.subSection = [];
    this.subSection.push(this.subSection$);
    this.matchSubSectionObj = this.subSection[0].filter((element: any) => {
      if (element.id === id) {
        return element;
      }
    });
    if (this.matchSubSectionObj.length > 0) {
      return this.matchSubSectionObj[0].sub_section_name;
    } else {
      return 'NA';
    }
  }

  onSearch(searchValue) {
    this._subSection.SearchText = searchValue;
    this.getAllSubSections(this._subSection);
  }

  onClear(clearText){
    this._subSection.SearchText = clearText;
    this.getAllSubSections(this._subSection);
  }

  sort(columnName: any) {
    this._subSection.SortItem = columnName;
    if (this._subSection.SortOrder == 'ASC') {
      this._subSection.SortOrder = 'DESC';
    } else {
      this._subSection.SortOrder = 'ASC';
    }
    this.getAllSubSections(this._subSection);
  }

  loadPage(page:number){
    if(page !== this.previousPage){
      this._subSection.PageNo = page - 1;
      this.previousPage = page;
      this.getAllSubSections(this._subSection);
    }
  }
}
