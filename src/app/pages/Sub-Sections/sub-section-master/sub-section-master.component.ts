import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
import { ToastrService } from 'ngx-toastr';
import { SubSections } from './../../../shared/models/sub-sections';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSectionService } from './../../../shared/services/sub-section/sub-section.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-sub-section-master',
  templateUrl: './sub-section-master.component.html',
  styleUrls: ['./sub-section-master.component.scss'],
})
export class SubSectionMasterComponent implements OnInit {
  subSectionForm: FormGroup;
  subSectionHeader: String;
  buttonName: String;
  subSectionObj: Observable<SubSections>;
  section$: any;
  sectionId: any;
  allSection: any;
  subSection$: any;
  _subsection: SubSections;
  errorMsg: any;
  section_id: any;
  sequence_numbers:any = [];
  hasError: any = false;
  onInit: boolean = false;

  constructor(
    private subSectionService: SubSectionService,
    private router: Router,
    private sectionService: SectionMasterService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute
  ) {
    this._subsection = new SubSections();
    this.sectionId = this.actRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getAllSections(this._subsection);
    this.getAllSubsection();
    if (this.router.url === '/pages/NewSubSection') {
      this.subSectionHeader = 'New Sub Section';
      this.buttonName = 'Submit';
    } else {
      this.subSectionHeader = 'Update Sub Section';
      this.buttonName = 'Update';
      this.getAllSubSectionsById();
    }

    this.subSectionForm = this.formBuilder.group({
      sectionId: ['', Validators.required, this.validateSection()],
      subSectionName: ['', [Validators.required]],
      description: ['', Validators.required],
      seqNo: [null, [Validators.required, this.validateSeqNo()]],
      selfSubSectionId: [null],
    });
  }

  onSubmit() {
    this.subSectionForm.markAllAsTouched();
    // if(this.subSectionForm.valid){
    if (this.router.url === '/pages/NewSubSection') {
      console.log('hello');
      this.subSectionObj = this.subSectionService.postSubSections(
        this.subSectionForm.value
      );
      this.subSectionObj.subscribe((res: any) => {
        console.log(res);
        if (res.success === true) {
          setTimeout(() => {
            this.toastr.success('Record added successfully');
            this.router.navigate(['/pages/SearchSubSection']);
          }, 2000);
        } else {
          this.toastr.error('Something is wrong');
        }
      });
    } else {
      this.editSubSection();
    }
    // }else{

    // }
  }
  getAllSubSectionsById() {
    this.subSectionService
      .getSubSectionById(this.sectionId)
      .subscribe((res: any) => {
        this.allSection = res.subsection;
        console.log(this.allSection);
        this.subSectionForm
          .get('subSectionName')
          .patchValue(this.allSection.sub_section_name);
        this.subSectionForm
          .get('description')
          .patchValue(this.allSection.sub_section_description);
        this.subSectionForm
          .get('seqNo')
          .patchValue(this.allSection.sequence_number);
        this.subSectionForm
          .get('selfSubSectionId')
          .patchValue(this.allSection.self_sub_section_id);
        this.subSectionForm
          .get('sectionId')
          .patchValue(this.allSection.section_id);
      });
  }
  editSubSection() {
    let editObj = {
      subSectionName: this.subSectionForm.get('subSectionName').value,
      description: this.subSectionForm.get('description').value,
      seqNo: this.subSectionForm.get('seqNo').value,
      selfSubSectionId: this.subSectionForm.get('selfSubSectionId').value,
      sectionId: this.subSectionForm.get('sectionId').value,
    };

    // setTimeout(() => {
    this.putSubSection(this.sectionId, editObj);
    // this.subSectionForm.reset();
    // }, );
  }
  putSubSection(subSecId, subSecObj) {
    this.subSectionService
      .updateSubSections(subSecId, subSecObj)
      .subscribe((res: any) => {
        if (res !== undefined) {
          this.toastr.success('Updated successfully');
          this.router.navigate(['/pages/SearchSubSection']);
        } else {
          this.toastr.error('Something went wrong');
        }
      });
  }
  getAllSections(subSectionObj) {
    this.sectionService.getALLSections(subSectionObj).subscribe((res: any) => {
      this.section$ = res.res.results;
    });
  }
  getAllSubsection() {
    this.subSectionService
      .getAllSubSection()
      .subscribe((res: any) => {
        console.log(res)
        this.subSection$ = res.res;
      });
  }

  public validateSeqNo(): ValidatorFn {
    return (control: AbstractControl):any => {
      this.subSectionService.getAllSubSection()
        .subscribe((data: any) => {
          let section = data.res;
          let isError = false;
          section.map((sections:any) => {
            let secID = sections.section_id;
            if(this.section_id === secID){
              this.sequence_numbers.push(sections.sequence_number);
              let seq_no;
              this.sequence_numbers.map((data: any) => seq_no = data);
              if(control.value === seq_no && this.onInit){
                isError = true;
                this.errorMsg = "Sequence Number is already exist";
              }
            }
          });
          this.hasError = isError;
          return { hasError: isError };
        });
    }
  }

  public validateSection(): ValidatorFn {
    return ((control: AbstractControl): any => {
      this.section_id = control.value;
    })
  }

  onSequenceChange(){
    this.onInit = true;
  }

}
