import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
import { QuestionMasterService } from '../../../shared/services/question-master/question-master.service';
import { AnswerValue, Questions } from '../../../shared/models/questions';
import { ToastrService } from 'ngx-toastr';
import { Sections } from 'src/app/shared/models/sections';
import { SubSections } from 'src/app/shared/models/sub-sections';

enum AnswerControlType {
  Dropdown = 'Dropdown',
  Radio = 'Radio',
  Checkbox = 'Checkbox',
  Textbox = 'Textbox',
}
enum AnswerValueType {
  OneToFive = '1-5',
  OneToThree = '1-3',
}
@Component({
  selector: 'app-question-master',
  templateUrl: './question-master.component.html',
  styleUrls: ['./question-master.component.scss'],
})
export class QuestionMasterComponent implements OnInit {
  questionForm: FormGroup;
  eAnsweControlType = AnswerControlType;
  eAnswerValueType = AnswerValueType;
  sectionObj: any;
  postQuestionObj: Observable<Questions>;
  question_id: any;
  questionHeader: string;
  button: string;
  questionObj: any;
  questionInfoById: any;
  subSectionObj: any;
  _sections: Sections;
  _subsections: SubSections;
  answerListingForm: FormGroup;
  subSectionNameObj: any = [];
  questions: Questions;
  subQuestionObj: any;
  title: any = [];
  section_id: any;
  subSection_id: any;
  hasError = false;
  errorMessage: string;
  answerTypeObj:any;

  constructor(
    private formBuilder: FormBuilder,
    private sectionMasterService: SectionMasterService,
    private router: Router,
    private questionMasterService: QuestionMasterService,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
  ) {
    this.question_id = this.actRoute.snapshot.params.id;
    this.questionForm = this.formBuilder.group({
      sectionId: ['', Validators.required, this.validateSection()],
      subSectionId: ['', this.validateSubSection()],
      questionName: ['', Validators.required],
      answerControlType: ['', [Validators.required, this.validateAnwerControlType()]],
      answerValueType: ['', [Validators.required]],
      deafaultAnswerValue: [''],
      sequenceNumber: ['', [Validators.required, this.validateQuestion()]],
      subQuestionsList: this.formBuilder.array([this.createAnswer()]),
    });
    this._sections = new Sections();
    this._subsections = new SubSections();
    this.questions = new Questions();
  }

  get subQuestionsListing(): FormArray {
    return this.questionForm.get('subQuestionsList') as FormArray;
  }

  createAnswer(): FormGroup {
    return this.formBuilder.group({
      subQuestion: [''],
    });
  }

  addNewAnswer() {
    this.subQuestionsListing.push(this.createAnswer());
  }

  removeAnswer(i) {
    this.subQuestionsListing.removeAt(i);
  }

  ngOnInit(): void {
    if (this.router.url === '/pages/NewQuestion') {
      this.questionHeader = 'Add Question';
      this.button = 'Submit';
    } else {
      this.questionObj = this.questionMasterService.getQuestionById(this.question_id);
      this.questionObj.subscribe((res: any) => {
        this.questionInfoById = res.res;
        this.getAnswer();
      });
      this.questionHeader = 'Update Question';
      this.button = 'Update';
      this.getQuestionDetailById();
    }
    this.getALLSections(this._sections);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    this.questionForm.markAllAsTouched();
    // if (this.questionForm.valid) {
      if (this.router.url === '/pages/NewQuestion') {
        this.postQuestionObj = this.questionMasterService.postQuestionObj(this.questionForm.value);
        this.postQuestionObj.subscribe((res: any) => {
          console.log(res)
          if (res) {
            setTimeout(() => {
              this.toastr.success('Questions added successfully!');
              this.questionForm.reset();
              this.router.navigate(['/pages/SearchQuestion']);
            }, 1500);
          }
        }, (error) => {
          if(error){
            this.toastr.error('Sequence Number is already taken!')
          }
        });
      // }
      } else {
        this.editQuestionDetails();
      }
  }
  getQuestionDetailById() {
    this.questionForm.get('sectionId').disable();
    this.questionForm.get('subSectionId').disable();
    this.questionObj = this.questionMasterService.getQuestionById(
      this.question_id
    );
    this.questionObj.subscribe((res: any) => {
      this.questionInfoById = res.res;
      this.onChnageOfSection(this.questionInfoById.section_id);
      this.questionForm.get('sectionId').patchValue(this.questionInfoById.section_id);
      this.questionForm.get('subSectionId').patchValue(this.questionInfoById.sub_section_id);
      this.questionForm.get('questionName').patchValue(this.questionInfoById.question);
      this.questionForm.get('answerControlType').patchValue(this.questionInfoById.answer_type);
      this.questionForm.get('answerValueType').patchValue(this.questionInfoById.answer_value);
      this.questionForm.get('deafaultAnswerValue').patchValue(this.questionInfoById.by_default_value);
      this.questionForm.get('sequenceNumber').patchValue(this.questionInfoById.sequence_number);
      this.questionForm.setControl('subQuestionsList', this.setExistingData(this.questionInfoById.sub_questions));
    });
  }

  setExistingData(ansValue: AnswerValue[]) : FormArray{
    const formArray = new FormArray([]);
    ansValue?.forEach(obj => {
      formArray.push(this.formBuilder.group({
        subQuestion: obj.title
      }))
    })
    return formArray;
  }

  getAnswer(){
    console.log("Get Answers", this.questionInfoById)
    return this.questionInfoById.sub_questions.map((ele:any) => {
      this.title = ele.title;
    })
  }

  editQuestionDetails() {
    let questionDetailObjForPost = {
      sectionId: this.questionForm.get('sectionId').value,
      subSectionId: this.questionForm.get('subSectionId').value,
      questionName: this.questionForm.get('questionName').value,
      answerControlType: this.questionForm.get('answerControlType').value,
      answerValueType: this.questionForm.get('answerValueType').value,
      deafaultAnswerValue: this.questionForm.get('deafaultAnswerValue').value,
      subQuestionsList: this.questionForm.get('subQuestionsList').value,
    };
    setTimeout(() => {
      this.putQuestionById(this.question_id, questionDetailObjForPost);
      this.questionForm.reset();
    }, 1500);
  }

  putQuestionById(queId, queObj) {
    this.questionMasterService
      .updateQuestionObj(queId, queObj)
      .subscribe((res: any) => {
        if (res !== undefined) {
          this.toastr.success('Question Updated Successfully!');
          this.router.navigate(['/pages/SearchQuestion']);
        } else {
          this.toastr.error('Something went wrong to edit question details!');
        }
      });
  }
  getALLSections(sectionObj) {
    this.sectionMasterService
      .getALLSections(sectionObj)
      .subscribe((res: any) => {
        this.sectionObj = res.res.results;
      });
  }

  onChnageOfSection(sectionId){
    this.questionMasterService.getSubSectionsBySection(sectionId).subscribe((res: any) => {
      this.subSectionNameObj = res.res.subsection;
    })
  }

  public validateSection(): ValidatorFn {
    return ((control: AbstractControl): any => {
      this.section_id = control.value;
    })
  }

  public validateSubSection(): ValidatorFn {
    return ((control: AbstractControl) : any => {
      this.subSection_id = control.value;
    })
  }

  public validateQuestion(): ValidatorFn {
    return ((control: AbstractControl) :any => {
      if(control.value.length > 0){
        let sectionData = [];
        let subSectionData = [];
        let sequenceNumData = [];
        let sectionId, subSectionId,sequenceNumber;
        this.questionMasterService.getQuestionsBySubSections(this.subSection_id).subscribe((res:any) => {
          let isError = false;
          let data = res.res;
          data.map((obj: any) => {
            sectionData.push(obj.section_id);
            subSectionData.push(obj.sub_section_id);
            sequenceNumData.push(obj.sequence_number);
            sectionData.map((secId:any) => sectionId = secId);
            subSectionData.map((subSecId: any) => subSectionId = subSecId);
            sequenceNumData.map((seqNo: any) => sequenceNumber = seqNo);

            if(this.section_id === sectionId){
              if(this.subSection_id === subSectionId){
                if(control.value == sequenceNumber){
                  isError = true;
                  this.errorMessage = "Sequence Number is already existed.";
                }
              }
            }
          });
          this.hasError = isError;
          return { hasError: isError };
        });
      }
    });
  }

  public validateAnwerControlType(): ValidatorFn {
    return ((control: AbstractControl) : any => {
      console.log(`control.value - `, control.value);
      this.answerTypeObj =  control.value;
    });
  }

}
