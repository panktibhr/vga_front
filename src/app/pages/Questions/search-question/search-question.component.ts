import { Questions } from './../../../shared/models/questions';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { QuestionMasterService } from 'src/app/shared/services/question-master/question-master.service';
import { ModalManager } from 'ngb-modal';

@Component({
  selector: 'app-search-question',
  templateUrl: './search-question.component.html',
  styleUrls: ['./search-question.component.scss']
})
export class SearchQuestionComponent implements OnInit {
  questionObj: any;
  page: any = 0;
  itemsPerPage: number = 5;
  previousPage: any;
  totalItems: any;
  _questions : Questions;
  _questionDeleteId: any;
  deleteModalRef: any;
  @ViewChild('contentDelete') contentDelete;
  constructor(private questionMasterService: QuestionMasterService, private router:Router, private toastr: ToastrService, private modalService: ModalManager ) {
    this._questions = new Questions();
  }

  ngOnInit(): void {
    this.getAllQuestions(this._questions);
  }

  getAllQuestions(questionsObj){
    this.questionMasterService.getAllQuestions(questionsObj).subscribe((res: any) => {
      if(res){
        this.totalItems = res.res.total;
        this.questionObj = res.res.results;
      }else{
        this.totalItems = 0;
        this.questionObj = [];
      }
    })
  }
  editQuestion(id){
    this.router.navigate(['/pages/edit-question', id])
  }
  deleteQuestion(id){
    //
    this._questionDeleteId= id;
      this.deleteModalRef = this.modalService.open(this.contentDelete, {
        size: "md",
        modalClass: 'mymodal',
        hideCloseButton: false,
        centered: false,
        backdrop: true,
        animation: true,
      })
      this.deleteModalRef.onClose.subscribe(() => {
      })
  }

  OnCanceleDeleteModal() {
    this.modalService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
    this.questionMasterService.deleteQuestionObj(this._questionDeleteId).subscribe((res: any) => {
        if(res.success === true){
          this.toastr.success('Question deleted successfully!')
          this.modalService.close(this.deleteModalRef);
          this.getAllQuestions(this._questions);
        }
    })
  }

  onSearch(searchValue) {
    this._questions.SearchText = searchValue;
    this.getAllQuestions(this._questions);
  }

  onClear(clearText){
    this._questions.SearchText = clearText;
    this.getAllQuestions(this._questions);
  }

  sort(columnName: any) {
    this._questions.SortItem = columnName;
    if (this._questions.SortOrder == 'ASC') {
      this._questions.SortOrder = 'DESC';
    } else {
      this._questions.SortOrder = 'ASC';
    }
    this.getAllQuestions(this._questions);
  }

  loadPage(page:number){
    if(page !== this.previousPage){
      this._questions.PageNo = page - 1;
      this.previousPage = page;
      this.getAllQuestions(this._questions);
    }
  }

}
