import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Sections } from 'src/app/shared/models/sections';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
@Component({
  selector: 'app-section-master',
  templateUrl: './section-master.component.html',
  styleUrls: ['./section-master.component.scss']
})
export class SectionMasterComponent implements OnInit {

  sectionForm: FormGroup;
  postSectionObj: Observable<Sections>;
  sectionHeader:String;
  button:String;
  sectionId:any;
  sectionInfoById:any;
  sequnce: any;
  _section: Sections;
  asc: any;
  click: any;
  msg: any;
  hasError: any = false;
  errorMsg: string;
  onInit: boolean = false;

  constructor(private sectionMasterService: SectionMasterService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router, private actRoute:ActivatedRoute) {
      this.sectionId = this.actRoute.snapshot.params.id;
      this._section = new Sections();
    }

  ngOnInit(): void {
    if(this.router.url === '/pages/newSection'){
      this.sectionHeader = "New Section";
      this.button = "Submit";
    }else{
      this.sectionHeader = "Update Section";
      this.button = "Update";
      this.getSectionId();
    }
    this.sectionForm = this.formBuilder.group({
      sectionName: ['', Validators.required],
      sectionDescription: ['', Validators.required],
      seqNo: [null, [Validators.required, this.validateSeqNo()]],
      isDefault: ['']
    });
  }

  onSubmit(){
    this.sectionForm.markAllAsTouched();
    if (this.sectionForm.valid) {
      if(this.router.url === '/pages/newSection'){
      this.postSectionObj = this.sectionMasterService.postSectionObj(this.sectionForm.value);
      this.postSectionObj.subscribe((res:any) => {
        if(res){
          if(res.success === true){
            setTimeout(() => {
              this.toastr.success('Section added successfully!');
              this.sectionForm.reset();
              this.router.navigate(['/pages/SearchSection']);
            },2000);
          }
        }
      })}else{
        this.editSection();
      }
    } else {
      // this.validateAllFormFields(this.sectionForm);
    }
  }

  getSectionId(){
    this.sectionMasterService.getSectionById(this.sectionId).subscribe((res:any) => {
      this.sectionInfoById = res.res;
      this.sectionForm.get('sectionName').patchValue(this.sectionInfoById.section_name);
      this.sectionForm.get('sectionDescription').patchValue(this.sectionInfoById.section_description);
      this.sectionForm.get('seqNo').patchValue(this.sectionInfoById.sequence_number);
      this.sectionForm.get('isDefault').patchValue(this.sectionInfoById.isdefault);
    })
  }

  editSection(){
    let obj = {
      "sectionName": this.sectionForm.get('sectionName').value,
      "sectionDescription": this.sectionForm.get('sectionDescription').value,
      "seqNo": this.sectionForm.get('seqNo').value,
      "isDefault": this.sectionForm.get('isDefault').value
    }
    setTimeout(() => {
      this.putSectionByIdToApi(this.sectionId, obj);
      this.sectionForm.reset();
    }, 2000);
  }

  putSectionByIdToApi(sectionId, sectionObj){
    this.sectionMasterService.updateSectionObj(sectionId, sectionObj).subscribe((res:any) => {
      if(res !== undefined){
        this.toastr.success('Record updated successfully');
        this.router.navigate(['/pages/SearchSection']);
      }else{
        this.toastr.error('Something is wrong');
      }
    })
  }

  public validateSeqNo(): ValidatorFn {
    return (control: AbstractControl):any => {
      this.sectionMasterService.getALLSections(this._section)
        .subscribe( data=> {
          let isError = false;
          this.asc=data;
          this.msg =""
          let arry=this.asc.res.results;
            arry.forEach((value)=>{
              if (value.sequence_number === control.value && this.onInit) {
                isError = true;
                this.errorMsg = "Sequence Number is already exist";
              }
            });
            this.hasError = isError;
            return { hasError: isError };
          });
    }
  }

  onSequenceChange(){
    this.onInit = true;
  }
}

