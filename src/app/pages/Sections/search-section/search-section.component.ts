import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SectionMasterService } from '../../../shared/services/section-master/section-master.service';
import { Sections } from 'src/app/shared/models/sections';
import { ModalManager } from 'ngb-modal';

@Component({
  selector: 'app-search-section',
  templateUrl: './search-section.component.html',
  styleUrls: ['./search-section.component.scss']
})
export class SearchSectionComponent implements OnInit {

  @ViewChild('contentDelete') contentDelete;
  sections$: any = [];
  _section: Sections;
  page: any = 0;
  itemsPerPage: number = 5;
  previousPage: any;
  totalItems: any;
  deleteModalRef: any;
  _searchSectionsDeleteId: any;
  // isLoading: boolean = true;

  constructor(private sectionMasterService: SectionMasterService, private actRoute: ActivatedRoute, private toastr: ToastrService, private router: Router,
    private modalService: ModalManager) {
    this._section = new Sections();
  }

  ngOnInit(): void {
    this.getALLSections(this._section);
  }

  getALLSections(sectionObj) {
    this.sectionMasterService.getALLSections(sectionObj).subscribe((res: any) => {
      if (res) {
        this.totalItems = res.res.total;
        this.sections$ = res.res.results;
        // this.isLoading = false;
      } else {
        this.sections$ = [];
        this.totalItems = 0;
        // this.isLoading = true;
      }
    })
  }

  onEditSectionDetails(id) {
    console.log(id)
    this.router.navigate(['/pages/edit-section', id])
  }

  onDeleteSection(id) {
    console.log(id);
    this._searchSectionsDeleteId= id;
    this.deleteModalRef = this.modalService.open(this.contentDelete, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
    })
    this.deleteModalRef.onClose.subscribe(() => {
    })
    }

    OnCanceleDeleteModal() {
      this.modalService.close(this.deleteModalRef);
    }

    onConfrimDelete() {
      this.deleteSectionByIdToApi(this._searchSectionsDeleteId);
          this.modalService.close(this.deleteModalRef);

    }

  deleteSectionByIdToApi(idSection) {
    this.sectionMasterService.deleteSectionObj(idSection).subscribe((res: any) => {
      console.log(res.success)
      if (res.success === true) {
        this.toastr.success("Section deleted successfully");
      } else {
        this.toastr.error("Somethig went wrong");
      }
      this.getALLSections(this._section);
    })
  }

  onSearch(searchValue) {
    this._section.SearchText = searchValue;
    this.getALLSections(this._section);
  }

  onClear(clearText){
    this._section.SearchText = clearText;
    this.getALLSections(this._section);
  }

  sort(columnName: any) {
    this._section.SortItem = columnName;
    if (this._section.SortOrder === 'ASC') {
      this._section.SortOrder = 'DESC';
    } else {
      this._section.SortOrder = 'ASC';
    }
    this.getALLSections(this._section);
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this._section.PageNo = page - 1;
      this.previousPage = page;
      this.getALLSections(this._section);
    }
  }
}
