import { Component, OnDestroy, OnInit } from '@angular/core';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';
import { ConsultantService } from 'src/app/shared/services/consultant/consultant.service';
import { EmployeesService } from 'src/app/shared/services/employees/employees.service';
import { environment } from 'src/environments/environment';
import { Clients } from 'src/app/shared/models/clients';
import { Consultant } from 'src/app/shared/models/consultant';
import { Employees } from 'src/app/shared/models/employees';
import { clientEmployees } from 'src/app/shared/models/client-employee';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';
import { Subscription } from 'rxjs';
import { VgAuditService } from 'src/app/shared/services/vg-audit/vg-audit.service';
import { vgAudit } from 'src/app/shared/models/vg-audit';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  totalClients:number;
  totalEmployees: number;
  totalConsultants: number;
  userObj: any;
  roleType: any;
  totalClientEmp: any;
  _clients: Clients;
  _employees:Employees;
  _consultants: Consultant;
  _clientEmp: clientEmployees;
  subscription: Subscription;
  clientId: any;
  notificationByClient:any;
  _vgAudits: vgAudit;
  allVgaReport: any;
  vgaReportStatus: any;

  constructor(private clientService: ClientsService,private toastr: ToastrService, private consultantService: ConsultantService, private employeeService: EmployeesService, private clientEmpService: ClientEmployeeService, private vgAuditService: VgAuditService, public router: Router) {
      this._employees = new Employees();
      this._clients = new Clients();
      this._consultants = new Consultant();
      this._clientEmp = new clientEmployees();
      this._vgAudits = new vgAudit();
      this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
      console.log(`this.userObj - `, this.userObj)
      this.roleType = this.userObj.roleName;
      this.clientId = sessionStorage.getItem('clientId');
  }

  ngOnInit(): void {
    this.getClientDetails();
    setTimeout(() => {
      this.clientId = sessionStorage.getItem('clientId');
      this.getReportByStatus(this.clientId);
      this.getAllVgAudits(this._vgAudits);
    },500)
    this.getAllEmployees(this._employees);
    this.getAllClients(this._clients);
    this.getAllConsultants(this._consultants);
    this.getAllClientEmployee(this._clientEmp);
    
    if(this.roleType === 'Client') {
      setTimeout(() => {
        this.getClientExpiryNotification(this.clientId); 
      },500)
    }
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  getClientExpiryNotification(id) {
   this.clientService.getExpiryNotification(id).subscribe((res:any) =>{
      this.notificationByClient = res.res;
      this.toastr.warning(this.notificationByClient);
    })
  }
  getAllEmployees(employeeObj){
    this.subscription = this.employeeService.getAllEmployees(employeeObj).subscribe((res: any) => {
      this.totalEmployees =  res.res.total;
    });
  }

  getAllClients(clientObj){
    this.subscription = this.clientService.getAllClients(clientObj).subscribe((res: any) => {
      this.totalClients =  res.res.total;
    });
  }

  getAllConsultants(consultantObj){
    this.subscription = this.consultantService.getAllConsultants(consultantObj).subscribe((res: any) => {
      this.totalConsultants =  res.res.total;
    });
  }

  getAllClientEmployee(clientEmpObj){
    this.subscription = this.clientEmpService.getClientEmployees(clientEmpObj).subscribe((res:any) => {
      this.totalClientEmp = res.res.total;
    });
  }

  getClientDetails() {
    if(this.roleType !== 'Super Admin' && this.roleType !== 'Consultant'){
      this.subscription = this.clientService.getClientForUsers(this.userObj.id).subscribe((res: any) => {
        this.clientId = res.res.id;
        sessionStorage.setItem('clientId', this.clientId);
      });
    }
  }

  getReportByStatus(clientId){
    this.subscription = this.vgAuditService.getStatusReportOfClient(clientId).subscribe((res: any) => {
      this.vgaReportStatus = res.res;
    });
  }

  getAllVgAudits(vgAuditObj){
    if(this.roleType === 'Client'){
      setTimeout(() => {
        this.subscription = this.vgAuditService.getAllVgAudits(this.clientId, vgAuditObj).subscribe((res:any) => {
          this.allVgaReport = res.res.length;
        });
      }, 500);
    } else {
      this.subscription = this.vgAuditService.getAllVgAudits(this.userObj.id, vgAuditObj).subscribe((res:any) => {
        this.allVgaReport = res.res.length;
      });
    }
  }

}
