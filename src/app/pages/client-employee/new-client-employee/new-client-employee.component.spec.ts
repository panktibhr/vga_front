import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewClientEmployeeComponent } from './new-client-employee.component';

describe('NewClientEmployeeComponent', () => {
  let component: NewClientEmployeeComponent;
  let fixture: ComponentFixture<NewClientEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewClientEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewClientEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
