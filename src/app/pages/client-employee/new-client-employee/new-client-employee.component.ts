import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subscription } from 'rxjs';
import { clientEmployees } from 'src/app/shared/models/client-employee';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';
import { UserAccountService } from 'src/app/shared/services/user-account/user-account.service';
import { Account } from 'src/app/shared/models/userAccount';
import { ClientsService } from 'src/app/shared/services/clients/clients.service';
import { Clients } from 'src/app/shared/models/clients';
import { CountriesService } from 'src/app/shared/services/countries/countries.service';
import { StatesService } from 'src/app/shared/services/states/states.service';
import { CitiesService } from 'src/app/shared/services/cities/cities.service';
import { Countries } from 'src/app/shared/models/countries';
import { States } from 'src/app/shared/models/states';
import { Cities } from 'src/app/shared/models/cities';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-new-client-employee',
  templateUrl: './new-client-employee.component.html',
  styleUrls: ['./new-client-employee.component.scss'],
})
export class NewClientEmployeeComponent implements OnInit, OnDestroy {
  employeeHeader: string;
  button: string;
  clientEmployeeForm: FormGroup;
  addDepartmentForm: FormGroup;
  addDesignationForm: FormGroup;
  addClientForm: FormGroup;
  countries: any;
  states: any;
  cities: any;
  empDesignation: any;
  empDepartment: any;
  empClient: any;
  postClientEmpObj: Observable<clientEmployees>;
  accounts$: any = [];
  clients$: any = [];
  company$: any = [];
  _accounts: Account;
  _clients: Clients;
  clientsObj: any;
  clientEmp_id: any;
  subscription: Subscription;
  statesSubscription: Subscription;
  employeeDetailsListing: Observable<clientEmployees>;
  getAllClientEmpDetails: any;
  getEmpDepartment: Observable<clientEmployees>;
  getEmpDesignation: Observable<clientEmployees>;
  _countries: Countries;
  _states: States;
  _cities: Cities;
  userObj: any;
  roleType: any;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private clientEmpService: ClientEmployeeService,
    private accountservice: UserAccountService,
    private clientservice: ClientsService,
    private actRoute: ActivatedRoute,
    private countryService: CountriesService,
    private stateService: StatesService,
    private cityService: CitiesService
  ) {
    this._accounts = new Account();
    this._clients = new Clients();
    this.clientEmp_id = this.actRoute.snapshot.params.id;
    this._countries = new Countries();
    this._states = new States();
    this._cities = new Cities();
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;}

  ngOnInit(): void {
    if (this.router.url === '/pages/new-clientEmployee') {
      this.employeeHeader = 'Add Client Employee';
      this.button = 'Submit';
    } else {
      this.employeeHeader = 'Update Client Employee';
      this.button = 'Update';
      this.onEditClientEmpById();
    }
    this.getClientEmployeeDepartment();
    this.getEmployeeDesignation();
    this.getCountries();
    this.getallClientsObj(this._clients);
    this.getAccountDetailObj(this._accounts);
    this.countries = JSON.parse(localStorage.getItem('Countries'));
    this.clientEmployeeForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      password: [''],
      parent_account_id: ['', Validators.required],
      clientEmpName: ['', [Validators.required, Validators.minLength(3)]],
      clientEmpTitle: ['', Validators.required],
      clientEmpLevel: ['', Validators.required],
      designation: ['', Validators.required],
      deptdiv: ['', Validators.required],
      client_id: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/),
        ],
      ],
      mobileNumber: [
        '',
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
        ],
      ],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', [Validators.required, Validators.maxLength(6), Validators.minLength(6)]],
      username: ['', Validators.required],
    });
    this.addDepartmentForm = this.formBuilder.group({
      deptdiv: [''],
    });
    this.addDesignationForm = this.formBuilder.group({
      designation: [''],
    });
    this.addClientForm = this.formBuilder.group({
      client: [''],
    });
  }

  generatePassword(length, charSet = '') {
    charSet = charSet
      ? charSet
      : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789^°!"§$%&/()=?`*+~\'#,;.:-_';
    let newPass = Array.apply(null, Array(length || 10))
      .map(function () {
        return charSet.charAt(Math.random() * charSet.length);
      })
      .join('');
    return newPass;
  }

  editDesignation(content2) {
    this.modalService.open(content2, { centered: false });
  }
  editDepartment(content) {
    this.modalService.open(content, { centered: false });
  }
  editClient(content) {
    this.modalService.open(content, { centered: false });
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    this.clientEmployeeForm.markAllAsTouched();
    this.clientEmployeeForm
      .get('password')
      .patchValue(this.generatePassword(8));
    if (this.router.url === '/pages/new-clientEmployee') {
      this.postClientEmpObj = this.clientEmpService.postClientEmployeeObj(
        this.clientEmployeeForm.value
      );
      this.postClientEmpObj.subscribe(
        (res: any) => {
          if (res?.success) {
            setTimeout(() => {
              this.toastr.success('Client Employee Added Successfully.');
              this.clientEmployeeForm.reset();
              this.router.navigate(['/pages/search-clientEmployee']);
            }, 2000);
          }
        },
        (error: any) => {
          if(error.error.errors[0].msg === 'Email is already exist.'){
            this.toastr.error('Email is already exist!');
          }else if(error.error.errors[0].msg === 'must be at last 3 chars long.'){
            this.toastr.error('Please enter user name more than 2 characters!');
          }else if(error.error.errors[0].msg === 'employee name must be at last 3 chars long.'){
            this.toastr.error('Please enter client employee name more than 2 characters!')
          }
        }
      );
    } else {
      this.editEmployeeDetail();
    }
  }

  onAddDepartment() {
    this.subscription = this.clientEmpService
      .postClientEmployeeDepartment(this.addDepartmentForm.value)
      .subscribe((res: any) => {
        this.getClientEmployeeDepartment();
      });
    this.modalService.dismissAll();
    this.toastr.success('Client Employee Department added successfully!');
    this.addDepartmentForm.reset();
  }
  getClientEmployeeDepartment() {
    this.getEmpDepartment = this.clientEmpService.getClientEmployeeDepartment();
    this.getEmpDepartment.subscribe((res: any) => {
      this.empDepartment = res.res;
    });
  }
  onAddDesignation() {
    this.subscription = this.clientEmpService
      .postClientEmployeeDesignation(this.addDesignationForm.value)
      .subscribe((res: any) => {
        this.getEmployeeDesignation();
        this.modalService.dismissAll();
        this.toastr.success('Employee Designation added successfully!');
        this.addDesignationForm.reset();
      });
  }
  getEmployeeDesignation() {
    this.getEmpDesignation = this.clientEmpService.getClientEmployeeDesignation();
    this.getEmpDesignation.subscribe((res: any) => {
      this.empDesignation = res.res;
    });
  }

  getAccountDetailObj(accountObj) {
    this.subscription = this.accountservice
      .getAccountDetail(accountObj)
      .subscribe((res: any) => {
        if (res) {
          if (res.success === true) {
            this.accounts$ = res.res.results;
          }
        }
      });
  }
  getParantAccountName(id) {
    let matchParentObj = this.accounts$.filter((element: any) => {
      if (element.id === id) {
        return id;
      }
    });
    if (matchParentObj.length > 0) {
      return matchParentObj[0].name;
    } else {
      return 'NA';
    }
  }

  getallClientsObj(clientObj) {
    this.subscription = this.clientservice
      .getAllClients(clientObj)
      .subscribe((res: any) => {
        if (res) {
          if (res.success === true) {
            this.clients$ = res.res.results;
          }
        }
      });
  }

  onEditClientEmpById() {
    this.subscription = this.clientEmpService
      .getClientEmployeesById(this.clientEmp_id)
      .subscribe((res: any) => {
        this.clientsObj = res.res;

        this.getStates(this.clientsObj.countries_id);
        this.getCities(this.clientsObj.states_id);

        this.clientEmployeeForm
          .get('clientEmpName')
          .patchValue(this.clientsObj.name);
        this.clientEmployeeForm
          .get('clientEmpTitle')
          .patchValue(this.clientsObj.employee_title);
        this.clientEmployeeForm
          .get('clientEmpLevel')
          .patchValue(this.clientsObj.employee_level);
        this.clientEmployeeForm
          .get('parent_account_id')
          .patchValue(this.clientsObj.parent_account_id);
        this.clientEmployeeForm
          .get('designation')
          .patchValue(this.clientsObj.designation);
        this.clientEmployeeForm
          .get('deptdiv')
          .patchValue(this.clientsObj.deptdiv);
        this.clientEmployeeForm
          .get('client_id')
          .patchValue(this.clientsObj.client_id);
        this.clientEmployeeForm
          .get('companyName')
          .patchValue(this.clientsObj.company_name);
        this.clientEmployeeForm.get('email').patchValue(this.clientsObj.email);
        this.clientEmployeeForm
          .get('mobileNumber')
          .patchValue(this.clientsObj.mobile);
        this.clientEmployeeForm
          .get('address')
          .patchValue(this.clientsObj.address);
        this.clientEmployeeForm
          .get('country')
          .patchValue(this.clientsObj.countries_id);
        this.clientEmployeeForm
          .get('state')
          .patchValue(this.clientsObj.states_id);
        this.clientEmployeeForm.get('city').patchValue(this.clientsObj.city_id);
        this.clientEmployeeForm.get('zipCode').patchValue(this.clientsObj.zip);
        this.clientEmployeeForm
          .get('username')
          .patchValue(this.clientsObj.username);
        this.clientEmployeeForm
          .get('password')
          .patchValue(this.clientsObj.password);
      });
  }
  editEmployeeDetail() {
    this.putEmployeeByIdToAPI(this.clientEmp_id, this.clientEmployeeForm.value);
  }
  putEmployeeByIdToAPI(empId, empObj) {
    this.subscription = this.clientEmpService
      .updateClientEmployeeObj(empId, empObj)
      .subscribe((res: any) => {
        if (res !== undefined) {
          this.toastr.success('Updated Employee Successfully');
          this.router.navigate(['/pages/search-clientEmployee']);
        } else {
          this.toastr.error('Something went wrong to edit client details!');
        }
      });
  }

  getCountries(){
    this.subscription = this.countryService.getAllCountries().subscribe((res: any) => {
      this.countries = res.res;
    })
  }

  getStates(countryId){
    this.subscription = this.stateService.getState(countryId).subscribe((res:any) => {
      this.states = res.res;
    })
  }

  getCities(stateId){
    this.subscription = this.cityService.getCities(stateId).subscribe((res:any) => {
      this.cities = res.res;
    })
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
}
