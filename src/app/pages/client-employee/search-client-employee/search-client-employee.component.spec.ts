import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchClientEmployeeComponent } from './search-client-employee.component';

describe('SearchClientEmployeeComponent', () => {
  let component: SearchClientEmployeeComponent;
  let fixture: ComponentFixture<SearchClientEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchClientEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchClientEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
