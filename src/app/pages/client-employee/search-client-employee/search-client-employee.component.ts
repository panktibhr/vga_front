import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalManager } from 'ngb-modal';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { clientEmployees } from 'src/app/shared/models/client-employee';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';

@Component({
  selector: 'app-search-client-employee',
  templateUrl: './search-client-employee.component.html',
  styleUrls: ['./search-client-employee.component.scss'],
})
export class SearchClientEmployeeComponent implements OnInit {
  clientEmp$: any = [];
  _clientEmp: clientEmployees;
  page: any = 0;
  itemsPerPage: number = 5;
  previousPage: any;
  totalItems: any;
  clientEmpObj: Observable<clientEmployees>;
  _searchEmployeeDeleteId: any;
  deleteModalRef: any;
  @ViewChild('contentDelete') contentDelete;
  _searchClintEmployeeDeleteId: any;
  constructor(
    private clientEmployeeService: ClientEmployeeService,
    private toastr: ToastrService, private router: Router,
    private modalService: ModalManager
  ) {
    this._clientEmp = new clientEmployees();
  }

  ngOnInit(): void {
    this.getClientEmployees(this._clientEmp);
  }

  getClientEmployees(clientEmpObj) {
    this.clientEmployeeService.getClientEmployees(clientEmpObj).subscribe((res: any) => {
      if (res) {
        this.clientEmp$ = res.res.results;
        this.totalItems = res.res.total;
      } else {
        this.totalItems = 0;
        this.clientEmp$ = [];
      }
    });
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this._clientEmp.PageNo = page - 1;
      this.previousPage = page;
      this.getClientEmployees(this._clientEmp);
    }
  }

  onSearch(searchText) {
    this._clientEmp.SearchText = searchText;
    this.getClientEmployees(this._clientEmp);
  }

  onClear(clearText){
    this._clientEmp.SearchText = clearText;
    this.getClientEmployees(this._clientEmp);
  }

  sort(columnName: any) {
    this._clientEmp.SortItem = columnName;
    if (this._clientEmp.SortOrder == 'ASC') {
      this._clientEmp.SortOrder = 'DESC';
    } else {
      this._clientEmp.SortOrder = 'ASC';
    }
    this.getClientEmployees(this._clientEmp);
  }

  onDeleteClientEmp(id) {
    this._searchClintEmployeeDeleteId= id;
    this.deleteModalRef = this.modalService.open(this.contentDelete, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
    })
    this.deleteModalRef.onClose.subscribe(() => {
    })
  }

  OnCanceleDeleteModal() {
    this.modalService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
       this.onDeleteClientEmpByIdToaApi(this._searchClintEmployeeDeleteId);
        this.toastr.success("User deleted successfully!")
        this.modalService.close(this.deleteModalRef);

      }

  onDeleteClientEmpByIdToaApi(idForClientEmp) {
    this.clientEmployeeService
      .deleteClientEmployeeObj(idForClientEmp)
      .subscribe((res: any) => {
        if (res.success) {
          this.getClientEmployees(this._clientEmp);
        } else {
          this.toastr.error('Soemthing is wrong');
        }
      });
  }

  onUpdateClientEmp(id){
    this.router.navigate(['/pages/edit-clientEmployee', id])
  }

  ngOnDestroy(){

  }
}
