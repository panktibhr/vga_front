import { PagesNotFoundComponent } from './pages-not-found/pages-not-found.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { OrganizationalOverviewComponent } from './organizational-overview/organizational-overview.component';
import { RolePemissionsComponent } from './masters/role-pemissions/role-pemissions.component';
import { NewEmployeeComponent } from './Employee/new-employee/new-employee.component';
import { SearchEmployeeComponent } from './Employee/search-employee/search-employee.component';
import { ClientMasterComponent } from './Clients/client-master/client-master.component';
import { SearchClientComponent } from './Clients/search-client/search-client.component';
import { NewConsultantComponent } from './Consultants/new-consultant/new-consultant.component';
import { SearchConsultantsComponent } from './Consultants/search-consultants/search-consultants.component';
import { SectionMasterComponent } from './Sections/section-master/section-master.component';
import { SearchSectionComponent } from './Sections/search-section/search-section.component';
import { SearchSubSectionComponent } from './Sub-Sections/search-sub-section/search-sub-section.component';
import { SubSectionMasterComponent } from './Sub-Sections/sub-section-master/sub-section-master.component';
import { SearchQuestionComponent } from './Questions/search-question/search-question.component';
import { QuestionMasterComponent } from './Questions/question-master/question-master.component';
import { NewClientEmployeeComponent } from './client-employee/new-client-employee/new-client-employee.component';
import { SearchClientEmployeeComponent } from './client-employee/search-client-employee/search-client-employee.component';
import { CountryComponent } from './masters/country/country.component';
import { StatesComponent } from './masters/states/states.component';
import { CitiesComponent } from './masters/cities/cities.component';

const pageRoutes: Routes = [
  // { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'organizational-overview', component: OrganizationalOverviewComponent },
      {
        path: 'management',
        loadChildren: () =>
        import('./management/management.module').then((module) => module.ManagementModule)
      },
      { path: 'role-permissions', component: RolePemissionsComponent },
      { path: 'countries' , component: CountryComponent},
      { path: 'states' , component: StatesComponent},
      { path: 'cities' , component: CitiesComponent},
      { path: 'new-employee', component: NewEmployeeComponent },
      { path: 'search-employee', component: SearchEmployeeComponent },
      { path: 'edit-employee/:id', component: NewEmployeeComponent },
      { path: 'EmployeessViewProfile/:id', component: NewEmployeeComponent },
      { path: 'new-client', component: ClientMasterComponent },
      { path: 'edit-client/:id', component: ClientMasterComponent },
      { path: 'viewProfile/:id', component: ClientMasterComponent },
      { path: 'search-client', component: SearchClientComponent },
      { path: 'new-clientEmployee', component: NewClientEmployeeComponent },
      { path: 'edit-clientEmployee/:id', component: NewClientEmployeeComponent },
      { path: 'search-clientEmployee', component: SearchClientEmployeeComponent },
      { path: 'new-consultant', component: NewConsultantComponent },
      { path: 'edit-consultant/:id', component: NewConsultantComponent},
      { path: 'ConsultantsViewProfile/:id', component: NewConsultantComponent },
      { path: 'search-consultants', component: SearchConsultantsComponent },
      {
        path: 'VGAudit',
        loadChildren: () =>
        import('./VG-Audit/vg-audit.module').then((module) => module.VGAuditModule)
      },
      { path: 'SearchSection', component: SearchSectionComponent },
      { path: 'newSection', component: SectionMasterComponent },
      { path: 'edit-section/:id', component: SectionMasterComponent },
      { path: 'SearchSubSection', component:  SearchSubSectionComponent},
      { path: 'NewSubSection', component:  SubSectionMasterComponent},
      { path: 'EditSubSection/:id', component: SubSectionMasterComponent},
      { path: 'SearchQuestion', component:  SearchQuestionComponent},
      { path: 'NewQuestion', component:  QuestionMasterComponent},
      { path: 'edit-question/:id', component: QuestionMasterComponent},
      { path: '**', component: PagesNotFoundComponent}
    ],
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forChild(pageRoutes)],
  exports: [RouterModule],
})
export class PageRoutingModule {}
