import { ToastrService } from 'ngx-toastr';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmployeesService } from 'src/app/shared/services/employees/employees.service';
import { Employees } from '../../../shared/models/employees';
import { ModalManager } from 'ngb-modal';
import { ClientEmployeeService } from 'src/app/shared/services/client-employee/client-employee.service';
import { environment } from 'src/environments/environment';
import { clientEmployees } from 'src/app/shared/models/client-employee';

@Component({
  selector: 'app-search-employee',
  templateUrl: './search-employee.component.html',
  styleUrls: ['./search-employee.component.scss']
})
export class SearchEmployeeComponent implements OnInit, OnDestroy {

  @ViewChild('contentDelete') contentDelete;
  myGroup: FormGroup;
  isLoading: boolean;
  employees: Employees;
  employeeObj: Observable<Employees>;
  employees$:any = [];
  // _employees:Employees;
  _clientEmployees: clientEmployees;
  page: any = 0;
  itemsPerPage: number = 5;
  previousPage: any;
  totalItems: any;
  empDepartment: any;
  deleteEmployeeSubscription: Subscription;
  deleteModalRef: any;
  _searchEmployeeDeleteId: any;
  button = 'Confirm';
  clientId: any;
  constructor(public formBuilder: FormBuilder, private employeeService:EmployeesService, private router:Router, private toastr:ToastrService, private modalService: ModalManager, private clientemployeeService: ClientEmployeeService) {
    // this._employees = new Employees();
    this._clientEmployees = new clientEmployees();
    this.clientId = sessionStorage.getItem('clientId');
    console.log(this.clientId)
  }

  ngOnInit() {
    // this.getAllEmployees(this._employees);
    this.getClientEmployeesByclientId(this._clientEmployees);
    this.getEmployeeDepartment();
  }

  getClientEmployeesByclientId(clientEmployeesObj){
    this.clientemployeeService.getClientEmployees(clientEmployeesObj).subscribe((res: any) => {
      if(res){
        this.totalItems = res.res.total;
        this.employees$ = res.res.results;
      }else{
        this.totalItems = 0;
        this.employees$ = [];
      }
    })
  }

  // getAllEmployees(employeeObj){
  //   this.employeeService.getAllEmployees(employeeObj).subscribe((res: any)=>{
  //     if(res){
  //       this.totalItems = res.res.total;
  //       this.employees$ = res.res.results;
  //     }else{
  //       this.totalItems = 0;
  //       this.employees$ = [];
  //     }
  //   })
  // }

  onEditEmployeeDetails(id) {
    this.router.navigate(['/pages/edit-employee', id])
  }

  onDeleteEmployee(id){
    this._searchEmployeeDeleteId = id;
    this.deleteModalRef = this.modalService.open(this.contentDelete, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
    })
    this.deleteModalRef.onClose.subscribe(() => { })
  }

  OnCanceleDeleteModal() {
    this.modalService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
    this.onDeleteEmployeeByIdToaApi(this._searchEmployeeDeleteId);
        this.toastr.success("User deleted successfully!")
        this.modalService.close(this.deleteModalRef);
      }

  onDeleteEmployeeByIdToaApi(idForEmployee){
    this.deleteEmployeeSubscription = this.employeeService.deleteEmployeeObj(idForEmployee)
    .subscribe((res: any) => {
      if (res.success) {
        this.getClientEmployeesByclientId(this._clientEmployees);
      } else {
        this.toastr.error('Soemthing is wrong');
      }
    });
  }

  loadPage(page: number) {
		if (page !== this.previousPage) {
			this.employees.PageNo = page - 1;
			this.previousPage = page;
		 this.getClientEmployeesByclientId(this._clientEmployees);
		}
  }

  onSearch(searchText){
    this._clientEmployees.SearchText = searchText;
    this.getClientEmployeesByclientId(this._clientEmployees);
  }

  onClear(clearText){
    this._clientEmployees.SearchText = '';
    this.getClientEmployeesByclientId(this._clientEmployees);
  }

  sort(columnName:any){
    this._clientEmployees.SortItem = columnName;
    if(this._clientEmployees.SortOrder == "ASC"){
      this._clientEmployees.SortOrder = "DESC";
    }else{
      this._clientEmployees.SortOrder = "ASC";
    }
    this.getClientEmployeesByclientId(this._clientEmployees);
  }

  getEmployeeDepartment(){
    this.employeeService.getEmployeeDepartment().subscribe((res: any) => {
      this.empDepartment = res.res;
    })
  }

  ngOnDestroy(){
    // this.deleteEmployeeSubscription?.unsubscribe();
  }

}
