import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { UserAccountService } from 'src/app/shared/services/user-account/user-account.service';
import { EmployeesService } from '../../../shared/services/employees/employees.service';
import { Account } from '../../../shared/models/userAccount';
import { ToastrService } from 'ngx-toastr';
import { Employees } from 'src/app/shared/models/employees';
import { ActivatedRoute, Router } from '@angular/router';
import { CountriesService } from 'src/app/shared/services/countries/countries.service';
import { StatesService } from 'src/app/shared/services/states/states.service';
import { CitiesService } from 'src/app/shared/services/cities/cities.service';
import { Countries } from 'src/app/shared/models/countries';
import { States } from 'src/app/shared/models/states';
import { Cities } from 'src/app/shared/models/cities';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: ['./new-employee.component.scss']
})
export class NewEmployeeComponent implements OnInit, OnDestroy {

  employeeForm: FormGroup;
  addDepartmentForm: FormGroup;
  addDesignationForm: FormGroup;
  button: String;
  employeeHeader: String;
  states: any;
  cities: any;
  userObj: any;
  roleType: any;
  employee$: any;
  employee_id: any;
  empDepartment: any;
  empDesignation: any;
  employeeInfoById: any;
  getAllEmployeeDetails: any;
  accounts$: any = [];
  countries: any = [];
  _states: States;
  _cities: Cities;
  _accounts: Account;
  _employees: Employees;
  _countries: Countries;
  accountsObj$: Observable<Account>;
  employeeObj: Observable<Employees[]>;
  postEmployeeObj: Observable<Employees>;
  employeeDetailsListing: Observable<Employees>;
  subscription: Subscription;

  constructor(public formBuilder: FormBuilder, private modalService: NgbModal, private accountservice: UserAccountService, private employeeService: EmployeesService,
    private toastr: ToastrService, private router: Router, private actRoute: ActivatedRoute,  private countryService: CountriesService, private stateService: StatesService, private cityService: CitiesService) {
    this.employee_id = this.actRoute.snapshot.params.id;
    this._accounts = new Account();
    this._employees = new Employees();
    this._countries = new Countries();
    this._states = new States();
    this._cities = new Cities();
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
  }

  ngOnInit(): void {
    this.getEmployeeDepartment();
    this.getEmployeeDesignation();
    this.getAllEmployees(this._employees);
    if (this.router.url === '/pages/new-employee') {
      this.employeeHeader = "Add Employee";
      this.button = "Submit";
    } else if (this.router.url === window.location.pathname) {
      this.employeeHeader = "Employee Profile";
      this.button = "Update";
      this.getEmployeeDetailById();
    } else {
      this.employeeHeader = "Update Employee";
      this.button = "Update";
      this.getEmployeeDetailById();
    }
    this.getCountries();
    this.getAccountDetailObj(this._accounts);
    this.employeeForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: [''],
      parent_account_id: ['', Validators.required],
      empName: ['', [Validators.required, Validators.minLength(3)]],
      empTitle: ['', Validators.required],
      empLevel: ['', Validators.required],
      designation: ['', Validators.required],
      deptDiv: ['', Validators.required],
      reportingTo: [''],
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      mobileNumber: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', [Validators.required, Validators.maxLength(6), Validators.minLength(6)]]
    })
    this.addDepartmentForm = this.formBuilder.group({
      department: ['']
    })
    this.addDesignationForm = this.formBuilder.group({
      designation: ['']
    })
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }

  generatePassword(length, charSet = "") {
    charSet = charSet ? charSet : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789^°!"§$%&/()=?`*+~\'#,;.:-_';
    let newPass = Array.apply(null, Array(length || 10)).map(function () {
      return charSet.charAt(Math.random() * charSet.length);
    }).join('');
    return newPass
  }

  editDesignation(content2) {
    this.modalService.open(content2, { centered: false });
  }
  editDepartment(content) {
    this.modalService.open(content, { centered: false });
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    this.employeeForm.markAllAsTouched();
    this.employeeForm.get("password").patchValue(this.generatePassword(8));
    if (this.employeeForm.valid) {
      if (this.router.url === '/pages/new-employee') {
        this.postEmployeeObj = this.employeeService.postEmployeeObj(this.employeeForm.value);
        this.postEmployeeObj.subscribe((res: any) => {
          if (res) {
            if (res.success === true) {
              setTimeout(() => {
                this.toastr.success('Employee added successfully!');
                this.employeeForm.reset();
                this.router.navigate(['/pages/search-employee']);
              }, 2000);
            }
          }
        }, (error) => {
          if(error.error.errors[0].msg === 'Email is already exist.'){
            this.toastr.error('Email is already exist!');
          } else if(error.error.errors[0].msg === 'username is already exist.'){
            this.toastr.error('Username is already exist!');
          } else if(error.error.errors[0].msg === 'must be at last 3 chars long.'){
            this.toastr.error('Username must be atleast 3 characters long!');
          } else{
            this.toastr.error('Invalid Data!');
          }
        });
      } else if (this.router.url === '/pages/EmployeessViewProfile/:id') {
        setTimeout(() => {
          this.editEmployeeDetail();
        }, 2000);
      }
      else {
        setTimeout(() => {
          this.editEmployeeDetail();
          this.router.navigate(['/pages/search-employee']);
        }, 1500);
      }
    } else {
      this.validateAllFormFields(this.employeeForm);
    }
  }

  getAccountDetailObj(accountObj) {
    this.accountservice.getAccountDetail(accountObj).subscribe((res: any) => {
      if (res) {
        if (res.success === true) {
          this.accounts$ = res.res.results;
        }
      }
    })
  }

  getParantAccountName(id) {
    let matchParentObj = this.accounts$.filter((element: any) => {
      if (element.id === id) {
        return id;
      }
    })
    if (matchParentObj.length > 0) {
      return matchParentObj[0].name
    }
  }

  getEmployeeDetailById() {
    this.employeeObj = this.employeeService.getEmployeesById(this.employee_id);
    this.employeeObj.subscribe((res: any) => {
      this.employeeInfoById = res.res;
      this.employeeForm.get('username').patchValue(this.employeeInfoById.username);
      this.employeeForm.get('password').patchValue(this.employeeInfoById.password);
      this.employeeForm.get('parent_account_id').patchValue(this.employeeInfoById.parent_account_id);
      this.employeeForm.get('empName').patchValue(this.employeeInfoById.name);
      this.employeeForm.get('empTitle').patchValue(this.employeeInfoById.employee_title);
      this.employeeForm.get('empLevel').patchValue(this.employeeInfoById.employee_level);
      this.employeeForm.get('designation').patchValue(this.employeeInfoById.designation);
      this.employeeForm.get('deptDiv').patchValue(this.employeeInfoById.department);
      this.employeeForm.get('reportingTo').patchValue(this.employeeInfoById.reporting_to);
      this.employeeForm.get('email').patchValue(this.employeeInfoById.email);
      this.employeeForm.get('mobileNumber').patchValue(this.employeeInfoById.mobile);
      this.employeeForm.get('address').patchValue(this.employeeInfoById.address);
      this.getStates(this.employeeInfoById.countries_id);
      this.employeeForm.get('country').patchValue(this.employeeInfoById.countries_id);
      this.getCities(this.employeeInfoById.states_id);
      this.employeeForm.get('state').patchValue(this.employeeInfoById.states_id);
      this.employeeForm.get('city').patchValue(this.employeeInfoById.city_id);
      this.employeeForm.get('zipCode').patchValue(this.employeeInfoById.zip);
    })
  }

  editEmployeeDetail() {
    this.putEmployeeByIdToAPI(this.employee_id, this.employeeForm.value);
    setTimeout(() => {
      this.employeeDetailsListing = this.employeeService.getAllEmployees(this._employees);
      this.employeeDetailsListing.subscribe((res: any) => {
        if (res) {
          this.getAllEmployeeDetails = res.res;
        }
      })
    }, 2000);
  }

  putEmployeeByIdToAPI(empId, empObj) {
    this.employeeService.updateEmployeeObj(empId, empObj).subscribe((res: any) => {
      if (res !== undefined) {
        this.toastr.success('Updated Employee Successfully');
      } else {
        this.toastr.error('Something went wrong to edit client details!');
      }
    })
  }
  getAllEmployees(employeeObj) {
    this.employeeService.getAllEmployees(employeeObj).subscribe((res: any) => {
      this.employee$ = res.res.results;
    })
  }

  onAddDepartment() {
    this.employeeService.postEmployeeDepartment(this.addDepartmentForm.value).subscribe((res: any) => {
      this.getEmployeeDepartment();
    })
    this.modalService.dismissAll();
    this.toastr.success('Employee Department added successfully!');
    this.addDepartmentForm.reset();
  }

  getEmployeeDepartment() {
    this.employeeService.getEmployeeDepartment().subscribe((res: any) => {
      this.empDepartment = res.res;
    })
  }

  onAddDesignation() {
    this.employeeService.postEmployeeDesignation(this.addDesignationForm.value).subscribe((res: any) => {
      this.getEmployeeDesignation();
      this.modalService.dismissAll();
      this.toastr.success('Employee Designation added successfully!');
      this.addDesignationForm.reset();
    })
  }

  getEmployeeDesignation() {
    this.employeeService.getEmployeeDesignation().subscribe((res: any) => {
      this.empDesignation = res.res;
    })
  }

  getCountries(){
    this.subscription = this.countryService.getAllCountries().subscribe((res: any) => {
      this.countries = res.res;
    })
  }

  getStates(countryId){
    this.subscription = this.stateService.getState(countryId).subscribe((res:any) => {
      this.states = res.res;
    })
  }

  getCities(stateId){
    this.subscription = this.cityService.getCities(stateId).subscribe((res:any) => {
      this.cities = res.res;
    })
  }
}
