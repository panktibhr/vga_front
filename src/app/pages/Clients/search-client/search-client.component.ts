import { Clients } from './../../../shared/models/clients';
import { ClientsService } from './../../../shared/services/clients/clients.service';
import { ModalManager } from 'ngb-modal';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-search-client',
  templateUrl: './search-client.component.html',
  styleUrls: ['./search-client.component.scss'],
})
export class SearchClientComponent implements OnInit, OnDestroy {
  @ViewChild('contentDeletes') contentDeletes;
  clients$: any = [];
  clientsObj: Observable<Clients>;
  client_id: string;
  searchClient: string;
  page: any = 0;
  itemsPerPage: number = 5;
  previousPage: any;
  totalItems: any;
  isLoading: boolean;
  _clients: Clients;
  subscription: Subscription;
  deleteModalRef: any;
  _searchClintDeleteId: any;

  constructor(
    private clientService: ClientsService,
    private router: Router,
    private toastr: ToastrService,
    private modalService: ModalManager
  ) {
    this._clients = new Clients();
  }

  ngOnInit() {
    this.getClientDetail(this._clients);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  getClientDetail(clientsObj) {
    this.clientsObj = this.clientService.getAllClients(clientsObj);
    this.subscription = this.clientsObj.subscribe((res: any) => {
      if (res) {
        this.totalItems = res.res.total;
        this.clients$ = res.res.results;
      } else {
        this.totalItems = 0;
        this.clients$ = [];
      }
    });
  }

  onEditClientDetails(id) {
    this.router.navigate(['pages/edit-client', id]);
  }
  onDeleteClientDetail(id) {
    console.log(id);
    this._searchClintDeleteId = id;
    this.deleteModalRef = this.modalService.open(this.contentDeletes, {
      size: 'md',
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
    });
    this.deleteModalRef.onClose.subscribe(() => {
    });
  }

  OnCanceleDeleteModal() {
    this.modalService.close(this.deleteModalRef);
  }

  onConfrimDelete() {
    this.deleteClientByIdToAPI(this._searchClintDeleteId);
    this.modalService.close(this.deleteModalRef);
    this.toastr.success('User deleted successfully!');
  }

  deleteClientByIdToAPI(idForClientDelete) {
    this.subscription = this.clientService
      .deleteClientDetailsObj(idForClientDelete)
      .subscribe((res: any) => {
        if (res.success) {
          this.getClientDetail(this._clients);
        } else {
          this.toastr.error('Soemthing is wrong');
        }
      });
  }

  onSearch(searchText, searchConsultant,searchRenewalStartDate,searchRenewalEndDate) {
    this._clients.SearchText = searchText;
    this._clients.SearchConsultant = searchConsultant;
    this._clients.SearchRenewalStartDate = searchRenewalStartDate;
    this._clients.SearchRenewalEndDate = searchRenewalEndDate;
    this.getClientDetail(this._clients);
  }

  onclear(clearText, clearConsultant, clearRenewalStartDate, clearRenewalEndDate){
    this._clients.SearchText = clearText;
    this._clients.SearchConsultant = clearConsultant;
    this._clients.SearchRenewalStartDate = clearRenewalStartDate;
    this._clients.SearchRenewalEndDate = clearRenewalEndDate;
    this.getClientDetail(this._clients);
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this._clients.PageNo = page - 1;
      this.previousPage = page;
      this.getClientDetail(this._clients);
    }
  }

  sort(pageNumber: any) {
    this._clients.SortItem = pageNumber;
    if (this._clients.SortOrder == 'ASC') {
      this._clients.SortOrder = 'DESC';
    } else {
      this._clients.SortOrder = 'ASC';
    }
    this.getClientDetail(this._clients);
  }
}
