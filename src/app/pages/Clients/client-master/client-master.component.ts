import { Employees } from 'src/app/shared/models/employees';
import { Observable, Subscription } from 'rxjs';
import { ClientsService } from './../../../shared/services/clients/clients.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Clients } from 'src/app/shared/models/clients';
import { UserAccountService } from '../../../shared/services/user-account/user-account.service';
import { Account } from '../../../shared/models/userAccount';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ConsultantService } from 'src/app/shared/services/consultant/consultant.service';
import { EmployeesService } from 'src/app/shared/services/employees/employees.service';
import { VgaAuditorService } from '../../../shared/services/vga-auditor/vga-auditor.service';
import { Consultant } from 'src/app/shared/models/consultant';
import { VGAAuditor } from 'src/app/shared/models/vga-auditor';
import { DomSanitizer } from '@angular/platform-browser';
import { CountriesService } from 'src/app/shared/services/countries/countries.service';
import { StatesService } from 'src/app/shared/services/states/states.service';
import { CitiesService } from 'src/app/shared/services/cities/cities.service';
import { Countries } from 'src/app/shared/models/countries';
import { States } from 'src/app/shared/models/states';
import { Cities } from 'src/app/shared/models/cities';
import { environment } from 'src/environments/environment';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { SectionMasterService } from 'src/app/shared/services/section-master/section-master.service';
@Component({
  selector: 'app-client-master',
  templateUrl: './client-master.component.html',
  styleUrls: ['./client-master.component.scss'],
})
export class ClientMasterComponent implements OnInit, OnDestroy {
  clientMasterForm: FormGroup;
  imageUrl: any = '../../../../assets/nophoto.png';
  countries: any;
  states: any;
  cities: any;
  client_id: any;
  clientInfoById: any;
  clientDetailsObjForPOST: any;
  getAllClientDetails: any;
  consultant: any;
  employees: any;
  accounts$: any = [];
  client$: any = [];
  button: string;
  clientPageTitle: string;
  auditors: any;
  _clients: Clients;
  _employees: Employees;
  _accounts: Account;
  _consultants: Consultant;
  _auditors: VGAAuditor;
  _countries: Countries;
  _states: States;
  _cities: Cities;
  clientsObj: Observable<Clients[]>;
  clientDetailsListing: Observable<Clients>;
  postClientObj$: Observable<Clients>;
  accountsObj$: Observable<Account>;
  subscription: Subscription;
  clientDuration: any = [
    { id: 1, name: 'Quarterly' },
    { id: 2, name: 'Annual' },
  ];
  userObj: any;
  roleType: any;
  selectedname: any;
  dropdownList = [];
  selectedDropDownData: any[];
  dropdownSettings: IDropdownSettings;
  getDropdownValues: any = [];

  constructor(
    public formBuilder: FormBuilder,
    private clientService: ClientsService,
    private accountservice: UserAccountService,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public datePipe: DatePipe,
    private consultantService: ConsultantService,
    private employeeService: EmployeesService,
    private VgaAuditorService: VgaAuditorService,
    private dom: DomSanitizer,
    private countryService: CountriesService, private stateService: StatesService, private cityService: CitiesService,
    private sectionService: SectionMasterService
  ) {
    this.client_id = this.actRoute.snapshot.params.id;
    this._clients = new Clients();
    this._consultants = new Consultant();
    this._employees = new Employees();
    this._accounts = new Account();
    this._auditors = new VGAAuditor();
    this._countries = new Countries();
    this._states = new States();
    this._cities = new Cities();
    this.userObj = JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem('userObj'), environment.SECRET_KEY).toString(CryptoJS.enc.Utf8));
    this.roleType = this.userObj.roleName;
  }

  ngOnInit() {
    if (this.router.url === '/pages/new-client') {
      this.button = 'Submit';
      this.clientPageTitle = 'New Client';
    } else if (this.router.url === window.location.pathname) {
      this.button = 'Update';
      this.clientPageTitle = 'View Profile';
      this.getClientDetailById();
    } else {
      this.button = 'Update';
      this.clientPageTitle = 'Update Client';
      this.getClientDetailById();
    }
    this.getAllSections();
    this.getConsultantData(this._consultants);
    this.getEmployeeData(this._employees);
    this.getVGAAuditorData();
    this.getAccountDetailObj();
    this.getCountries();
    this.clientMasterForm = this.formBuilder.group({
      id: [],
      clientLogo: [''],
      clientName: ['', [Validators.required, Validators.minLength(3)]],
      mobileNumber: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      companyName: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zip: ['', [Validators.required, Validators.pattern('^[0-9]{6}$')]],
      website: ['', Validators.required],
      contactPerson: ['', Validators.required],
      contactPersonDesignation: ['', Validators.required],
      contactPersonPhone: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      contactPersonEmail: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      chiefPerson: ['', Validators.required],
      chiefPersonDesignation: ['', Validators.required],
      chiefPersonPhone: ['', Validators.required],
      chiefPersonEmail: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      industryType: ['', Validators.required],
      processType: ['', Validators.required],
      customerType: ['', Validators.required],
      businessScale: ['', Validators.required],
      ownershipType: ['', Validators.required],
      businessNature: ['', Validators.required],
      marketType: ['', Validators.required],
      intensity: ['', Validators.required],
      coreDrivingForce: ['', Validators.required],
      moderateDrivingForce: ['', Validators.required],
      minorDrivingForce: ['', Validators.required],
      coreCompetence: ['', Validators.required],
      moderateCompetence: ['', Validators.required],
      minorCompetence: ['', Validators.required],
      profitabilityStatus: ['', Validators.required],
      sections: [this.selectedDropDownData],
      employees: ['', Validators.required],
      vgaAuditor: ['', Validators.required],
      consultant: ['', Validators.required],
      clientType: ['', Validators.required],
      clientCreationDate: ['', Validators.required],
      clientDuration: ['', Validators.required],
      renewalDate: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      username: ['', Validators.required],
      password: ['', Validators.required],
      parent_account_id: ['', Validators.required],
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'sectionName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  onChangeClientProfilePicture(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const uploadFiles = event.target.files[0]; // use this if you want to send imageObject to API
      const uploadLogo = uploadFiles;
      reader.readAsDataURL(uploadFiles);
      // const reader = new FileReader();
      reader.onload = () => {
        this.imageUrl = reader.result as string; // use this when you want to send base64 value of an image to API
        this.clientMasterForm.patchValue({ clientLogo: reader.result });
        this.clientMasterForm.get('clientLogo').updateValueAndValidity();
        // this.clientMasterForm.patchValue({ clientLogo: uploadFiles });
      };
      // reader.readAsDataURL(uploadFiles);
    }
  }

  generatePassword(length, charSet = '') {
    charSet = charSet ? charSet : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789^°!"§$%&/()=?`*+~\'#,;.:-_';
    let newPass = Array.apply(null, Array(length || 10)).map(function () {
      return charSet.charAt(Math.random() * charSet.length);
    }).join('');
    return newPass;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    this.clientMasterForm.markAllAsTouched();
    this.clientMasterForm.get('password').patchValue(this.generatePassword(8));
    if (this.clientMasterForm.valid) {
      if (this.router.url === '/pages/new-client') {
        this.postClientObj$ = this.clientService.postClientDetailsObj(this.clientMasterForm.value);
        this.postClientObj$.subscribe((res: any) => {
          this.clientDetailsObjForPOST = this.clientMasterForm.value;
          if (res) {
            if (res.success === true) {
              this.toastr.success('Client added Successfully!');
              setTimeout(() => {
                this.router.navigate(['/pages/search-client']);
                this.clientMasterForm.reset();
              }, 1000);
            }
          }
        }, (error) => {
          if(error.error.errors[0].msg === 'Email is already exist.'){
            this.toastr.error('Email is already exist!');
          } else if(error.error.errors[0].msg === 'username is already exist.'){
            this.toastr.error('Username is already exist!');
          } else if(error.error.errors[0].msg === 'must be at last 3 chars long.'){
            this.toastr.error('Username must be at 3 characters long!');
          } else{
            this.toastr.error('Invalid Data!')
          }
        });
      } else if(this.router.url === window.location.pathname) {
        setTimeout(() => {
          this.editClientDetails();
        }, 1500);
      } else {
        setTimeout(() => {
          this.editClientDetails();
          this.router.navigate(['/pages/search-client']);
        }, 1500);
      }
    } else {
      this.toastr.error('Invalid Form Data!');
    }
  }

  getAccountDetailObj() {
    this.accountsObj$ = this.accountservice.getAccountDetail(this._accounts);
    this.accountsObj$.subscribe((res: any) => {
      if (res) {
        if (res.success === true) {
          this.accounts$ = res.res.results;
        }
      }
    });
  }

  getParantAccountName(id) {
    let matchParentObj = this.accounts$.filter((element: any) => {
      if (element.id === id) {
        return id;
      }
    });
    if (matchParentObj.length > 0) {
      return matchParentObj[0].name;
    }
  }

  getClientDetailById() {
    this.clientsObj = this.clientService.getClientsById(this.client_id);
    this.clientsObj.subscribe((res: any) => {
      this.clientInfoById = res.res;
      let sectionList = this.clientInfoById.section_details;

      sectionList?.filter((list: any) => {
        this.getDropdownValues.push(JSON.parse(list).sectionName);
      });
      let clientCreationDate = this.clientInfoById.client_creation_date;
      let clientCreationTransformedDate = this.datePipe.transform(clientCreationDate, 'yyyy-MM-dd');
      let renewalDate = this.clientInfoById.renewal_date;
      const renewalTransformedDate = this.datePipe.transform(renewalDate, 'yyyy-MM-dd');
      this.imageUrl = this.dom.bypassSecurityTrustUrl(`data:image/png;base64,${this.clientInfoById.logo}`);
      this.clientMasterForm.get('clientName').patchValue(this.clientInfoById.name);
      this.clientMasterForm.get('mobileNumber').patchValue(this.clientInfoById.mobile_number);
      this.clientMasterForm.get('companyName').patchValue(this.clientInfoById.company_name);
      this.clientMasterForm.get('address').patchValue(this.clientInfoById.address);
      this.clientMasterForm.get('email').patchValue(this.clientInfoById.email);
      this.clientMasterForm.get('username').patchValue(this.clientInfoById.username);
      this.clientMasterForm.get('parent_account_id').patchValue(this.clientInfoById.parent_account_id);
      this.getStates(this.clientInfoById.country);
      this.clientMasterForm.get('country').patchValue(this.clientInfoById.country);
      this.getCities(this.clientInfoById.state);
      this.clientMasterForm.get('state').patchValue(this.clientInfoById.state);
      this.clientMasterForm.get('city').patchValue(this.clientInfoById.city);
      this.clientMasterForm.get('zip').patchValue(this.clientInfoById.zip);
      this.clientMasterForm.get('website').patchValue(this.clientInfoById.website);
      this.clientMasterForm.get('contactPerson').patchValue(this.clientInfoById.contact_person);
      this.clientMasterForm.get('contactPersonDesignation').patchValue(this.clientInfoById.contact_person_designation);
      this.clientMasterForm.get('contactPersonPhone').patchValue(this.clientInfoById.contact_person_phone);
      this.clientMasterForm.get('contactPersonEmail').patchValue(this.clientInfoById.contact_person_email);
      this.clientMasterForm.get('chiefPerson').patchValue(this.clientInfoById.chief_person);
      this.clientMasterForm.get('chiefPersonDesignation').patchValue(this.clientInfoById.chief_person_designation);
      this.clientMasterForm.get('chiefPersonPhone').patchValue(this.clientInfoById.chief_person_phone);
      this.clientMasterForm.get('chiefPersonEmail').patchValue(this.clientInfoById.chief_person_email);
      this.clientMasterForm.get('industryType').patchValue(this.clientInfoById.industry_type);
      this.clientMasterForm.get('processType').patchValue(this.clientInfoById.process_type);
      this.clientMasterForm.get('customerType').patchValue(this.clientInfoById.customer_type);
      this.clientMasterForm.get('businessScale').patchValue(this.clientInfoById.business_scale);
      this.clientMasterForm.get('ownershipType').patchValue(this.clientInfoById.ownership_type);
      this.clientMasterForm.get('businessNature').patchValue(this.clientInfoById.business_nature);
      this.clientMasterForm.get('marketType').patchValue(this.clientInfoById.market_type);
      this.clientMasterForm.get('intensity').patchValue(this.clientInfoById.intensity);
      this.clientMasterForm.get('coreDrivingForce').patchValue(this.clientInfoById.core_driving_force);
      this.clientMasterForm.get('moderateDrivingForce').patchValue(this.clientInfoById.moderate_driving_force);
      this.clientMasterForm.get('minorDrivingForce').patchValue(this.clientInfoById.minor_driving_force);
      this.clientMasterForm.get('coreCompetence').patchValue(this.clientInfoById.core_competence);
      this.clientMasterForm.get('moderateCompetence').patchValue(this.clientInfoById.moderate_competence);
      this.clientMasterForm.get('minorCompetence').patchValue(this.clientInfoById.minor_competence);
      this.clientMasterForm.get('profitabilityStatus').patchValue(this.clientInfoById.profitability_status);
      this.clientMasterForm.get('employees').patchValue(this.clientInfoById.employee);
      this.clientMasterForm.get('consultant').patchValue(this.clientInfoById.consultant);
      this.clientMasterForm.get('vgaAuditor').patchValue(this.clientInfoById.auditor);
      this.clientMasterForm.get('clientType').patchValue(this.clientInfoById.client_type);
      this.clientMasterForm.get('clientCreationDate').patchValue(clientCreationTransformedDate);
      this.clientMasterForm.get('clientDuration').patchValue(this.clientInfoById.client_duration);
      this.clientMasterForm.get('renewalDate').patchValue(renewalTransformedDate);
      this.clientMasterForm.get('sections').patchValue(this.getDropdownValues);
    });
  }

  editClientDetails() {
    this.putClientByIdToAPI(this.client_id, this.clientMasterForm.value);
    setTimeout(() => {
      this.clientDetailsListing = this.clientService.getAllClients(this._clients);
      this.clientDetailsListing.subscribe((res: any) => {
        if (res) {
          this.getAllClientDetails = res.client;
        }
      });
    }, 1500);
  }

  putClientByIdToAPI(clientId, clientDetailObj) {
    this.clientService.updateClientById(clientId, clientDetailObj).subscribe((res: any) => {
      if (res !== undefined) {
        this.toastr.success('Client Detail edited successfully!');
        if(this.roleType !== 'Client')
          this.router.navigate(['/pages/search-client']);
      } else
        this.toastr.error('Something went wrong to edit client details!','Student');
    }, (error) => {
      console.log(`error - `, error)
      // if(error.error.errors[0].msg === 'Email is already exist.'){
      //   this.toastr.error('Email is already exist!');
      // } else if(error.error.errors[0].msg === 'username is already exist.'){
      //   this.toastr.error('Username is already exist!');
      // } else if(error.error.errors[0].msg === 'must be at last 3 chars long.'){
      //   this.toastr.error('Username must be at 3 characters long!');
      // } else{
      //   this.toastr.error('Invalid Data!')
      // }
    });
  }
  getConsultantData(consultantsObj) {
    this.consultantService.getAllConsultants(consultantsObj).subscribe((res: any) => {
      this.consultant = res.res.results;
    });
  }
  getEmployeeData(employeeObj) {
    this.employeeService.getAllEmployees(employeeObj).subscribe((res: any) => {
      this.employees = res.res.results;
    });
  }
  getVGAAuditorData() {
    this.subscription = this.VgaAuditorService.getAllVGAAuditors().subscribe((res: any) => {
      this.auditors = res.res;
      console.log(`res - `, res)
    });
  }
  getCountries(){
    this.subscription = this.countryService.getAllCountries().subscribe((res: any) => {
      this.countries = res.res;
    });
  }

  getStates(countryId){
    this.subscription = this.stateService.getState(countryId).subscribe((res:any) => {
      this.states = res.res;
    });
  }

  getCities(stateId){
    this.subscription = this.cityService.getCities(stateId).subscribe((res:any) => {
      this.cities = res.res;
    });
  }

  getAllSections(){
    this.subscription = this.sectionService.getAllSec().subscribe((section: any) => {
      let temp = [];
      section.res.filter((sectionData: any) => {
        temp.push({id: sectionData.id, sectionName: sectionData.section_name});
      });
      this.dropdownList = temp;
    });
  }

  onItemSelect(item: any) {
    let data = [];
    data.push(item);
    this.selectedDropDownData = data[0];
  }

  onSelectAll(items: any) {
    let data = [];
    data.push(items);
    this.selectedDropDownData = data[0];
  }

}
